(in-package :smots)

(defgeneric pair-projection (sletters alphai))

(defmethod pair-projection ((sletters list) (alphai alphabet))
  (do ((pp ()))
      ((endp sletters) (nreverse pp))
    ;;    (format *error-output* "main do sletters ~A ~%" sletters)
    (let* ((sl (pop sletters))
	   (psl (projection sl alphai)))
      (push (cons
	     psl
	     (make-sword
	      (cons
	       sl
	       (if (not (empty-sletterp psl))
		   ()
		   (loop
		      while (and sletters
				 (empty-sletterp
				  (projection (car sletters) alphai)))
		      collect (pop sletters))))))
	    pp))))

(defmethod fusion-pp-rec ((pp1 list) (pp2 list))
  (cond
    ((endp pp1)
     (if (endp pp2) (empty-sword) (reduce #'concatenation (mapcar #'cdr pp2))))
    ((endp pp2)
     (reduce #'concatenation (mapcar #'cdr pp1)))
    (t
     (let ((sl1 (caar pp1))
	   (sl2 (caar pp2)))
       (cond
	 ((and (empty-sletterp sl1)
	       (empty-sletterp sl2))
	  (reduce #'lunion
		  (list
		   (concatenation
		    (cdar pp1)
		    (fusion-pp-rec (cdr pp1) pp2))
		   (concatenation
		    (cdar pp2)
		    (fusion-pp-rec pp1 (cdr pp2)))
		   (concatenation
		    (make-mix (list (cdar pp1) (cdar pp2)))
		    (fusion-pp-rec (cdr pp1) (cdr pp2))))))
	 ((empty-sletterp sl1)
	  (concatenation
	   (cdar pp1)
	   (fusion-pp-rec (cdr pp1) pp2)))
	 ((empty-sletterp sl2)
	  (concatenation
	   (cdar pp2)
	   (fusion-pp-rec pp1 (cdr pp2))))
	 ((eq sl1 sl2)
	  (concatenation
	   (make-sword
	    (list
	     (make-sletter (letters (malphabet-of 
				     (list (cdar pp1)
					   (cdar pp2)))))))
	   (fusion-pp-rec (cdr pp1) (cdr pp2)))))))))

;;(defun extend-sword-fun (sletters)
;;  (lambda (sword) (make-sword (append (letters sword) sletters))))

(defun extend-lexp-fun (sletters)
  (lambda (lexp) (concatenation lexp (make-sword sletters))))

(defun mapfun (funs x)
  (mapcar (lambda (f) (funcall f x)) funs))

(defgeneric two-empty-first-letter (sword1 sword2 res-fusion))
(defgeneric one-empty-first-letter (sword1 res-fusion))
  
;; (defmethod two-empty-first-letter ((sword1 sword) (sword2 sword) (res-fusion list))
;;   (let* ((mix-swords (words
;; 		      (mix sword1 sword2)
;; ))
;; 	 (funs (mapcar (lambda (mix-sword)
;; 			 (extend-lexp-fun (letters mix-sword)))
;; 		       mix-swords)))
;;     (loop for sword in res-fusion
;; 	  append 
;; 	  (mapfun funs sword))))

(defmethod two-empty-first-letter ((sword1 sword) (sword2 sword) (res-fusion list))
  (let ((mix-exp (make-mix (list sword1 sword2))))
    (loop
       for lexpr in res-fusion
       collect (make-concatenation (list lexpr mix-exp)))))

(defmethod one-empty-first-letter ((sword sword) (res-fusion list))
  (mapcar
   (extend-lexp-fun (letters sword))
   res-fusion))

(defgeneric eq-sl1-sl2 (sword1 sword2 res-fusion))

(defmethod eq-sl1-sl2 ((sword1 sword) (sword2 sword) (res-fusion list))
  (let ((sletters1 (letters sword1))
	(sletters2 (letters sword2)))
    (assert (= 1 (length sletters1)))
    (assert (= 1 (length sletters2)))
    (mapcar
;;     (extend-sword-fun
     (extend-lexp-fun
      (list (make-sletter (union (letters (car sletters1))
				 (letters (car sletters2))))))
     res-fusion)))

(defgeneric fusion-pp-it (pp1 pp2)
  (:documentation "iteratively merges pair-projection lists"))

(defgeneric fusion-pp-rec (pp1 pp2)
  (:documentation "recursively merges pair-projection lists"))

(defmethod fusion-pp-it ((pp1 list) (pp2 list))
  (assert (not (and (endp pp1) (endp pp1))))
  (let ((res-fusion  (list (empty-sword))))
    (loop
     while (and pp1 pp2)
     do (cond
	  ((and (empty-sletterp (caar pp1)) (empty-sletterp (caar pp2)))
	   (setf res-fusion
		 (two-empty-first-letter 
		  (cdar pp1)
		  (cdar pp2)
		  res-fusion))
	   (pop pp1)
	   (pop pp2))
	  ((empty-sletterp (caar pp1))
	   (setf res-fusion
		 (one-empty-first-letter (cdar pp1) res-fusion))
	   (pop pp1))
	  ((empty-sletterp (caar pp2))
	   (setf res-fusion
		 (one-empty-first-letter (cdar pp2) res-fusion))
	   (pop pp2))
	  ((eq (caar pp1) (caar pp2))
	   (setf res-fusion (eq-sl1-sl2 (cdar pp1) (cdar pp2) res-fusion))
	   (pop pp1)
	   (pop pp2))))
    (when (and pp1 (endp pp2))
      (setf res-fusion (mapcar
;;			(extend-sword-fun (letters (cdar pp1)))
			(extend-lexp-fun (letters (cdar pp1)))
			res-fusion)))
    (when (and pp2 (endp pp1))
      (setf res-fusion (mapcar
;;			(extend-sword-fun (letters (cdar pp2)))
			(extend-lexp-fun (letters (cdar pp2)))
			res-fusion)))
    (reduce #'lunion  res-fusion)))

(defvar *iterative* t)

(defgeneric fusion (sword1 sword2 malphai  &optional iterative))

(defmethod fusion ((sword1 sword) (sword2 sword) (malphai alphabet)
		   &optional (iterative t))
;;  (format *error-output* "fusion sword sword ~A ~A~%" sword1 sword2)
  (let ((pp1 (pair-projection (letters sword1) malphai))
	(pp2 (pair-projection (letters sword2) malphai)))
    (if (eq (make-sword
	     (remove-if #'empty-sletterp (mapcar #'car pp1)))
	    (make-sword
	     (remove-if #'empty-sletterp (mapcar #'car pp2))))
	(if iterative 
	    (fusion-pp-it pp1 pp2)
	    (fusion-pp-rec pp1 pp2))
	(empty-language sword1))))


(defgeneric complementary-malphabet (sword1 sword2 alphai)
  (:documentation "complement sword1 with mletters of sword2"))

(defmethod complementary-malphabet :before ((lexpr1 s-mixin) (lexpr2 s-mixin) (alphai alphabet))
  (declare (ignore lexpr1 lexpr2))
  (assert (not (aemptyp alphai))))

(defmethod complementary-malphabet ((lexpr1 s-mixin) (lexpr2 s-mixin) (alphai alphabet))
  (aset-difference 
   (projection (malphabet-of lexpr2) alphai)
   (malphabet-of lexpr1)))

(defmethod ljoin ((sword1 sword) (sword2 sword))
  ;;  (format *error-output* "ljoin sword sword ~A ~A~%" sword1 sword2)
  (let* ((alpha1 (alphabet-of sword1))
	 (alpha2 (alphabet-of sword2))
	 (alphai (aintersection alpha1 alpha2)))
    (if (aemptyp alphai)
	(make-mix (list sword1 sword2))
	(let ((ap1 (complementary-malphabet sword1 sword2 alphai))
	      (ap2 (complementary-malphabet sword2 sword1 alphai)))
	  (if (and (aemptyp ap1) (aemptyp ap2))
	      (fusion sword1 sword2 alphai *iterative*)
	      (let ((e1 (if (aemptyp ap1)
			    sword1
			    (complete-with-mletters sword1 ap1)))
		    (e2 (if (aemptyp ap2)
			    sword2
			    (complete-with-mletters sword2 ap2))))
		(ljoin e1 e2)))))))

(defmethod brute-join ((sword1 sword) (sword2 sword))
  (compute-language (ljoin sword1 sword2)))

;;(aset-difference 
;; (projection (malphabet-of sword2) alphai)
;; (malphabet-of sword1)))
