(in-package :smots)

(defclass commutative-mixin () ())

(defmethod clean-args :before ((assoc-lexpr commutative-mixin))
  (when *trace-clean-args*
    (format *error-output* "clean-args :before commutative-mixin ~A~%" assoc-lexpr))
;;  (let ((args (order-according-priority (args assoc-lexpr))))
  (let ((args (args assoc-lexpr)))
    (setf (args assoc-lexpr)
	  (multiple-value-bind (lt lf)
	      (split args
		     (lambda (x)
		       (eq (class-of x) (class-of assoc-lexpr))))
	    (append lt lf))))
  assoc-lexpr)
