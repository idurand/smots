(in-package :smots)
;;; some sletters
(defparameter *a* (make-sletter (list (make-mletter "a"))))
(defparameter *b* (make-sletter (list (make-mletter "b"))))
(defparameter *c* (make-sletter (list (make-mletter "c"))))
(defparameter *d* (make-sletter (list (make-mletter "d"))))
(defparameter *e* (make-sletter (list (make-mletter "e"))))
(defparameter *f* (make-sletter (list (make-mletter "f"))))
(defparameter *ab* (make-sletter (list (make-mletter "a") (make-mletter "b"))))
(defparameter *ac* (make-sletter (list (make-mletter "a") (make-mletter "c"))))
(defparameter *cd* (make-sletter (list (make-mletter "c") (make-mletter "d"))))
(defparameter *abc* (make-sletter (list (make-mletter "a") (make-mletter "b") (make-mletter "c"))))
(defparameter *abe* (make-sletter (list (make-mletter "a") (make-mletter "b") (make-mletter "e"))))
(defparameter *abf* (make-sletter (list (make-mletter "a") (make-mletter "b") (make-mletter "f"))))
(defparameter *ade* (make-sletter (list (make-mletter "a") (make-mletter "d") (make-mletter "e"))))
(defparameter *aef* (make-sletter (list (make-mletter "a") (make-mletter "e") (make-mletter "f"))))
(defparameter *bdf* (make-sletter (list (make-mletter "b" ) (make-mletter "d") (make-mletter "f"))))
(defparameter *de* (make-sletter (list (make-mletter "d" ) (make-mletter "e" ))))
;;; some swords
(defparameter *wa* (input-sword "[{a}]"))
(defparameter *wb* (input-sword "[{b}]"))
(defparameter *wc* (input-sword "[{c}]"))
(defparameter *wd* (input-sword "[{d}]"))
(defparameter *we* (input-sword "[{e}]"))
(defparameter *wf* (input-sword "[{f}]"))

(defparameter *wc-c* (input-sword "[{c}{c}]"))

(defparameter *wac* (input-sword "[{a,c}]"))
(defparameter *wbdf* (input-sword "[{b,d,f}]"))
(defparameter *wde* (input-sword "[{d,e}]"))
(defparameter *ww* (input-sword "[{a,b}{c}{b}{a}{b}{a,b}{a}{a,b,c}]"))

(defparameter *waabb* (make-sword-n (list *a* *a* *b* *b*)))
(defparameter *waa* (make-sword-n (list *a* *a*)))
(defparameter *wbb* (make-sword-n (list *b* *b*)))
(defparameter *wab* (make-sword-n (list *a* *b*)))
(defparameter *wba* (make-sword-n (list *b* *a*)))
(defparameter *wad* (make-sword-n (list *a* *d*)))
(defparameter *wbc* (make-sword-n (list *b* *c*)))
(defparameter *wadbc* (make-sword-n (list *a* *d* *b* *c*)))
(defparameter *wade* (make-sword-n (list *a* *d* *e*)))
(defparameter *waabb* (make-sword-n (list *a* *a* *b* *b*)))
(defparameter *wbbaa* (make-sword-n (list *b* *b* *a* *a*)))
(defparameter *wacac* (make-sword-n (list *a* *c* *a* *c*)))
(defparameter *wabb* (make-sword-n (list *a* *b* *b*)))
(defparameter *slang* (make-slanguage (list *waa* *wbb*)))
(defparameter *lexpr* (make-ljoin (list *waa* *wbb*)))
(compute-language *lexpr*)

;;; tests projection
(defparameter *mf* (input-sword "[{a,c}{a,b}{c,d}{a,b,c}]"))
(defparameter *mg* (input-sword "[{e}{a,e,f}{e}{a,b}{f}{a,b,f}{e}]"))
(defparameter *mh* (input-sword "[{a,e,f}{e}{a,b}{f}]"))
(defparameter *alphaf* (alphabet-of *mf*))
(defparameter *alphag* (alphabet-of *mg*))
(defparameter *alphah* (alphabet-of *mh*))

(defparameter *alphau* (reduce #'aunion (mapcar #'alphabet-of (list *mf* *mg* *mh*))))
(defparameter *alphas* (mapcar #'alphabet-of (list *mf* *mg* *mh*)))

(defparameter *alphainter* (reduce #'aintersection *alphas*))
(defparameter *alphau* (reduce #'aunion *alphas*))

(defparameter *pf* (projection *mf* *alphainter*))
(defparameter *pg* (projection *mg* *alphainter*))
(projection *mg* *alphainter*)
(projection *mh* *alphainter*)


(defparameter *m1* (input-sword "[{a,c}{a,b}{c,d}{a,b,c}]"))
(defparameter *m2* (input-sword "[{a,e,f}{e}{a,b}{f}{a,b,f}]"))

;; SMOTS> *ex2*
;; [{a_0,b_0,e_0}] . <[{a_1}] X [{b_1}] X [{e_1}]>
;; SMOTS> *sl*
;; {a_0,b_0,e_0}
;; SMOTS> (cut-extreme-letter *ex2* *sl*)
;;   0: (CUT-EXTREME-LETTER [{a_0,b_0,e_0}] . <[{a_1}] X [{b_1}] X [{e_1}]>
;;                          {a_0,b_0,e_0})
;; ; Evaluation aborted

(defparameter 
	   *s1* 
	 (make-concatenation 
	  (list
	   (input-sword "[{a}{b}]")
	   (make-mix
	    (list
	     (input-sword "[{a_1}]")	     
	     (input-sword "[{b_1}]"))))))

(defparameter 
    *s2* 
  (make-concatenation 
   (list
    (make-mix
     (list
      (input-sword "[{b}]")	     
      (input-sword "[{c}]")))
    (input-sword "[{b_1}{c_1}]"))))

(defparameter 
    *ex1* 
  (make-concatenation 
   (list
    (input-sword "[{a}{b}]"))))

;; SMOTS> *ex1*
;; ([{b_0}] . ([{a_1}] X [{b_1}])) X [{c_0}{c_1}]
;; SMOTS> *ex2*
;; (([{b_0}] X [{c_0}]) . [{b_1}{c_1}]) X [{a_1}]
;; SMOTS> *w1*
;; [{b_1}{c_1}]
;; SMOTS> *w2*
;; [{c_0}]

;; faire un quasi-mix qui autorise des malphabets disjoints (mais avec des
;; alphabets non disjoints
;; c'est au moment du calcul qu'on rectifie les r�gles implicites

SMOTS> *e1*
([{a,b,e}] . ([{a}] X [{b}] X [{e}]))
SMOTS> *e2*
((([{a}] X [{b}]) . [{a}{b}]) X [{e}{e}])
(lintersection *e1* *e2*)
