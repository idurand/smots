(in-package :smots)

(defvar *print-type* nil)
(defvar *internal-letters* nil)

(defgeneric letters (object)
  (:documentation "list of all the letters used in OBJECT"))

(defgeneric (setf letters) (val spec)
  (:documentation "writer on the list of letters defined in SPECIFICATION"))

(defmethod letters ((spec (eql nil)))
  *internal-letters*)

(defmethod (setf letters) (val (spec (eql nil)))
  (setf *internal-letters* val))

(defclass letter (abstract-letter named-object alpha-object)
  ((granularity :reader granularity :initarg :granularity))
 (:documentation "class for letters with granularity"))

(defmethod print-object ((letter letter) stream)
  (write-name (name letter) stream))

(defgeneric make-letter (string &optional granularity)
  (:documentation "created a letter STRING"))

(defmethod make-letter-preserving-granularity ((string string))
  (let* ((name (make-name string))
	 (letter (car
		  (member name (letters *spec*) :test #'eq :key #'name))))
    (or letter
	(make-letter string 2))))

(defmethod make-letter ((string string) &optional (granularity 2))
  (let* ((name (make-name string))
	 (letter (car
		  (member name (letters *spec*) :test #'eq :key #'name))))
    (when (and letter (not (= (granularity letter) granularity)))
      (format *error-output*
	      "Warning letter ~A already defined with granularity ~A ~%"
	      letter (granularity letter))
      (format *error-output* "Changing granularity to ~A ~%" granularity)
      (setf (slot-value letter 'granularity) granularity))
    (unless letter
      (setf letter
	    (make-instance 'letter
			   :name name
			   :granularity granularity))
      (push letter (letters *spec*)))
    letter))

(defmethod make-punctual-letter ((string string))
  (make-letter string 1))

(defmethod punctualp ((letter letter))
  (= (granularity letter) 1))

(defmethod intervalp ((letter letter))
  (= (granularity letter) 2))
   
(defgeneric make-unique-internal-letter (&optional granularity)
  (:method (&optional (granularity 2))
    (make-instance 'letter
		   :name (symbol-name (gensym)) :granularity granularity)))

(defgeneric equiv-named-letters (letter1 letter2)
  (:documentation "T if LETTER1 and LETTER2 have the same name")
  (:method ((letter1 abstract-letter) (letter2 abstract-letter))
    (eq (name letter1) (name letter2))))
