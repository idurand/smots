(in-package :smots)

(defgeneric empty-sword ()
  (:documentation "empty-sword"))

(defgeneric internal-swords (specification)
  (:documentation "current swords of SPECIFICATION"))

(defgeneric (setf internal-swords) (val specification)
 (:documentation "writer on current swords of SPECIFICATION"))

(defgeneric swords (specification)
  (:documentation "list of defined swords in SPECIFICATION"))

(defgeneric (setf swords) (val specification)
 (:documentation "writer on the list of defined swords in SPECIFICATION"))

(defmethod empty-sword ()
  (make-sword ()))

(defvar *internal-swords* (make-hash-table :test #'equal))

(defmethod internal-swords ((spec (eql nil)))
  *internal-swords*)

(defun make-swords () (make-hash-table :test #'equal))

(defclass sword (word s-mixin) ())

(defmethod elementaryp ((sword sword))
  (declare (ignore sword))
  (values t t))

(defmethod directedp ((sword sword))
  (declare (ignore sword))
  (values t t))

(defmethod name ((sword sword))
  (let ((*show-mark* t))
    (format nil "~A" sword)))

(defgeneric make-letter-mark-pairs (alphabet)
  (:documentation "makes a list of pairs ((l1 . 0) (l2 . 1) ... (ln . n-1))"))

(defmethod make-letter-mark-pairs ((alphabet alphabet))
  (mapcar (lambda (letter) (cons letter (- (granularity letter) 3)))
	  (letters alphabet)))
      
(defun numbered-sletters (sletters &optional (pairs nil))
  (unless pairs
    (setf pairs (make-letter-mark-pairs (alphabet-of sletters))))
  (mapcar (lambda (sletter)
	      (make-sletter
	       (mapcar
		(lambda (mletter)
		    (let ((pair (assoc mletter pairs
				       :test (lambda (il p)
						 (equiv-named-letters il p))))
			  (letter (letter mletter)))
		      (make-mletter letter
				    (incf (cdr pair) (- 3 (granularity letter))))))
		(letters sletter))))
	  sletters))

(defun correct-sletters (sletters)
  (let ((alphabet (alphabet-of sletters)))
    (every
     (lambda (letter)
       (correct-mletters sletters letter))
     (letters alphabet))))

(defun make-sword-n (sletters &key (number t) (pairs nil))
;;  (when (endp sletters)
;;      (return-from make-sword-n (empty-sword)))
  (assert (every (lambda (e) (typep e 'sletter)) sletters))
  (setf sletters (remove-if #'empty-sletterp sletters))
  (when number
      (setf sletters (numbered-sletters sletters pairs)))
  (assert (correct-sletters sletters))
  (or (gethash sletters (internal-swords *spec*))
      (setf (gethash sletters (internal-swords *spec*))
	    (make-instance 'sword
			   :name nil
			   :letters sletters
			   :lettertype 'sletter
			   :cardinality 1
			   :wordtype 'sword))))

(defmethod args ((sword sword))
  (mapcar (lambda (sletter) (make-sword-n (list sletter)))
	  (letters sword)))

(defgeneric get-letter-mark-pairs (malphabet &optional max)
  (:documentation 
    "liste d'association contenant les paires 
     (letter . mark-max-ou-min) pour les letters du MALPHABET"))

(defmethod get-letter-mark-pairs ((malphabet alphabet) &optional (max t)) 
  (let* ((mletters (letters malphabet))
	 (letters (letters-from-mletters mletters)))
    (mapcar (lambda (letter)
		(cons letter
		      (reduce
		       (if max
			   #'max
			   #'min)
		       (mapcar
			#'mark
			(remove-if-not
			 (lambda (mletter)
			   (equiv-named-letters mletter letter))
			 mletters)))))
	    letters)))

(defun correct-mletters (sletters letter)
  (let ((marks 
	 (mapcar
	  #'mark
	  (mappend
	   #'letters
	   (mapcar
	    (lambda (sletter)
	      (projection
	       sletter
	       (make-alphabet
		(list letter))))
	    sletters)))))
    (and (equal marks (remove-duplicates marks :test #'=))
	 (equal marks (sort (copy-list marks) #'<)))))

(defmethod swordp ((x t))
  (or (empty-wordp x)
      (typep x 'sword)))

(defun make-sword (sletters)
  (make-sword-n sletters :number nil))

(defmethod set-marks ((sword sword) &optional (pairs nil))
  (make-sword-n (letters sword) :pairs pairs))

(defmethod concatenation ((sword1 sword) (sword2 sword))
;;  (format *error-output* "concatenation swords~%")
  (make-sword (append (letters sword1) (letters sword2))))

(defgeneric true-swordp (sword)
  (:documentation "t if marks of mletters from SWORD start from 0"))

(defmethod true-swordp ((sword sword))
  (let ((mletters (letters (malphabet-of sword)))
	(letters (letters (alphabet-of sword))))
    (every
     #'zerop
     (mapcar
      (lambda (letter)
	(apply #'min
	       (mapcar
		#'mark
		(remove-if-not (lambda (mletter) (eq (letter mletter) letter)) mletters))))
      letters))))

(defmethod mirror ((sword sword))
  (assert (true-swordp sword))
  (make-sword-n (reverse (letters sword))))

(defmethod mirror-p ((sword sword))
  (assert (true-swordp sword))
  (eq sword (mirror sword)))

(defun swordsp (swords)
  (every (lambda (sword) (typep sword 'sword))
	 swords))

(defmethod projection ((sword sword) (alphabet alphabet))
  (let ((sletters (mapcar (lambda (sletter)
			      (projection sletter alphabet))
			  (letters sword))))
    (make-sword (remove-if
		 #'empty-sletterp
		 sletters))))

(defmethod basic-sword ((letter letter))
  (make-sword (list
	       (make-sletter (list (make-mletter letter)))
	       (make-sletter (list (make-mletter letter 1))))))

	     
(defmethod extreme-letters ((sword sword) &optional (last nil))
;;  (assert (not (empty-wordp sword)))
  (if (empty-wordp sword)
      ()
      (if last
	  (last (letters sword))
	  (list (car (letters sword))))))

(defmethod cut-extreme-letter ((sword sword) (letter abstract-letter)
				&optional (last nil))
;;  (format *error-output* "cut-extreme-letter sword~%")
  (if (empty-wordp sword)
      (empty-language sword)
      (if (eq letter (car (extreme-letters sword last)))
	  (make-sword (if last
			  (butlast (letters sword))
			  (cdr (letters sword))))
	  (empty-language sword))))

(defmethod lunion ((sword1 sword) (sword2 sword))
  (format *error-output* "lunion swords~%")
  (if (eq sword1 sword2)
      sword1
      (make-slanguage (list sword1 sword2))))

(defvar *memoize-ljoin-swords* *memoize*)

(defun add-sword (sword swords)
  (setf (gethash (name sword) swords) sword))

(defgeneric complete-with-smaller-sword (sword dsword))

(defmethod complete-with-smaller-sword ((sword sword) (dsword sword))
  (let ((sletters (letters sword))
	(lastmletter (first (letters (car (last (letters dsword)))))))
    (do ((prefix '() (cons (car suffix) prefix))
	 (suffix sletters (cdr suffix)))
	((endp suffix) sletters (format *error-output* "nil complete-with-sword"))
      (let* ((sletter (car suffix))
	     (found (member (letter lastmletter) (letters sletter) :key #'letter)))
	(when found
	  (assert (< (mark lastmletter) (mark (car found))))
	  (return-from complete-with-smaller-sword
	    (concatenation
	     (if (endp prefix)
		 dsword
		 (mix (make-sword (nreverse prefix))
		      dsword))
	     (make-sword suffix))))))))

(defgeneric complete-with-greater-sword (sword dsword))

(defmethod complete-with-greater-sword ((sword sword) (dsword sword))
  (assert (simple-swordp dsword))
  (let ((sletters (letters sword))
	(firstmletter (first (letters (first (letters dsword))))))
    (do ((prefix '() (cons (car suffix) prefix))
	 (suffix (reverse sletters) (cdr suffix)))
	((endp suffix) sletters (format *error-output* "nil complete-with-greater-sword"))
    (let* ((sletter (car suffix))
	   (found (member (letter firstmletter) (letters sletter) :key #'letter)))
;;      (format *error-output* "sletter ~A mletter ~A found ~A~%" sletter firstmletter found )
    (when found
      (assert (> (mark firstmletter) (mark (car found))))
      (return-from complete-with-greater-sword
	(concatenation
	 (make-sword (reverse suffix))
	 (if (endp prefix)
	     dsword
	     (mix
	      dsword
	      (make-sword prefix))))))))))

(defgeneric complete-with-sword (sword dsword)
  (:documentation "returns a language obtained by adding DSWORD to SWORD"))

(defmethod complete-with-sword ((sword sword) (dsword sword))
  (let* ((mletter (first (letters (car (letters dsword)))))
	 (mark (mark mletter))
	 (mletter-found (car (member (letter mletter) (letters (malphabet-of sword)) :key #'letter))))
    (if (and mletter-found (<= mark (mark mletter-found)))
	(complete-with-smaller-sword sword dsword)
	(complete-with-greater-sword sword dsword))))

(defmethod complete-with-sword ((language language) (dsword sword))
  (reduce #'lunion 
	  (mapcar (lambda (sword) (complete-with-sword sword dsword))
		  (words language))))

(defgeneric complete-with-mletters (language malphabet)
  (:documentation "completes every word of LANGUAGE with mletters of MALPHABET"))

(defmethod complete-with-mletters ((language language) (malphabet alphabet))
  (let ((swords (delanoy-args (projection malphabet (alphabet-of language))))
	(language language))
    (dolist (sword swords language)
      (setf language (complete-with-sword language sword)))))

(defgeneric simple-swordp (sword)
  (:documentation "SWORD contains simple sletters only"))

(defmethod simple-swordp ((sword sword))
  (let ((sletters (letters sword)))
    (every #'simple-sletterp sletters)))

(defgeneric delanoy-args (malphabet)
  (:documentation
   "list of swords defining explicitely the implicit relations between the mletters of MALPHABET"))

(defmethod delanoy-args ((malphabet alphabet))
 (let* ((mletters (letters malphabet))
        (letters (letters (make-alphabet (letters-from-mletters mletters)))))
   (mapcar (lambda (letter)
               (make-sword
                (mapcar
                 (lambda (mletter) (make-sletter (list mletter)))
                 (remove-if-not (lambda (mletter)
                                    (equiv-named-letters mletter letter))
                                mletters))))
           letters)))

(defun write-sword (sword stream)
  (format stream "Sword ~A~%" sword))

(defun write-current-sword (stream)
  (write-sword (sword *spec*) stream))

(defun save-sword (file)
  (back-up-file (absolute-filename file))
  (with-open-file (foo (absolute-filename file) :direction :output)
    (if foo
        (progn
          (write-current-sword foo)
          (format t "Sword saved~%"))
        (prog1
            nil
          (format *error-output* "unable to open ~A~%" file)))))

(defgeneric shift-sword-sletter (sword sletter)
  (:documentation "increases by 1 marks of letters in SLETTER"))

(defmethod shift-sword-sletter ((sword sword) (sletter sletter))
  (let* ((malpha (malphabet-of sword))
	 (pairs (get-letter-mark-pairs malpha nil))
	 (letters (letters (alphabet-of sletter))))
    (mapc (lambda (pair)
	    (unless (member (car pair) letters)
	      (decf (cdr pair))))
	  pairs)
    (make-sword-n (letters sword) :number t :pairs pairs)))

(defmethod contains-empty-sword ((sword sword))
  (empty-wordp sword))
