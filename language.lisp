(in-package :smots)

(defgeneric slanguage (specification)
  (:documentation "current slanguage of SPECIFICATION"))

(defgeneric (setf slanguage) (val specification)
 (:documentation "writer on current slanguage of SPECIFICATION"))

(defgeneric slanguages (specification)
  (:documentation "list of defined slanguages in SPECIFICATION"))

(defgeneric (setf slanguages) (val specification)
 (:documentation "writer on the list of defined slanguages in SPECIFICATION"))

(defun make-slanguages () (make-hash-table :test #'equal))

(defclass language (alanguage)
  ((words :initform nil :accessor words :initarg :words)))

(defclass slanguage (s-mixin language) ())

(defmethod wordtype ((slanguage slanguage))
  'sword)

(defmethod languagetype ((s-mixin s-mixin))
  'slanguage)

(defmethod compute-cardinality ((language language))
  (length (words language)))

(defmethod initialize-instance :after
    ((language language) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (setf (cardinality language)
	(compute-cardinality language)))

(defun languagep (o)
  (typep o 'language))

(defmethod print-object ((language language) stream)
  (format stream "{")
  (display-sequence (words language) stream :sep ", ")
  (format stream "} "))

(defun make-language (words &key wordtype languagetype (name "unamed"))
  (let ((words (remove-duplicates words :from-end t :test #'eq)))
    (if (and words (null (cdr words)))
	(car words)
	(make-instance languagetype
		       :words words
		       :name name
		       :wordtype wordtype
		       :languagetype languagetype))))

(defun make-slanguage (swords)
  (make-language swords :wordtype 'sword :languagetype 'slanguage))

(defgeneric empty-language (alanguage)
  (:documentation "creates empty language according to wordtype"))

(defmethod empty-language ((alanguage alanguage))
  (make-instance
   (languagetype alanguage)
   :name "empty"
   :wordtype (wordtype alanguage)))

(defgeneric empty-slanguage ()
  (:documentation "creates empty slanguage"))

(defmethod empty-slanguage ()
  (make-instance 'slanguage :name "empty" :wordtype 'sword))

(defun empty-languagep (l)
  (and (languagep l) (endp (words l))))

(defmethod lintersection ((language1 language) (language2 language))
  (lgeneric-bool language1 language2 #'intersection))

(defmethod lunion ((language1 language) (language2 language))
  (lgeneric-bool language1 language2 #'union))

(defgeneric lset-difference (language1 language2)
  (:documentation "difference of LANGUAGE1 and LANGUAGE2"))

(defmethod lset-difference ((language1 language) (language2 language))
  (lgeneric-bool language1 language2 #'set-difference))

(defgeneric lconcatenation (language1 language2)
  (:documentation "concatenation of LANGUAGE1 and LANGUAGE2"))

(defmethod lconcatenation ((language1 language) (language2 language))
  (lgeneric language1 language2 #'concatenation))

(defmethod mirror ((language language))
  (make-language
   (mapcar #'mirror (words language))
   :wordtype (wordtype language)
   :languagetype (type-of language)
   :name (concatenate 'string "mirror-of-" (name language))))

(defmethod mirror-p ((language language))
  (equivalent language (mirror language)))

(defgeneric mirror-closure (language)
  (:documentation "closure mirror"))

(defmethod mirror-closure ((language language))
  (let ((closure 
	 (lunion language (mirror language))))
    (setf (name closure) (concatenate 'string "mirror-closure-of-" (name language)))
    closure))

(defgeneric mix (language1 language2)
  (:documentation "concatenation of LANGUAGE1 and LANGUAGE2"))

(defmethod mix :before (language1 language2)
  (assert (disjoint-alphabets (list language1 language2))))

(defmethod brute-mix :before (language1 language2)
  (assert (disjoint-alphabets (list language1 language2))))

(defmethod mix ((language1 language) (language2 language))
  (when (empty-languagep language1)
    (return-from mix language1))
  (when (empty-languagep language2)
    (return-from mix language1))
  (lgeneric language1 language2 #'brute-mix))

(defmethod brute-join ((language1 language) (language2 language))
  (when (empty-languagep language1)
    (return-from brute-join language1))
  (when (empty-languagep language2)
    (return-from brute-join language1))
  (lgeneric language1 language2 #'brute-join))

(defmethod extreme-letters ((language language) &optional (last nil))
;;  (format *error-output* "extreme-letters language~%")
  (extreme-letters (words language) last))

(defmethod equivalent ((language1 language) (language2 language))
  (let* ((words1 (words language1))
	 (words2 (words language2))
	 (ex-or (set-exclusive-or  words1 words2 :test #'eq)))
    (values (null ex-or) (car ex-or))))
;;    (and (subsetp words1 words2) (subsetp words2 words1))))

(defmethod computed ((language language))
  t)

(defmethod lgeneric-bool ((language1 language) (language2 language) boolfun)
  (make-language
   (words-generic-bool (words language1) (words language2) boolfun)
   :wordtype (wordtype language1)
   :languagetype (languagetype language1)))
  
(defmethod lgeneric ((language1 language) (language2 language) fun)
  (words-generic (words language1) (words language2) fun))

;;  (make-language
;;   :wordtype(wordtype language1)
;;   :languagetype (languagetype language1))

(defmethod ljoin ((language1 language) (language2 language))
;;  (format *error-output* "ljoin language langage ~%")
  (lgeneric language1 language2 #'ljoin))
 
(defmethod compute-language ((language language))
;;  (format *error-output* "compute-language language~%")
  language)

(defmethod compute-prefix-language ((language language) (n integer))
;;  (format *error-output* "compute-prefix-language language~%")
  (make-language
   (remove-if (lambda (word) (> (wlength word) n))
	      (words language))
   :wordtype (wordtype language)
   :languagetype (languagetype language)))

(defmethod cut-extreme-letter ((lexprs list) (letter abstract-letter) &optional (last nil))
;;  (format *error-output* "cut-extreme-letter list~%")
  (mapcar (lambda (lexpr)
	    (cut-extreme-letter lexpr letter last))
	  lexprs))

(defmethod cut-extreme-letter ((language language) (letter abstract-letter) &optional (last nil))
;;  (format *error-output* "cut-extreme-letter language")
  (make-language
   (remove-if #'empty-languagep
	      (cut-extreme-letter (words language) letter last))
   :wordtype (wordtype language)
   :languagetype (languagetype language)))

(defmethod concatenation ((language1 language) (language2 language))
  (lgeneric language1 language2 #'concatenation))

(defmethod priority ((language language))
  6)

(defmethod weight ((language language))
  (declare (ignore language))
  1)

(defmethod projection ((language language) (alphabet alphabet))
  (let ((words (projection (words language) alphabet)))
    (make-language
     words
     :wordtype (wordtype language)
     :languagetype (type-of language)))) 

(defun add-language (language languages)
  (setf (gethash (name language) languages) language))

(defun add-current-slanguage ()
  (add-language (slanguage *spec*) (slanguages *spec*)))

(defun set-current-slanguage (language)
  (setf (slanguage *spec*) language)
  (add-current-slanguage))

(defun write-slanguage (slanguage stream)
  (format stream "Slanguage ~A~%" (name slanguage))
  (unless (empty-wordp slanguage)
    (format stream "~A~%" slanguage)))
  
(defun write-current-slanguage (stream)
  (write-slanguage (slanguage *spec*) stream))

(defun save-slanguage (file)
  (back-up-file (absolute-filename file))
  (with-open-file (foo (absolute-filename file) :direction :output)
    (if foo
        (progn
          (write-current-slanguage foo)
          (format *standard-output* "Slanguage saved~%"))
        (prog1
            nil
          (format *error-output* "unable to open ~A~%" file)))))

(defmethod compute-prefix-language :around ((alanguage alanguage) (n integer))
  (format *error-output* "compute-prefix-language around~%")
  (if (zerop n)
      (if (contains-empty-sword alanguage)
	  (empty-sword)
	  (empty-language alanguage))
      (call-next-method)))

(defmethod gen-calphabet-of ((language language) lettertype)
;;  (format *error-output* "gen-calphabet-of language t")
  (gen-calphabet-of (words language) lettertype))
