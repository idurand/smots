(in-package :smots)

(defvar *special-chars* (list #\- #\( #\) #\> #\)))

(defgeneric names (specification)
  (:documentation "list of all the names defined in SPECIFICATION"))

(defgeneric (setf names) (val specification)
  (:documentation "writer on the list of names defined in SPECIFICATION"))

(defvar *internal-names* nil)

(defmethod names ((spec (eql nil)))
  *internal-names*)

(defmethod (setf names) (val (spec (eql nil)))
  (setf *internal-names* val))

(defun make-name (name)
  (or
    (find-if (lambda (n) (equal n name)) (names *spec*))
    (progn
      (push name (names *spec*))
      name)))

(defun strong-name (name)
   (if (intersection (coerce name 'list) *special-chars*)
       (format nil "\"~A\"" name)
       (format nil "~A" name)))

(defun write-name (name stream)
  (format stream (strong-name name)))
