(in-package :smots)

(defgeneric left-handside (rule)
  (:documentation "returns the left-handside of RULE"))

(defgeneric right-handside (rule)
  (:documentation "returns the right-handside of RULE"))

(defclass rule ()
    ((lh :initarg :lh :accessor left-handside)
     (rh :initarg :rh :accessor right-handside)))

(defun make-rule (lh rh)
  (make-instance 'rule :lh lh :rh rh))

(defmethod print-object ((rule rule) stream)
  (format stream "~A -> ~A"  (left-handside rule) (right-handside rule)))
  
(defgeneric equivalent-rules-p (rule1 rule2)
  (:documentation "T if RULE1 and RULE2 are equivalent"))

(defclass wrule (rule) ())

(defun make-wrule (lh rh)
  (assert (and (swordp lh) (swordp rh)))
  (make-instance 'wrule :lh lh :rh rh))

(defun wrulep (o)
  (typep o 'wrule))

(defclass rules ()
  ((rules-list :initarg :rules-list :reader rules-list)))

(defun make-rules (rules)
  (make-instance 'rules :rules-list (remove-duplicates rules :test #'equivalent-rules-p)))

(defmethod print-object ((rules rules) stream)
  (mapc (lambda (rule) (format stream "~A~%" rule))
	(rules-list rules)))

(defclass wrules (rules) ())

(defun make-wrules (rules)
  (assert (every #'wrulep rules))
  (make-instance 'wrules :rules-list (remove-duplicates rules :test
							#'equivalent-rules-p)))

(defmethod equivalent-rules-p ((wrule1 wrule) (wrule2 wrule))
  (and (eq (left-handside wrule1) (left-handside wrule2))
       (eq (right-handside wrule1) (right-handside wrule2))))

(defgeneric split-sletters (sletters lh n)
  (:documentation
   "try to recognize LH at position N; if LH is recognize 
    a list containing the sletters before LH and the sletters after LH
    is returned, NIL otherwise"))

(defmethod split-sletters ((sletters list) (lh list) n)
  (let* ((prefixe '())
	 (suffixe '())
	 (llh (length lh))
	 (lsls (length sletters)))
    (loop
     for i from 0 to (1- n)
     do (push (pop sletters) prefixe))
    (loop
     for i from n to (1- (+ n llh))
     do (if (eq (car sletters) (car lh))
	    (progn
	      (pop sletters)
	      (pop lh))
	    (return-from split-sletters nil)))
    (loop
     for i from (+ n llh) to (1- lsls)
     do (progn
	  (push (car sletters) suffixe)
	  (setf sletters (cdr sletters))))
    (list (nreverse prefixe) (nreverse suffixe))))

(defgeneric apply-rule-to-word (word rule)
  (:documentation 
   "the list of words which can be obtained 
    by one step rewriting on WORD using RULE"))
	    
(defmethod apply-rule-to-word ((word word) (rule rule))
  (let* ((sletters (letters word))
	 (lh (letters (left-handside rule)))
	 (llh (length lh))
	 (lsls (length sletters))
	 (res '()))
    (dotimes (i (1+ (- lsls llh)) (nreverse res))
      (let ((pair (split-sletters sletters lh i)))
	(when pair
	  (push (make-sword (append (car pair)
				    (letters (right-handside rule))
				    (cadr pair)))
		res))))))
			    
(defgeneric apply-rules-to-word (word rule)
  (:documentation 
   "the list of words which can be obtained 
    by one step rewriting on WORD using the rules in RULE"))

(defmethod apply-rules-to-word ((word word) (rules rules))
  (remove-duplicates
   (mappend
    #'(lambda (rule)
	(apply-rule-to-word word rule))
    (rules-list rules))))

(defgeneric apply-rules-to-words (words rules)
  (:documentation 
   "the list of words which can be obtained 
    by one step rewriting on a word of WORDS using RULES"))

(defmethod apply-rules-to-words ((words list) (rules rules))
  (remove-duplicates
   (mappend
    #'(lambda (word)
	(apply-rules-to-word word rules))
    words)))

