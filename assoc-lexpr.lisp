(in-package :smots)

(defvar *trace-clean-args* nil)

(defclass assoc-lexpr (op-lexpr memo-mixin)
  ((args :initarg :args :accessor args))
  (:documentation "an expression with an operator and arguments representing a language"))

(defun assoc-lexprp (o)
  (typep o 'assoc-lexpr))

(defgeneric args (assoc-lexpr)
  (:documentation 
    "the ordered list of the arguments of the NARY-LEXPR"))

(defvar *with-paren* t)

;;(defmethod print-object :around ((assoc-lexpr assoc-lexpr) stream)
(defmethod print-object ((assoc-lexpr assoc-lexpr) stream)
  (when *with-paren*
    (format stream "("))
  (let ((*with-paren* t))
    (display-sequence (args assoc-lexpr) stream
		      :sep (format nil " ~A "(lop assoc-lexpr))))
  (when *with-paren*
    (format stream ")")))

(defmethod equivalent ((assoc-lexpr1 assoc-lexpr) (assoc-lexpr2 assoc-lexpr))
  (and (eq (lop assoc-lexpr1)  (lop assoc-lexpr2))
       (equivalent (args assoc-lexpr1) (args assoc-lexpr2))))

(defgeneric simplify-assoc-lexpr (assoc-lexpr)
  (:documentation "simplifies ASSOC-LEXPR when it has 0 or 1 argument"))

(defmethod simplify-assoc-lexpr ((assoc-lexpr assoc-lexpr))
  (let ((args (args assoc-lexpr)))
    (when (endp args) (format *error-output* "null args in simplify-assoc-lexpr~%"))
    (if (endp args)
	(empty-language assoc-lexpr)
	(if (null (cdr args))
	    (car args)
	    assoc-lexpr))))

(defun make-assoc-lexpr (args class wordtype)
  (simplify-assoc-lexpr
   (clean-args ;; fait appel aux proprietes des operateurs
    (make-instance class :name nil :args args :wordtype wordtype))))

(defmethod compute-language ((lexpr assoc-lexpr))
;;  (format *error-output* "compute-language assoc-lexpr~%")
  (reduce (lfun lexpr)
	  (mapcar #'compute-language (args lexpr))))

(defmethod weight ((assoc-lexpr assoc-lexpr))
  (apply #'max (mapcar #'weight (args assoc-lexpr))))

(defmethod complete-with-mletters ((assoc-lexpr assoc-lexpr) (malphabet alphabet))
  (let ((args (args assoc-lexpr)))
    (make-assoc-lexpr
     (mapcar (lambda (arg)
	       (complete-with-mletters arg malphabet))
	     args)
     (class-of assoc-lexpr)
     (wordtype assoc-lexpr))))

(defmethod projection ((assoc-lexpr assoc-lexpr) (alphabet alphabet))
  (let ((args (mapcar (lambda (arg) (projection arg alphabet))
		       (args assoc-lexpr))))
    (make-assoc-lexpr args (class-of assoc-lexpr) (wordtype assoc-lexpr))))

(defgeneric assoc-simplify-args-rec (args lop)
  (:documentation "simplifications due to associativity"))

(defmethod assoc-simplify-args-rec ((args list) (lop lop))
  (cond
    ((endp args)
     '())
    ((eq (lop (car args)) lop)
     (assoc-simplify-args-rec (append (args (car args)) (cdr args)) lop))
    (t (cons (car args) (assoc-simplify-args-rec (cdr args) lop)))))

;;; les clean-args des autres propriétés (commutativite, idempotence, bool)
;;; sont appelés en méthodes before et after
(defmethod clean-args ((lexpr assoc-lexpr))
  (when *trace-clean-args*
    (format *error-output* "clean-args assoc-lexpr ~A~%" lexpr))
  (let* ((args (args lexpr))
	 (newargs (assoc-simplify-args-rec
		   args
		   (lop lexpr))))
      (setf (args lexpr) newargs))
    lexpr)

(defmethod projection ((assoc-lexpr assoc-lexpr) (alphabet alphabet))
  (let ((args (mapcar (lambda (arg) (projection arg alphabet))
		       (args assoc-lexpr))))
    (make-assoc-lexpr args (class-of assoc-lexpr) (wordtype assoc-lexpr))))

(defmethod compute-cardinality ((assoc-lexpr assoc-lexpr))
  (and
   (cfun assoc-lexpr)
   (let ((args (mapcar #'compute-cardinality (args assoc-lexpr))))
     (and (every #'identity args)
	  (reduce (cfun assoc-lexpr) args)))))

(defmethod compute-prefix-language ((assoc-lexpr assoc-lexpr) (n integer))
  (let ((args (mapcar (lambda (arg)
			(compute-prefix-language arg  n))
		      (args assoc-lexpr))))
    (compute-prefix-language (reduce (lfun assoc-lexpr) args) n)))
