(in-package :smots)
;;(init)
;;; some swords
(defparameter *wa* (input-sword-n "[{a}]"))
(defparameter *wb* (input-sword-n "[{b}]"))
(defparameter *wc* (input-sword-n "[{c}]"))
(defparameter *wd* (input-sword-n "[{d}]"))
(defparameter *we* (input-sword-n "[{e}]"))
(defparameter *wf* (input-sword-n "[{f}]"))

(defparameter *wa-a* (input-sword-n "[{a}{a}]"))
(defparameter *wb-b* (input-sword-n "[{b}{b}]"))
(defparameter *wc-c* (input-sword-n "[{c}{c}]"))
(defparameter *wd-d* (input-sword-n "[{d}{d}]"))
(defparameter *we-e* (input-sword-n "[{e}{e}]"))
(defparameter *wf-f* (input-sword-n "[{f}{f}]"))

(defparameter *wac* (input-sword-n "[{a,c}]"))
(defparameter *wab* (input-sword-n "[{a,b}]"))
(defparameter *wbc* (input-sword-n "[{b,c}]"))
(defparameter *wa-b* (input-sword-n "[{a}{b}]"))
(defparameter *wabe* (input-sword-n "[{a,b,e}]"))
(defparameter *wabc* (input-sword-n "[{a,b,c}]"))

(defparameter *wbdf* (input-sword-n "[{b,d,f}]"))
(defparameter *wde* (input-sword-n "[{d,e}]"))

(defparameter *contrainte1*
  (make-concatenation-n
   (list
    *wabe*
    (make-mix (list *wa* *wb* *we*)))))

(defparameter *contrainte2*
  (make-concatenation-n
   (list
    (make-mix (list *wa* *wb*))
    *wa*
    *wb*)))

(defparameter *contrainte3*
  (make-lunion
   (list
    (make-concatenation-n
     (list
      (make-mix (list *wa* *wc*))
      *wa*
      *wc*
      *wd-d*))
    (make-concatenation-n
     (list 
      (make-mix (list *wa* *wc*))
      *wac*
      *wd-d*)))))

(defparameter *contrainte4*
  (make-concatenation-n
   (list
    *wb*
    *wbdf*
    (make-mix (list *wf* *wd*)))))

(defparameter *contrainte5*
  (make-concatenation-n
   (list
    (make-mix (list *we* *wd*))
    *wde*)))

(defparameter *pb1* nil)
(defparameter *pb2* nil)
(defparameter *pb3* nil)
(defparameter *pb4* nil)
(defparameter *pb5* nil)

;; SMOTS> *contrainte1*
;; [{a0,b0,e0}] . ([{a1}] X [{b1}] X [{e1}])
;; SMOTS> *contrainte2*
;; ([{a0}] X [{b0}]) . [{a1}{b1}]
;; SMOTS> *contrainte3*
;; (([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) U (([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}])
;; SMOTS> *contrainte4*
;; [{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])
;; SMOTS> *contrainte5*
;; ([{e0}] X [{d0}]) . [{d1,e1}]

(setf *pb1* *contrainte1*)
;; cardinality 13
(setf *pb2* (ljoin *contrainte2* *contrainte1*))
;; cardinality 5
(setf *pb3* (ljoin *contrainte3* *pb2*))
;; cardinality 378
(setf *pb4* (ljoin *contrainte4* *pb3*))
;; cardinality 180
(setf *pb5* (ljoin *contrainte5* *pb4*))
;; cardinality 18

;; SMOTS> *pb1*
;; [{a0,b0,e0}] . ([{a1}] X [{b1}] X [{e1}])
;; SMOTS> *pb2*
;; [{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])
;; SMOTS> *pb3*
;; ((([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}]))) U ((([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])))
;; SMOTS> *pb4*
;; (([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}]))) U (([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])))
;; SMOTS> *pb5*
;; ((([{e0}] X [{d0}]) . [{d1,e1}]) J ([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}]))) U ((([{e0}] X [{d0}]) . [{d1,e1}]) J ([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])))

;; SMOTS> (compute-language *pb5*)
;; {[{a0,b0,e0}{c0}{a1}{c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,e0}{c0}{a1,c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,c0,e0}{a1,c1}{b1,d0,f0}{f1}{d1,e1}], [{c0}{a0,b0,e0}{a1,c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,e0}{c0}{a1,c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,c0,e0}{a1,c1}{b1,d0,f0}{d1,e1,f1}], [{c0}{a0,b0,e0}{a1,c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,e0}{c0}{a1,c1}{b1,d0,f0}{d1,e1}{f1}], [{a0,b0,c0,e0}{a1,c1}{b1,d0,f0}{d1,e1}{f1}], [{c0}{a0,b0,e0}{a1,c1}{b1,d0,f0}{d1,e1}{f1}], [{a0,b0,c0,e0}{a1}{c1}{b1,d0,f0}{f1}{d1,e1}], [{c0}{a0,b0,e0}{a1}{c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,e0}{c0}{a1}{c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,c0,e0}{a1}{c1}{b1,d0,f0}{d1,e1,f1}], [{c0}{a0,b0,e0}{a1}{c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,e0}{c0}{a1}{c1}{b1,d0,f0}{d1,e1}{f1}], [{a0,b0,c0,e0}{a1}{c1}{b1,d0,f0}{d1,e1}{f1}], [{c0}{a0,b0,e0}{a1}{c1}{b1,d0,f0}{d1,e1}{f1}]} 

;; reflechir a first-letters d'un join
;; memoization des formats de mix et obtention d'un nouveau mix par subsitution

;; j'ai comment� les ljoin de bool-mixin pour empecher
;; que les unions descendent

;; changer mix (sword sword)
;; faire de la m�moisation
;; proc�der par substitution de mix d�j� calcul� d�s que c'est possible.

;; voir une notion d'ordre sur les expression et ne pas remplacer
;; une expression par une plus compliqu�e
;; arrive-t'on au bout des trains avec *level-ljoin*=0

;; transferer les test pour faire ou pas l'intersection dans lintersection

;; 23/01/2007
;; tester (compute-language (make-mix (list *wa* *wb*)))




(setf *pb4* (ljoin *contrainte4* *pb3*))
;; cardinality 180
(setf *pb5* (ljoin *contrainte5* *pb4*))
;; cardinality 18

;; SMOTS> *pb1*
;; [{a0,b0,e0}] . ([{a1}] X [{b1}] X [{e1}])
;; SMOTS> *pb2*
;; [{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])
;; SMOTS> *pb3*
;; ((([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}]))) U ((([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])))
;; SMOTS> *pb4*
;; (([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}]))) U (([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])))
;; SMOTS> *pb5*
;; ((([{e0}] X [{d0}]) . [{d1,e1}]) J ([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1}{c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}]))) U ((([{e0}] X [{d0}]) . [{d1,e1}]) J ([{b0}{b1,d0,f0}] . ([{f1}] X [{d1}])) J (([{a0}] X [{c0}]) . [{a1,c1}{d0}{d1}]) J ([{a0,b0,e0}] . ([{a1}{b1}] X [{e1}])))

;; SMOTS> (compute-language *pb5*)
;; {[{a0,b0,e0}{c0}{a1}{c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,e0}{c0}{a1,c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,c0,e0}{a1,c1}{b1,d0,f0}{f1}{d1,e1}], [{c0}{a0,b0,e0}{a1,c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,e0}{c0}{a1,c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,c0,e0}{a1,c1}{b1,d0,f0}{d1,e1,f1}], [{c0}{a0,b0,e0}{a1,c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,e0}{c0}{a1,c1}{b1,d0,f0}{d1,e1}{f1}], [{a0,b0,c0,e0}{a1,c1}{b1,d0,f0}{d1,e1}{f1}], [{c0}{a0,b0,e0}{a1,c1}{b1,d0,f0}{d1,e1}{f1}], [{a0,b0,c0,e0}{a1}{c1}{b1,d0,f0}{f1}{d1,e1}], [{c0}{a0,b0,e0}{a1}{c1}{b1,d0,f0}{f1}{d1,e1}], [{a0,b0,e0}{c0}{a1}{c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,c0,e0}{a1}{c1}{b1,d0,f0}{d1,e1,f1}], [{c0}{a0,b0,e0}{a1}{c1}{b1,d0,f0}{d1,e1,f1}], [{a0,b0,e0}{c0}{a1}{c1}{b1,d0,f0}{d1,e1}{f1}], [{a0,b0,c0,e0}{a1}{c1}{b1,d0,f0}{d1,e1}{f1}], [{c0}{a0,b0,e0}{a1}{c1}{b1,d0,f0}{d1,e1}{f1}]} 

;; SMOTS> (set-difference (words  (compute-language *ei*))  (words (lintersection *l1* *l2*)))
;; already computed
;; ([{a,b,e}{c}{a}{b,e}{d}{d}{c}] [{a,b,e}{c}{a}{b,e}{d}{c,d}]
;;  [{a,b,e}{c}{a}{b,e}{d}{c}{d}] [{a,b,e}{c}{a}{b,e}{c,d}{d}]
;;  [{c}{a,b,e}{a}{b,e}{d}{d}{c}] [{c}{a,b,e}{a}{b,e}{d}{c,d}]
;;  [{c}{a,b,e}{a}{b,e}{d}{c}{d}] [{c}{a,b,e}{a}{b,e}{c,d}{d}]
;;  [{a,b,c,e}{a}{b,e}{c,d}{d}] [{a,b,c,e}{a}{b,e}{d}{c}{d}]
;;  [{a,b,c,e}{a}{b,e}{d}{c,d}] [{a,b,c,e}{a}{b,e}{d}{d}{c}])
;; SMOTS> *ex1*
;; ([{a,b,e}] . ([{a}{b}] X [{e}])) X [{c}{c}] X [{d}{d}]
;; SMOTS> *ex2*
;; ((([{a}] X [{c}]) . [{a}{c}{d}{d}]) U (([{a}] X [{c}]) . [{a,c}{d}{d}])) X [{b}{b}] X [{e}{e}]
;; SMOTS> (cardinality *l1*)
;; 10577
;; SMOTS> (cardinality *l2*)
;; 39502
;; SMOTS> (cardinality (lintersection *l1* *l2*))
;; 360
;; SMOTS> (defparameter *ei* (lintersection *ex1* *ex2*))
;; *EI*
;; SMOTS> (cardinality (compute-language *ei*))
;; 372
;; SMOTS> (defparameter *p1* (projection *ex1* (make-alphabet (some-letters "a" "b" "c" "e" ))))
;; *P1*
;; SMOTS> (defparameter *p2* (projection *ex2* (make-alphabet (some-letters "a" "b" "c" "e" ))))
;; *P2*
;; SMOTS> (check-op *p1* *p2* #'lintersection)
