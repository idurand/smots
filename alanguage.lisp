(in-package :smots)

(defvar *simplify-concatenation* t)

(defgeneric empty-wordp (o)
  (:documentation ""))

(defgeneric wordtype (o)
  (:documentation ""))

(defgeneric languagetype (o)
  (:documentation ""))

(defclass alanguage (named-object alpha-object) 
  ((cardinality
    :initarg :cardinality
    :accessor cardinality
    :initform nil)
   (wordtype :reader wordtype :initarg :wordtype :initform 'word))
  (:documentation "abstract class for languages"))

(defgeneric cardinality (alanguage)
  (:documentation "gives the number of words in ALANGUAGE when easy to compute, NIL otherwise"))

(defgeneric compute-cardinality (alanguage)
  (:documentation "compute the number of words in ALANGUAGE when easy or NIL"))

(defgeneric lintersection (language1 language2)
  (:documentation "intersection of LANGUAGE1 and LANGUAGE2"))

(defgeneric lunion (language1 language2)
  (:documentation "union of LANGUAGE1 and LANGUAGE2"))

(defgeneric lgeneric (language1 language2 fun)
  (:documentation "fun of LANGUAGE1 and LANGUAGE2"))

(defgeneric lgeneric-bool (language1 language2 boolfun)
  (:documentation "boolfun of LANGUAGE1 and LANGUAGE2"))

(defgeneric equivalent (language1 language2)
  (:documentation "comparaison of LANGUAGE1 and LANGUAGE2")
  (:method ((l1 alanguage) (l2 alanguage))
    (eq l1 l2))
  (:method ((languages1 list) (languages2 list))
    (every (lambda (arg1 arg2) (equivalent arg1 arg2))
	   languages1
	   languages2)))

(defgeneric ljoin (sexpr1 sexpr2)
  (:documentation "ljoin de deux slangages"))

(defgeneric mirror (language)
  (:documentation "mirror of LANGUAGE"))

(defgeneric priority (alanguage)
  (:documentation "priority of ALANGUAGE"))

(defgeneric factor-extreme-letter (alanguage &optional last)
  (:documentation
   "transform ALANGUAGE into a union of concatenations whose
    first (or last) argument is a possible first (or last) letter"))

(defgeneric factor-first-letter (alanguage)
  (:documentation
   "transform ALANGUAGE into a union of concatenations whose
    first argument is a possible first letter"))

(defgeneric factor-last-letter (alanguage)
  (:documentation
   "transform ALANGUAGE into a union of concatenations whose
    last argument is a possible last letter"))

(defgeneric reconstruct (alanguage)
  (:documentation "reconstruct ALANGUAGE according global parameters")
;;; by default, nothing
  (:method ((alanguage alanguage)) alanguage))

(defgeneric factorize (alanguage)
  (:documentation "try to factorize ALANGUAGE into a simpler expression")
  (:method ((alanguage alanguage)) alanguage))

(defun alanguagep (o)
  (typep o 'alanguage))

(defmethod priority ((alanguage alanguage)) 6)

(defgeneric order-according-priority (alanguages)
  (:documentation "order a list of ALANGUAGE according to the priority of their operators")
  (:method ((args list))
    (sort (copy-list args) #'>
	  :key #'(lambda (arg) (priority arg)))))

(defgeneric extreme-letters (language &optional last)
  (:documentation 
   "set of the first (resp. last) letters of the words in the LANGUAGE"))

(defgeneric compute-language (alanguage)
  (:documentation "compute the set of words represented by ALANGUAGE"))

(defgeneric compute-prefix-language (alanguage n)
  (:documentation "compute the set of words of length <= n of ALANGUAGE"))

(defgeneric first-letters (alanguage)
  (:documentation "the set of letters that appear at the beginning of words in ALANGUAGE"))

(defgeneric last-letters (alanguage)
  (:documentation "the set of letters that appear at the end of words in ALANGUAGE"))

(defmethod first-letters (o)
  (extreme-letters o))

(defmethod last-letters (o)
  (extreme-letters o 'last))

(defgeneric computed (alanguage)
  (:documentation "T if ALANGUAGE is computed in extension"))

(defgeneric cut-extreme-letter (alanguage letter &optional last)
  (:documentation "residual of ALANGUAGE by LETTER (from end or from beginning"))

(defgeneric cut-first-letter (alanguage letter)
  (:documentation "residual of ALANGUAGE by LETTER (from beginning)"))

(defgeneric cut-last-letter (alanguage letter)
  (:documentation "residual of ALANGUAGE by LETTER (from end)"))

(defmethod cut-first-letter ((alanguage alanguage) (letter abstract-letter))
  (cut-extreme-letter alanguage letter))

(defmethod cut-last-letter ((alanguage alanguage) (letter abstract-letter))
  (cut-extreme-letter alanguage letter 'last))

(defmethod extreme-letters ((alanguages list) &optional (last nil))
;;  (format *error-output* "extreme-letters list~%")
  (remove-duplicates
   (mappend #'(lambda (alanguage) (extreme-letters alanguage last))
	    alanguages)))

(defgeneric concatenation (language1 language2)
  (:documentation 
   "concatenation of the 2 languages preserving existing marks"))

(defgeneric concatenation-n (language1 language2)
  (:documentation 
   "concatenation of the 2 languages with numbering"))

(defgeneric ccard (alanguage)
  (:documentation 
   "cardinality of ALANGUAGE; compute ALANGUAGE if not already computed"))

(defmethod ccard ((alanguage alanguage))
  (cardinality (compute-language alanguage)))

(defgeneric residual (alanguage letters)
  (:documentation "residual of a language by a word or a sequence of letters"))

(defgeneric mirror (alanguage)
  (:documentation "the mirror language of ALANGUAGE"))

(defgeneric mirror-p (alanguage)
  (:documentation "true if ALANGUAGE is closed with the mirror operation"))

(defgeneric contains-empty-sword (alanguage)
  (:documentation "t if ALANGUAGE contains [])"))

(defgeneric reduce-lunion-on-prefix-languages (fsls sexpr n)
  (:documentation ""))

(defmethod residual ((alanguage alanguage) (letters list))
  (if (endp letters)
      alanguage
      (residual (cut-first-letter alanguage (car letters)) (cdr letters))))

(defmethod elementaryp ((alanguage alanguage))
  (declare (ignore alanguage))
  ;; second value will be T when the first value is sure
  (values nil nil))

(defmethod directedp ((alanguage alanguage))
  ;; if T just one value relevant
  ;; if nil second value will be T when the first value nil is sure
  (or (elementaryp alanguage) (values nil nil)))

;; by default we compute the whole language
;; we must try to do better when possible
(defmethod compute-prefix-language ((lexpr alanguage) (n integer))
;;  (format *error-output* "compute-prefix-language op-lexpr~%")
  (let ((l (compute-language lexpr)))
    (when l
      (compute-prefix-language l n))))

(defgeneric gen-calphabet-of (object lettertype)
  (:documentation "constructs and returns the alphabet of the letters of LETTERTYPE present in OBJECT")
  (:method ((o t) lettertype) (make-alphabet () lettertype))
;; pour se debarraser du cas des lettres de type xlettertype
  (:method :around ((alpha-object alpha-object) lettertype)
    (if (eq lettertype (type-of alpha-object))
	(make-alphabet (list alpha-object) lettertype)
	(call-next-method))))

;; pour la plupart des objects on doit construire
(defmethod alphabet-of (o)
;;  (format *trace-output* "alphabet-of primary ~A ~%" o)
  (gen-calphabet-of o 'letter))

;; pour les alpha objects memoisation
(defmethod alphabet-of :around ((alpha-object alpha-object))
;;  (format *trace-output* "alphabet-of around ~A ~%" alpha-object)
  (with-slots (alphabet-of) alpha-object
    (or alphabet-of
	(setf alphabet-of (call-next-method)))))

(defgeneric gen-alphabet-of (o lettertype)
  (:method (o (lettertype (eql 'letter)))
    (declare (ignore lettertype))
    (alphabet-of o)))
