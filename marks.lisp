(in-package :smots)

(defgeneric set-marks (language &optional pairs))

;;; descructive

(defun merge-marks (pairs newpairs)
  (assert (equal (mapcar #'car pairs) (mapcar #'car newpairs)))
  (mapc (lambda (pair newpair)
	  (setf (cdr pair) (max (cdr pair) (cdr newpair))))
	pairs
	newpairs))

;;; attention il faudrait que set-marks retourne les max pour
(defmethod set-marks ((assoc-lexpr assoc-lexpr) &optional (pairs nil))
;;  (format *error-output* "set-marks ~A~%" assoc-lexpr)
  (when (eq (type-of assoc-lexpr) 'lunion)
    (format *error-output* "Warning set-marks of a union~A~%" assoc-lexpr))
  (let* ((originalpairs (copy-tree pairs))
	(arg
	 (mapcar
	  (lambda (arg)
	    (let ((newpairs (copy-tree originalpairs)))
	      (prog1 (set-marks arg newpairs)
		(merge-marks pairs newpairs))))
	  (args assoc-lexpr))))
  (make-assoc-lexpr
   arg
   (class-of assoc-lexpr)
   (wordtype assoc-lexpr))))

(defgeneric set-sequential-marks (lexprs &optional pairs)
  (:documentation "numbers the letters of lexprs starting from 0 or from
		    the value indicated in PAIRS"))

(defmethod set-sequential-marks ((lexprs list) &optional (pairs nil))
  (if (endp lexprs)
      '()
      (progn
	(unless pairs
	  (setf pairs (make-letter-mark-pairs (alphabet-of lexprs))))
	(let ((first (set-marks (car lexprs) pairs)))
	  (let ((newpairs (get-letter-mark-pairs (malphabet-of (cdr lexprs)))))
	    (setf newpairs (set-difference newpairs pairs :key #'car))
	    (cons first (set-sequential-marks (cdr lexprs) (append pairs newpairs))))))))

(defmethod set-marks ((language language) &optional (pairs nil))
;;  (format *error-output* "set-marks slanguage")
  (make-language
   (mapcar (lambda (sword)
	     (set-marks sword
			(copy-tree pairs)))
	   (words language))
   :wordtype
   (wordtype language)
   :languagetype
   (languagetype language)))

(defmethod set-marks ((lexprs list) &optional (pairs nil))
  (mapcar (lambda (lexpr)
	    (set-marks lexpr (copy-tree pairs)))
	  lexprs))
