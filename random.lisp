(in-package :smots)

(defun some-letters (&rest l)
  (mapcar #'make-letter l))

(defun random-letter (letters)
  (assert letters)
  (nth (random (length letters)) letters))

(defun random-mletter (letters)
  (assert letters)
  (make-mletter
   (nth (random (length letters)) letters)))

(defun random-n-letters (n letters)
  (assert letters)
  (let ((rletters ()))
    (loop
     while (< (length rletters) n)
     do (pushnew (random-letter letters) rletters))
    rletters))
   
(defun random-letters (letters)
  (random-n-letters (1+ (random (length letters))) letters))

(defun random-alphabet (alphabet)
  (let ((letters (letters alphabet)))
    (make-alphabet
     (random-letters letters))))

(defun random-mletters (n letters)
  (assert letters)
  (mapcar #'make-mletter (random-n-letters n letters)))

(defun random-sletter (n letters)
  (assert letters)
  (make-sletter
   (random-mletters n letters)))
  
(defun random-sletters (n letters)
  (assert letters)
  (mapcar (lambda (x)
	    (declare (ignore x))
	    (random-sletter
	     (1+ (random (length letters)))
	     letters))
	  (make-list n)))

(defun random-sword (n alphabet)
;;  (let ((alpha (random-alphabet alphabet)))
  (make-sword-n (random-sletters n (letters alphabet))))

(defun random-sup-seq (seq fun)
  (if (endp seq)
      (and (zerop (random 2)) (list (funcall fun)))
      (append (and (zerop (random 2)) (list (funcall fun)))
	      (list (car seq))
	      (random-sup-seq (cdr seq) fun))))

(defun random-sup-sletter (sletter newletters)
  (assert newletters)
  (make-sletter
   (remove-duplicates
    (random-sup-seq (letters sletter)
		    (lambda ()
		      (random-mletter newletters))))))

(defun random-sup-sletters (sletters newletters)
  (assert newletters)
   (remove-if #'empty-sletterp
	      (mapcar 
	       (lambda (sletter)
		 (random-sup-sletter sletter newletters))
	       (random-sup-seq sletters
			       (lambda () (make-sletter ()))))))

(defmethod random-sup-sword ((sword sword) (alphabet alphabet))
  (let* ((swalpha (alphabet-of sword))
	 (ad (aset-difference alphabet swalpha)))
    (if (aemptyp ad)
	sword
	(make-sword-n
	 (random-sup-sletters
	  (letters sword) (letters ad))))))

(defun random-erase (seq &optional (n 2))
  (mapcan
   (lambda (e)
     (and (zerop (random n))
	 (list e)))
   seq))

(defun random-inf-sletters (sletters)
  (let ((sletters (random-erase sletters)))
    (remove-if #'empty-sletterp
	       (mapcar (lambda (sletter)
			 (make-sletter (random-erase (letters sletter) 3)))
		       sletters))))

(defgeneric joinnablep (sword1 sword2)
  (:documentation "true if SWORD1 J SWORD2 is not the empty word"))

(defmethod joinablep ((sword1 sword) (sword2 sword))
  (not (empty-wordp (ljoin sword1 sword2))))

(defgeneric random-inf-sword (sword)
  (:documentation "a random sword of SWORD"))

(defmethod random-inf-sword ((sword sword))
  (make-sword-n
   (random-inf-sletters (letters sword))))

;; SMOTS> (setf *sw* (random-sword 10 (some-letters "a" "b")))
;; [{a}{a,b}{a,b}{a}{a}{a,b}{a,b}{a,b}{a,b}{a}]
;; SMOTS> (setf *sw1*
;; 	     (random-sup-sword *sw* (some-letters "c" "d")))
;; [{c}{a}{a,b,d}{a,b,c}{a,d}{a,d}{a,b,c}{a,b,d}{c}{a,b,c,d}{a,b,c}{a,c}]
;; SMOTS> (setf *sw2* 
;; 	     (random-sup-sword *sw* (some-letters "e" "f")))
;; [{a,e}{a,b,f}{f}{a,b}{f}{a,e}{a,e}{a,b,f}{a,b,f}{a,b}{f}{a,b,e,f}{a,f}]
;; SMOTS> (ljoin *sw1* *sw2*)
;; [{c}{a,e}{a,b,d,f}{f}{a,b,c}{f}{a,d,e}{a,d,e}{a,b,c,f}{a,b,d,f}{c}{a,b,c,d}{f}{a,b,c,e,f}{a,c,f}]
