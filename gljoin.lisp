(in-package :smots)

(defvar *level-first* 2)

;; 0 on ne regarde pas les premières lettres
;; 1 on regarde les premières lettres (ou dernières) dans l'espoir de trouver une intersection vide
;; 2 on regarde les premières lettres et on décompose suivant les premières lettres si le cut-extreme-letters se fait facilement
;; 3 on décompose toujours.

;; trouver une numérotation qui fasse ce que veut sylviane sans avoir à changer de niveau
;; par exemple 1 est OK.

(defclass gljoin (assoc-lexpr s-mixin ic-mixin) ())

(defmethod cut-extreme-letter ((lexpr gljoin) (letter abstract-letter) &optional (last nil))
;;  (format *error-output* "cut-extreme-letter gljoin~%")
  (when (or (computed lexpr)
	  (<= (weight lexpr) 3)
	  (> *level-first* 2))
      (cut-extreme-letter (compute-language lexpr) letter last)
;; sinon rien pour l'instant 
))

(defmethod weight ((gljoin gljoin))
  (apply #'+ (mapcar #'weight (args gljoin))))

(defmethod contains-empty-sword ((gljoin gljoin))
  (every #'contains-empty-sword  (args gljoin)))

;; {} est absorbant pour J
(defmethod clean-args :before ((lexpr gljoin))
  (when *trace-clean-args*
    (format *error-output* "clean-args :before gljoin ~A~%" lexpr))
  (when (some #'empty-languagep (args lexpr))
    (setf (args lexpr) (list (empty-language lexpr)))))

  

