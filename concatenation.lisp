(in-package :smots)

(defvar *concatenation-lop* (make-lop
			     "."
			     #'concatenation
			     4
			     :cfun-lop #'+))
			     
(defclass concatenation (assoc-lexpr s-mixin)
  ((lop :allocation :class
       :initform *concatenation-lop*
       :reader lop)))

(defun concatenationp (o)
  (or (typep o 'concatenation)
      (and (swordp o) (cdr (letters o)))))

(defun make-concatenation (lexprs)
  (make-assoc-lexpr lexprs 'concatenation 'sword))

(defun make-concatenation-n (lexprs &optional (pairs nil))
;;  (format *error-output* "make-concatenation-n~%")
  (make-concatenation
   (set-sequential-marks lexprs pairs)))

(defgeneric concatenation-simplify-args-rec (args)
  (:documentation "simplifications of args for concatenation"))

(defmethod concatenation-simplify-args-rec ((args list))
;;  (format *error-output* "concatenation-simplify-args-rec simplify ~A ~%" *simplify-concatenation*)
  (cond
    ((endp args)
      ())
    ((empty-wordp (car args))
     (concatenation-simplify-args-rec (cdr args)))
    (*simplify-concatenation*
     (if (swordp (car args))
	 (do ((res (pop args)))
	     ((or (endp args) (not (swordp (car args))))
	      (cons res (concatenation-simplify-args-rec args)))
	   (setf res (funcall (lfun-lop *concatenation-lop*) res (pop args))))
	 (cons (car args) (concatenation-simplify-args-rec (cdr args)))))
    (t args)))

(defmethod clean-args :after ((lexpr concatenation))
  (when *trace-clean-args*
    (format *error-output* "clean args concatenation ~a~%" lexpr))
  (if (some #'empty-languagep (args lexpr))
      (setf (args lexpr) nil)
      (setf (args lexpr)
	    (concatenation-simplify-args-rec
	     (remove-if #'empty-wordp (args lexpr))))))

(defmethod extreme-letters ((concatenation concatenation) &optional (last nil))
;;  (format *error-output* "extreme-letters concatenation~%")
  (let ((pairs
	 (remove-if
	  #'null
	  (mapcar
	   (lambda (arg) (list arg (extreme-letters arg last)))
	   (args concatenation))
	  :key #'cdr)))
    (cadr 
     (if last
	 (car (last pairs))
	 (car pairs)))))

(defmethod cut-extreme-letter ((concatenation concatenation)
			       (letter abstract-letter) &optional (last nil))
;;  (format *error-output* "cut-extreme-letter concatenation ~%")
  (let ((pairs
	 (remove-if
	  #'null
	  (mapcar
	   (lambda (arg) (list arg (extreme-letters arg last)))
	   (args concatenation))
	  :key #'cdr)))
    (if (and pairs (member letter
			   (cadr (if last
				     (car (last pairs))
				     (car pairs)))))
	(let ((rest (if last
			(cut-last-letter (car (car (last pairs))) letter)
			(cut-first-letter (car (car pairs)) letter))))
	  (when rest
	    (make-assoc-lexpr
	     (if last
		 (append (mapcar #'car (butlast pairs)) (list rest))
		 (cons rest (mapcar #'car (cdr pairs))))
	     (class-of concatenation)
	     (wordtype concatenation))))
	(empty-language concatenation))))

(defmethod concatenation ((lexpr1 alanguage) (lexpr2 alanguage))
  (make-concatenation (list lexpr1 lexpr2)))

(defmethod set-marks ((concatenation concatenation) &optional (pairs nil))
  (make-concatenation
   (set-sequential-marks (args concatenation) pairs)))

(defmethod contains-empty-sword ((concatenation concatenation))
  (every #'contains-empty-sword (args concatenation)))
	  
(defmethod reduce-lunion-on-prefix-languages ((fsls list) (lexpr alanguage) (n integer))
  (reduce #'lunion
	  (mapcar (lambda (sletter)
		    (let ((suffix
			   (compute-prefix-language (cut-extreme-letter lexpr sletter)
						    (1- n))))
		      (concatenation (make-sword (list sletter)) suffix)))
		  fsls)))

(defmethod compute-prefix-language ((concatenation concatenation) (n integer))
  (let ((fsls (first-letters concatenation)))
    (reduce-lunion-on-prefix-languages fsls concatenation  n)))
