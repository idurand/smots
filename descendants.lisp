(in-package :smots)

(defgeneric accessible-words (words rules)
  (:documentation 
   "all accessible words which can be obtained from WORDS (one word or a list of words)
    using noetherian rewrite rules RULES"))
  

(defmethod accessible-words ((words list) (rules rules))
  (do* ((acc '() (append acc newacc))
	(newacc words (set-difference nextacc acc))
	(nextacc (apply-rules-to-words newacc rules)
		 (apply-rules-to-words newacc rules)))
       ((endp newacc) acc)))

(defmethod accessible-words ((word word) (rules rules))
  (accessible-words (list word) rules))

