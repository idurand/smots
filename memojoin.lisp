(in-package :smots)
 
(defmethod ljoin :around ((sword1 sword) (sword2 sword))
  (if *memoize-ljoin-swords*
      (let ((found
	     (or (gethash (list sword1 sword2) (ljoin-sword-table *spec*))
		 (gethash (list sword2 sword1) (ljoin-sword-table *spec*)))))
	(or found
;;	    (format *error-output* "ljoin already computed~%")
	    (setf (gethash (list sword1 sword2) (ljoin-sword-table *spec*))
		  (call-next-method))))
      (call-next-method)))

