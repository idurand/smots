(in-package :smots)
(defgeneric egraph-from-eexpr (eexpr))

(defmethod egraph-from-eexpr ((egraph egraph))
  egraph)

(defmethod egraph-from-eexpr ((sword sword))
  (if (empty-wordp sword)
      (make-egraph nil)
      (let ((sletters (letters sword)))
	(if (null (cdr sletters))
	    (make-egraph nil (list (car sletters)))
	    (let ((slorigin (butlast sletters))
		  (slextrem (cdr sletters)))
	      (make-egraph
	       (mapcar #'make-edge slorigin slextrem)))))))

(defmethod egraph-concatenation ((g1 egraph) (g2 egraph))
  (assert (not (or (egraph-empty g1) (egraph-empty g2))))
  (let ((extremities (egraph-extremity-nodes g1))
	(origins (egraph-origin-nodes g2))
	(edges1 (egraph-edges g1))
	(edges2 (egraph-edges g2)))
    (make-egraph
     (append edges1 edges2 (bipartite-edges extremities origins)))))

(defmethod egraph-from-eexpr ((lexpr concatenation))
  (assert (directedp lexpr))
  (let ((args (args lexpr)))
    (reduce #'egraph-concatenation (mapcar #'egraph-from-eexpr args))))

;; il faudra faire plus simple pour mix (disjoint qqch)
(defmethod egraph-from-eexpr ((lexpr gljoin))
  (assert (directedp lexpr))
  (let ((args (args lexpr)))
    (reduce #'egraph-union (mapcar #'egraph-from-eexpr args))))

(defun double-z (edges origin1 origin2)
    (let ((edges1 (node-descendant-edges origin1 edges)))
      (when (= 2 (length edges1))
	(let ((edges2 (node-descendant-edges origin2 edges)))
	  (when (= 2 (length edges2))
	    (let ((sl1 (make-slalphabet (edges-extremity-nodes edges1)))
		  (sl2 (make-slalphabet (edges-extremity-nodes edges2))))
	      (and
	       (eq sl1 sl2)
	       (let ((exts (letters sl1)))
		 (and
		  (= 2 (node-in-degree (first exts) edges))
		  (= 2 (node-in-degree (second exts) edges)))
		 (make-concatenation
		  (list
		   (make-mix
		    (list
		     (make-sword (list origin1))
		     (make-sword (list origin2))))
		   (make-mix
		    (mapcar (lambda (sl) (make-sword (list sl)))
			    exts))))))))))))

(defun egraph-expression-multi-ter (g) ;; que les petits Z
  (let* ((edges (egraph-edges g))
	 (origins (edges-origin-nodes edges))
	 (origin (first origins)))
    (or (and (= 2 (length origins)) (double-z edges origin (second origins)))
	(let* ((de (node-descendant-edges origin edges))
	       (rest (set-difference edges de :test #'eq)))
	  (make-ljoin
	   (list
	    (egraph-expression (make-egraph de))
	    (egraph-expression (make-egraph rest))))))))

(defun egraph-expression-multi (g)
  (let* ((edges (egraph-edges g))
	 (origin (car (edges-origin-nodes edges)))
	 (de (node-descendant-edges origin edges))
	 (rest (set-difference edges de :test #'eq)))
    (make-ljoin
     (list
      (egraph-expression (make-egraph de))
      (egraph-expression (make-egraph rest))))))

;; there are no freenodes!
(defun egraph-expression-one-component (g)
  (let* ((edges (egraph-edges g))
	 (origins (edges-origin-nodes edges))
	 (origin (car origins)))
    (if (null (cdr origins))
	(make-concatenation
	 (list
	  (make-sword origins) ;; just a list of one node
	  (egraph-expression
	   (egraph-remove-node origin g))))
	(let* ((extremities (edges-extremity-nodes edges))
	       (extremity (car extremities)))
	  (if (null (cdr extremities))
	      (make-concatenation
	       (list
		(egraph-expression
		 (egraph-remove-node extremity g))
		(make-sword extremities))) ;; just a list of one node
	      (egraph-expression-multi-ter g))))))

(defun egraph-expression-general-case (g)
  (let ((cc (egraph-connected-components g)))
    (if (null (cdr cc))
	(egraph-expression-one-component g)
	(make-mix
	 (mapcar (lambda (c)
		   (egraph-expression-general-case (egraph-remove-nodes c g)))
		 cc)))))

(defmethod egraph-expression ((alanguage alanguage))
  alanguage)

(defmethod egraph-expression ((lexpr (eql nil)))
  (empty-slanguage))

(defmethod egraph-expression ((g egraph))
  (cond
    ((egraph-empty g)
     (empty-sword))
    ((egraph-freenodes g)
     (make-ljoin
	(list
	 (make-mix (mapcar (lambda (sletter)
			     (make-sword (list sletter)))
			   (egraph-freenodes g)))
	 (egraph-expression
	  (egraph-remove-nodes (egraph-freenodes g) g)))))
    (t
     (egraph-expression-general-case g))))

(defmethod ljoin-directed ((lexpr1 alanguage) (lexpr2 alanguage))
  (if (try-to-join-sletters (letters (slalphabet-of lexpr1))
			    (letters (slalphabet-of lexpr2)))
;;      (egraph-expression
       (egraph-union
	(egraph-from-eexpr lexpr1)
	(egraph-from-eexpr lexpr2))
;;       )
      (empty-slanguage)))

;; (defun potential-z (g)
;;   (let*
;;       ((edges (egraph-edges g))
;;        (origins (edges-origin-nodes edges))
;;     (mapcar ;; origin, descendants, antecedants
;;      (lambda (origin)
;;        (let* ((exts
;; 	       (make-slalphabet
;; 		(edges-extremity-nodes (node-descendant-edges-z origin edges))))
;; 	      (oris
;; 	       (make-slalphabet
;; 		(edges-origin-nodes
;; 		 (mapcan
;; 		  (lambda (extremity)
;; 		    (node-antecedant-edges-z extremity edges))
;; 		  (letters exts))))))
;; 	 (list origin exts oris)))
;;      origins)))

;; (defun egraph-expression-multi-bis (g) ;; essai avec grands Z
;; ;; a mon avis ça ne marche pas
;;   (let ((edges (egraph-edges g))
;; 	(triples (potential-z g)))
;;     (loop
;;       while triples
;;       do (let ((t1 (pop triples)))
;; 	   (loop
;; 	     for t2 in triples
;; 	     when
;; 	     (and (eq (third t1) (third t2))
;; 		  (eq (second t1) (second t2)))
;; 	     do (let ((ainter-ext (aintersection (second t1) (second t2))))
;; 		  (let ((ee
;; 			 (mapcar
;; 			  #'make-sword
;; 			  (cartesian-product (list (letters ainter-ori)
;; 						   (letters ainter-ext))))
;; 		    (unless (aemptyp ainter)
;; 		      (let ((ainter-ori (aintersection (third t1) (third t2))))
;; 			(unless (aemptyp ainter-ori)
;;       			  (mapcar
;; 			   #'make-sword
;; 			   (cartesian-product (list (letters ainter-ori)
;; 						   (letters ainter-ext))))
;; 		      (let ((newnode
;; 			     (make-sletter
;; 			      (list (make-mletter (make-unique-internal-letter 1))))))
;; 			(loop
;; 			  for ext in (letters ainter)
;; 			  do (let*
;; 				 ((ori (first t1))
;; 				  (e (make-edge ori ext)))
;; 				 (print (list ori ext e edges))
;; 			       (when
;; 				   (and
;; 				    (member e edges :test #'eq)
				    
;; 				 (setf edges
;; 				       (cons
;; 					(make-edge ori newnode)
;; 					(cons
;; 					 (make-edge newnode ext)
;; 					 (remove e edges :test #'eq)))))))))))))
;;     edges))
