(in-package :smots)

(defclass bool-mixin (ic-mixin) ()
  (:documentation "to add properties of boolean expressions"))

(defmethod cut-extreme-letter ((lexpr bool-mixin)
				(letter abstract-letter)
				&optional (last nil))
  (format *error-output* "WARNING cut-extreme-letter bool ~A ~%" (class-of lexpr))
  (let ((args (cut-extreme-letter (args lexpr) letter last)))
    (unless (some #'null args)
      (and args (make-assoc-lexpr args (class-of lexpr) (wordtype lexpr))))))

(defmethod compute-prefix-language ((lexpr bool-mixin) (n integer))
  (let ((args (mapcar
	       (lambda (arg)
		 (compute-prefix-language arg n))
	       (args lexpr))))
    (reduce (lfun lexpr) args)))

;; distributivity of join over boolean expressions 

(defmethod ljoin ((lexpr1 bool-mixin) (lexpr2 alanguage))
;;  (format *error-output* "ljoin bool-mixin alanguage ~%")
  (make-assoc-lexpr 
	   (mapcar (lambda (arg)
		    (ljoin arg lexpr2))
		  (args lexpr1))
	   (class-of lexpr1) (wordtype lexpr1)))
  
(defmethod ljoin ((lexpr1 alanguage) (lexpr2 bool-mixin))
;;  (format *error-output* "ljoin alanguage bool-mixin ~%")
  (ljoin lexpr2 lexpr1))

