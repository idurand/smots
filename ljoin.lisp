(in-package :smots)

(defvar *egraph-mode* t)

(defun toggle-egraph-mode ()
  (setf *egraph-mode* (not *egraph-mode*)))

(defgeneric brute-join (l1 l2))

(defvar *ljoin-lop* (make-lop "J" #'brute-join 2))

(defclass ljoin (gljoin)
  ((lop :allocation :class
	:initform *ljoin-lop*
	:reader lop)))

(defun ljoinp (lexpr)
  (typep lexpr 'ljoin))

(defun make-ljoin (lexprs)
  (if (disjoint-alphabets lexprs)
      (make-mix lexprs)
      (make-assoc-lexpr lexprs 'ljoin 'sword)))

(defgeneric mix-with-mletters (lexpr1 alphac malpha2)
  (:documentation ""))

(defmethod mix-with-mletters ((lexpr1 alanguage) (alphac alphabet)
			      (malpha2 alphabet))
  (let ((malphabet (projection malpha2 alphac)))
    (if (aemptyp malphabet)
	lexpr1
	(make-mix (cons lexpr1 (delanoy-args malphabet))))))
			
(defmethod ljoin ((lexpr1 alanguage) (lexpr2 mix))
;;  (format *error-output* "ljoin alanguage mix~A J ~A~%" lexpr1 lexpr2)
  (if (delanoyp lexpr2 (malphabet-of lexpr1))
      lexpr1
      (call-next-method)))
 
(defmethod ljoin ((lexpr1 mix) (lexpr2 alanguage))
;;  (format *error-output* "ljoin mix alanguage~%")
  (if (delanoyp lexpr1 (malphabet-of lexpr2))
      lexpr2
      (call-next-method)))

(defmethod ljoin :around ((lexpr1 alanguage) (lexpr2 alanguage))
;;  (format *error-output* "ljoin around alanguage s1 directed ~A s2 directed ~A~%" (directedp lexpr1) (directedp lexpr2))
  (cond
    ((eq lexpr1 lexpr2)
     lexpr1)
    ((or (empty-wordp lexpr1)
	 (empty-wordp lexpr2))
     (empty-sword))
    ((and *egraph-mode*
	  (directedp lexpr1)
	  (directedp lexpr2)
	  (not (and (swordp lexpr1) (swordp lexpr2))))
     (ljoin-directed lexpr1 lexpr2))
    (t
     (call-next-method))))

(defmethod ljoin ((lexpr1 alanguage) (lexpr2 alanguage))
;;  (format *error-output* "ljoin alanguage ~%~A JOIN ~A~%" lexpr1 lexpr2)
  (let* ((malpha1 (malphabet-of lexpr1))
	 (malpha2 (malphabet-of lexpr2))
	 (alpha1 (alphabet-of lexpr1))
	 (alpha2 (alphabet-of lexpr2))
	 (alphai (aintersection alpha1 alpha2)))
    (if
     (aemptyp alphai)
     (make-mix (list lexpr1 lexpr2))
     (let* ((e1 (mix-with-mletters lexpr1 (aset-difference alpha2 alpha1) malpha2))
	    (e2 (mix-with-mletters lexpr2 (aset-difference alpha1 alpha2) malpha1))
	    (ei (lintersection e1 e2)))
       (if (equivalent ei (make-lintersection (list e1 e2)))
	   (progn  (format *error-output* "intersection n a rien apporte~%")
		   (make-ljoin (list lexpr1 lexpr2)))
	   ei)))))

(defmethod extreme-letters ((lexpr ljoin) &optional (last nil))
;;  (format *error-output* "extreme-letters ljoin~%")
  (let ((fls (cartesian-product (mapcar
				 (lambda (arg)
				     (mapcar (lambda (sletter)
						 (make-sword
						  (list sletter)))
					     (extreme-letters arg last)))
				 (args lexpr)))))
    (remove-duplicates
     (mappend #'(lambda (sl)
			(extreme-letters sl last))
	      (mapcar
	       (lambda (tuple)
		   (reduce #'ljoin tuple))
	       fls)))))


(defun check-op (e1 e2 op)
  (let* ((l1 (compute-language e1))
	 (l2 (compute-language e2))
	 (l (funcall op l1 l2))
	 (e (funcall op e1 e2))
	 (le (compute-language e)))
    (or (equivalent l le)
	(values nil
		(set-difference (words l) (words le))
		(set-difference (words le) (words l))))))
