;;;; -*- Mode: lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;; ASDF system definition
;;;
;;; Copyright (C) 2007 Irene Durand (idurand@labri.fr)
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free Software
;;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

(in-package :common-lisp-user)

(pushnew :smots *features*)

(defpackage :smots-system
  (:use :asdf :common-lisp))

(in-package :smots-system)

(defparameter *smots-directory* (directory-namestring *load-truename*))

(asdf:defsystem :smots
  :name "smots"
  :description "smots: is a system implementing swords"
  :version "1.0"
  :author "Irene Durand <idurand@labri.fr>"
  :licence "General Public Licence"
;;  :depends-on (:autowrite)
  :depends-on ()
  :serial t ;; the dependencies are linear
  :components (
	       (:file "package")
	       (:file "general")
	       (:file "time")
	       (:file "hashtable")
	       (:file "file-path")
	       (:file "globals")
	       (:file "object")
	       (:file "named-object")
	       (:file "names")
	       (:file "abstract-letter")
	       (:file "alpha-object")
	       (:file "letter")
	       (:file "alphabet")
	       (:file "alanguage")
	       (:file "mletter")
	       (:file "words")
	       (:file "language")
	       (:file "lexpr")
	       (:file "word")
	       (:file "sletter")
	       (:file "memo")
	       (:file "delanoy")
	       (:file "sword")
	       (:file "random")
	       (:file "fusion")
	       (:file "wtrs")
	       (:file "descendants")
	       (:file "operator")
	       (:file "op-lexpr")
	       (:file "assoc-lexpr")
	       (:file "unary-lexpr")
	       (:file "ac-lexpr")
	       (:file "marks")
	       (:file "concatenation")
	       (:file "idem")
	       (:file "bool")
	       (:file "gljoin")
	       (:file "mix")
	       (:file "lstar")
	       (:file "mirror")
	       (:file "lunion")
	       (:file "problem")
	       (:file "specification")
	       (:file "memojoin")
	       (:file "lexer")
	       (:file "parser")
	       (:file "input")
	       (:file "memomix")
	       (:file "init")
	       (:file "lintersection")
	       (:file "ljoin")
	       (:file "explode")
	       (:file "factor")
	       (:file "egraph")
	       (:file "elementary-join")
	       (:file "directed-join")
	       (:file "granularity")
	       (:file "macro-sletter")
	       (:file "egraph-to-dot")
)
;;	       (:file "tests"))
  )
