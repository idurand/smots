(in-package :smots)

(defun make-specs () (make-hash-table :test #'equal))

(defgeneric lexpr (spec))

(defclass specification (named-object)
  ((names :initform nil :accessor names)
   (alphabets :initform nil :accessor alphabets)
   (letter-counter :initform -1 :accessor letter-counter)   
   (letters :initform nil :accessor letters)
   (sletters :initform nil :accessor sletters)
   (internal-swords :initform (make-hash-table :test #'equal)
		    :accessor internal-swords)
   (mletters :initform nil :accessor mletters)
   (sword :initform nil :accessor sword)
   (sexpr :initform nil :accessor sexpr)
   (slanguage :initform nil :accessor slanguage)
   (problem :initform (make-problem "The Problem" nil) :accessor problem)
   (swords :initform (make-swords) :accessor swords)
   (sexprs :initform (make-sexprs) :accessor sexprs)
   (slanguages :initform (make-slanguages) :accessor slanguages)
   (problems :initform (make-problems) :accessor problems)
   (mix-sword-table :initform (make-hash-table :test #'equal)
		    :reader mix-sword-table)
   (mix-basic-sword-table :initform (make-hash-table)
			  :reader mix-basic-sword-table)
   (ljoin-sword-table :initform (make-hash-table :test #'equal) :reader ljoin-sword-table)
   ))

(defmethod gen-calphabet-of ((specification specification) (lettertype (eql 'letter)))
;;  (format t "gen-calphabet-of specification  letter ~%")
  (make-alphabet (letters specification) 'letter))

(defmethod gen-calphabet-of ((specification specification) (lettertype (eql 'mletter)))
;;  (format t "gen-calphabet-of specification  mletter ~%")
  (make-alphabet (mletters specification) 'mletter))

(defmethod gen-calphabet-of ((specification specification) (lettertype (eql 'sletter)))
;;  (format t "gen-calphabet-of specification  sletter ~%")
  (make-alphabet (sletters specification) 'sletter))

(defun make-spec (name)
 (make-instance 'specification :name name))

(defun load-spec (stream)
  (handler-case
      (let ((*spec* (make-instance 'specification :name "new")))
	(parse-spec stream)
	*spec*)
    (smots-parse-error ()
      (prog1 nil (format *error-output* "parse error~%")))
    (all-errors ()
      (prog1 nil (format *error-output* "error~%")))))

(defun read-spec-from-path (path)
  (let ((spec (load-file-from-absolute-filename path #'load-spec)))
    (when spec
      (setf (name spec) (file-namestring path)))
    spec))

(defun add-current-sword ()
  (add-sword (sword *spec*) (swords *spec*)))

(defun set-current-sword (sword)
  (add-sword sword (swords *spec*))
  (setf (sword *spec*) sword))

(defun set-current-sexpr (lexpr)
  (add-sexpr lexpr (sexprs *spec*))
  (setf (sexpr *spec*) lexpr))

(defun add-spec (spec specs)
  (setf (gethash (name spec) specs) spec))

(defun add-current-spec ()
  (add-spec *spec* *specs*))

(defun add-current-slanguage ()
  (add-language (slanguage *spec*) (slanguages *spec*)))

(defun add-current-sexpr ()
  (add-sexpr (sexpr *spec*) (sexprs *spec*)))

(defun get-spec (name)
  (gethash name *specs*))

(defun list-specs ()
  (list-keys *specs*))

(defun set-current-spec (spec)
  (setf *spec* spec)
  (add-current-spec))

(defun back-up-file (file)
  (when (probe-file file)
      (let ((nfile (concatenate 'string file ".bak")))
        (back-up-file nfile)
        (rename-file file nfile))))

(defun set-current-problem (problem)
  (setf (problem *spec*) problem))

(defun write-problem (problem stream)
  (format stream "Problem ~A" problem)
  (display-sequence (constraints problem)
		    stream :sep #\Newline)
  (format stream "~%"))

(defun write-spec (spec stream)
  (let ((problem (problem spec)))
    (when problem
      (write-problem problem stream)))
  (format stream "~%")
  (let ((sword (sword spec)))
    (when sword
      (write-sword sword stream)))
  (format stream "~%")
  (let ((slanguage (slanguage spec)))
    (when slanguage
      (write-slanguage slanguage stream)
   (format stream "~%")     )))

;;   (when (sword spec)
;;     (write-sword stream))
;;   (when (sexpr spec)
;;     (write-sexpr stream))
;;   (when (slanguage spec)
;;     (write-slanguage stream))
;;   (write-problems spec stream)
;;   (write-slanguages spec stream)
;;   (write-swords spec stream)
;;   (write-sexpr spec stream))

(defun write-current-spec (stream)
  (write-spec *spec* stream))

(defun save-spec (file)
  (back-up-file (absolute-filename file))
  (with-open-file (foo (absolute-filename file) :direction :output)
    (if foo
        (progn
          (write-current-spec foo)
          (format *standard-output* "Specification saved~%"))
        (prog1
            nil
          (format *error-output* "unable to open ~A~%" file)))))
