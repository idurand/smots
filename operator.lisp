(in-package :smots)

(defgeneric lfun-lop (lop)
  (:documentation "la fonction binaire associée à l'opérateur lop"))

(defgeneric cfun-lop (lop)
  (:documentation "la fonction de comptage associée à l'opérateur lop"))

(defclass lop ()
  ((symbol-lop :initarg :symbol-lop)
   (lfun-lop :initarg :lfun-lop :reader lfun-lop)
   (fun-lop :initarg :fun-lop :reader fun-lop)
   (cfun-lop :initform nil :initarg :cfun-lop :reader cfun-lop)
   (priority :initarg :priority :reader priority)))

(defun make-lop (symbol-lop lfun-lop priority
		&key (fun-lop nil) (cfun-lop nil))
  (make-instance 'lop
		 :symbol-lop symbol-lop
		 :lfun-lop lfun-lop
		 :priority priority
		 :fun-lop fun-lop
		 :cfun-lop cfun-lop
		 ))
 
(defmethod print-object ((lop lop) stream)
  (format stream "~a" (slot-value lop 'symbol-lop)))

