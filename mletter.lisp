(in-package :smots)

(defvar *show-mark* nil)

(defun toggle-show-mark ()
  (setf *show-mark* (not *show-mark*)))

(defvar *internal-mletters* nil)

(defgeneric mletters (specification)
  (:documentation "list of all the mletters defined in SPECIFICATION")
  (:method ((spec (eql nil))) *internal-mletters*))

(defgeneric (setf mletters) (val specification)
  (:documentation "writer on the list of mletters defined in SPECIFICATION")
  (:method (val (spec (eql nil))) (setf *internal-mletters* val)))

(defgeneric malphabet-of (object)
  (:documentation "alphabet of the mletters present in OBJECT"))

(defclass m-mixin ()
  ((malphabet-of :initform nil))
  (:documentation "for languages with marked letters"))

(defclass mletter (abstract-letter m-mixin)
  ((letter :initarg :letter :reader letter)
   (mark :initarg :mark :reader mark))
  (:documentation "class for markd letters"))

(defmethod gen-calphabet-of ((mletter mletter) lettertype)
  (gen-alphabet-of (letter mletter) lettertype))

(defmethod gen-alphabet-of (o (lettertype (eql 'mletter)))
  (declare (ignore lettertype))
  (malphabet-of o))

;; pour les m-mixin memoisation
(defmethod malphabet-of :around ((m-mixin m-mixin))
  ;;  (format *error-output* "malphabet-of around m-mixin ~A ~%" m-mixin)
  (with-slots (malphabet-of) m-mixin
    ;;    (format *error-output* "slot malphabet ~A ~%" malphabet-of)
    (or malphabet-of
	(setf malphabet-of (call-next-method)))))

;; pour les autres on doit construire
(defmethod malphabet-of (o)
  (gen-calphabet-of o 'mletter))

(defun mletterp (o)
  (typep o 'mletter))

(defgeneric make-mletter (string &optional mark)
  (:documentation "return an mletter with mark MARK from the letter of name STRING"))

(defmethod make-mletter ((string string) &optional (mark 0))
  (make-mletter (make-letter-preserving-granularity string) mark))

(defmethod make-mletter ((letter letter) &optional (mark 0))
  (or (find-if (lambda (mletter)
		 (and (eq letter (letter mletter))
		      (= mark (mark mletter))))
	       (mletters *spec*))
      (let ((mletter (make-instance 'mletter
				    :letter letter
				    :mark mark)))
	(setf (mletters *spec*) (append (mletters *spec*) (list mletter)))
	mletter)))

(defmethod name ((mletter mletter))
  (name (letter mletter)))

(defmethod granularity ((mletter mletter))
  (granularity (letter mletter)))

(defmethod punctualp ((mletter mletter))
  (punctualp (letter mletter)))

(defmethod letter-number ((mletter mletter))
  (letter-number (letter mletter)))

(defmethod print-object ((mletter mletter) stream)
  (format stream "~A" (letter mletter))
  (let ((mark (mark mletter)))
    (when *show-mark*
      (format stream "_~D" mark))))

(defmethod letter-value ((mletter mletter))
  (let ((n (letter-number mletter))
	(ind (mark mletter)))
    (+ (* 10 n) ind)))

(defun letters-from-mletters (mletters)
  (remove-duplicates (mapcar #'letter mletters)
		     :test #'eq))

(defun make-malphabet (mletters)
  (make-alphabet mletters 'mletter))

(defun alphabet-from-malphabet (malphabet)
  (make-alphabet
   (mapcar #'letter
	   (letters malphabet))
   'letter))

(defgeneric malphabet-of (alpha-object)
  (:method (o)
    ;;  (format *error-output* "malphabet ~A~%" o)
    (gen-calphabet-of o 'mletter)))

(defmethod max-mark ((letter letter) (malphabet alphabet))
  (max-mark letter (letters malphabet)))

(defmethod max-mark ((letter letter) (mletters list))
  (reduce #'max
	  (mapcar #'mark
		  (remove letter mletters :key #'letter :test-not #'eq))))

(defgeneric next-mletter (mletter)
  (:documentation "the mletter with same letter as MLETTER and mark(MLETTER) + 1")
  (:method ((mletter mletter))
    (let ((mark (mark mletter)))
      (assert (evenp mark))
      (make-mletter (letter mletter) (1+ mark)))))

(defgeneric previous-mletter (mletter)
  (:documentation "the mletter with same letter as MLETTER and mark(MLETTER) - 1")
  (:method ((mletter mletter))
    (let ((mark (mark mletter)))
      (assert (oddp mark))
      (make-mletter (letter mletter) (1- mark)))))
