(in-package :smots)

(defvar *memoize* t)

(defclass memo-mixin ()
  ((memo :initform nil :accessor memo)))

(defmethod cardinality :around ((memo-mixin memo-mixin))
  (if (and *memoize* (computed memo-mixin))
      (cardinality (memo memo-mixin))
      (call-next-method)))

(defmethod compute-language :around ((memo-mixin memo-mixin))
  (if *memoize*
      (with-slots (memo) memo-mixin
	(when memo (format *error-output* "language already computed~%"))
	(or memo
	    (setf memo
		  (call-next-method))))
      (call-next-method)))

(defgeneric computed (memo-mixin)
  (:documentation "T if the language is computed in extension"))

(defmethod computed ((memo-mixin memo-mixin))
  (memo memo-mixin))
