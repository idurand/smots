(in-package :smots)

(defvar *lstar-lop* (make-lop "*" nil 3))

;; on convient que les arguments de l'etoile sont toujours
;; implicitement numerotes a partir de 0
;; au moment ou on calcule il faudra faire des set-marks?

(defclass lstar (s-mixin unary-lexpr)
  ((lop :allocation :class
	:initform *lstar-lop*
	:reader lop)))

(defun lstarp (lexpr)
  (typep lexpr 'lstar))

(defun make-lstar (lexpr)
  (make-unary-lexpr lexpr 'lstar))

(defmethod set-marks ((lstar lstar) &optional (pairs nil))
  (make-lstar
   (set-marks (arg lstar) pairs)))

(defmethod simplify-assoc-lexpr ((lstar lstar))
  (let ((args (args lstar)))
    (if (or (endp args)
	    (and (null (cdr args))
		 (empty-wordp (car args))))
	(empty-sword)
	lstar)))

(defmethod extreme-letters ((lexpr lstar) &optional (last nil))
;;  (format *error-output* "extreme-letters glunion~%")
  (extreme-letters (arg lexpr) last))

;; faux a finir - voir residuels
(defmethod cut-extreme-letter ((lstar lstar) (letter abstract-letter) &optional (last nil))
;;  (format *error-output* "cut-extreme-letter lstar ~%")
;; in case of last marks are not relevant!
  (let* ((arg (arg lstar))
	 (residual (cut-extreme-letter arg letter last)))
    (when residual
      (make-concatenation
       (if last 
	   (list lstar residual)
	   (list residual lstar))))))

(defgeneric expand-lstar (lstar)
  (:documentation "A* -> Epsilon + A+"))

(defmethod expand-lstar ((lstar lstar))
  (make-lunion
   (list
    (empty-sword)
    (make-concatenation
     (list
      (arg lstar)
      (set-marks lstar (get-letter-mark-pairs (malphabet-of (arg lstar)))))))))

(defmethod compute-language ((language lstar))
  (format *error-output* "cannot compute in extension an infinite language"))

(defmethod compute-prefix-language ((lstar lstar) (n integer))
  (format *error-output* "compute-prefix-language lstar~%")
;;  (if (zerop n)
;;      (empty-sword)
  (let ((arg (arg lstar)))
    (assert (not (empty-wordp arg)))
    (lunion
     (empty-sword)
     (reduce-lunion-on-prefix-languages
      (first-letters arg)
      (expand-lstar lstar) n))))

(defmethod contains-empty-sword ((lstar lstar))
  (declare (ignore lstar))
  t)

(defmethod directedp ((lstar lstar))
  (declare (ignore lstar))
  nil)