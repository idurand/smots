(in-package :smots)

(defclass alpha-object ()
  ((alphabet-of :initform nil))
  (:documentation "object which has an alphabet of letters"))

(defgeneric alphabet-of (alpha-object)
  (:documentation "alphabet of the letters present in ALPHA-OBJECT"))
