(in-package :smots)

(defgeneric brute-mix (sword1 sword2))

(defmethod mix ((alanguage1 alanguage) (alanguage2 alanguage))
  (make-mix (list alanguage1 alanguage2)))

(defvar *mix-lop* (make-instance
		     'lop
		     :symbol-lop "X"
		     :lfun-lop #'brute-mix
		     :priority 0))

(defclass mix (gljoin)
  ((lop :allocation :class :initform *mix-lop* :reader lop)))

(defun mixp (lexpr)
  (typep lexpr 'mix))

(defun make-mix (args)
  (assert (disjoint-alphabets args))
  (make-assoc-lexpr args 'mix 'sword))

(defgeneric delanoy-mix (malphabet)
  (:documentation "mix corresponding to MALPHABET"))

(defmethod delanoy-mix ((malphabet alphabet))
  (make-mix (delanoy-args malphabet)))

(defgeneric delanoy-argsp (args malphabet)
  (:documentation "T if ARGS are delanoy args for MALPHABET"))

(defmethod delanoy-argsp ((args list) (malpha alphabet))
  (let ((dargs (delanoy-args malpha)))
     (and (subsetp args dargs)
	  (subsetp dargs args))))

(defmethod delanoyp ((mix mix) (malpha alphabet))
  (let ((args (args mix)))
    (and (every (lambda (arg) (swordp arg)) args)
	 (delanoy-argsp args malpha))))

(defgeneric words-of-mix-args (args))

(defmethod words-of-mix-args ((args list))
  (assert (every #'slettersp args))
  (let ((rules (rules-from-mix args)))
    (accessible-words
     (make-sword (reduce #'append  args :from-end t))
     rules)))

(defmethod brute-mix ((sword1 sword) (sword2 sword))
;;  (format *error-output* "brute-mix swords~%")
  (assert (disjoint-alphabets (list sword1 sword2)))
  (let ((args (list (letters sword1) (letters sword2))))
    (make-slanguage (words-of-mix-args args))))

(defmethod brute-mix ((sword sword) (slanguage slanguage))
  (reduce
   #'lunion
   (mapcar (lambda (sw)
	     (brute-mix sword sw))
	   (words slanguage))))
(defmethod brute-mix ((slanguage slanguage) (sword sword))
  (brute-mix sword slanguage))
 
(defmethod extreme-letters ((mix mix) &optional (last nil))
;;  (format *error-output* "extreme-letters mix~%")
  (let ((largs (cartesian-product
	     (mapcar (lambda (arg)
		       (extreme-letters arg last))
		     (args mix)))))
    (remove-duplicates
     (mapcar
      (lambda (w)
	(car (letters w)))
      (remove-duplicates
       (mappend
	(lambda (sletters)
	  (words-of-mix-args (mapcar #'list sletters)))
	largs))))))

(defgeneric rules-from-mix (args))

(defgeneric gen-rules (sletter1 sletter2))

(defmethod gen-rules ((sletter1 sletter) (sletter2 sletter))
  (let* ((alpha1 (alphabet-of sletter1))
	 (alpha2 (alphabet-of sletter2))
	 (alphai (aintersection alpha1 alpha2)))
    (and (aemptyp alphai)
	 (let ((sletter (make-sletter (append (letters sletter1)
					      (letters sletter2)))))
	   (list 
	    (make-wrule (make-sword (list sletter1 sletter2))
			(make-sword (list sletter)))	
	    (make-wrule (make-sword (list sletter))
			(make-sword (list sletter2 sletter1))))))))
	

(defgeneric gen-rules-tuple (sletters))

(defmethod gen-rules-tuple ((sletters list))
  (let ((ps (remove-if (lambda (set)
			   (null (cdr set)))
		       (powerset sletters))))
    (mappend
     (lambda (nuple)
	 (let ((pairs (remove-if (lambda (partition)
				     (not (= 2 (length partition))))
				 (partitions nuple))))
	   (mappend (lambda (pair)
			(let ((mletters1 (mappend #'letters (car pair)))
			      (mletters2 (mappend #'letters (cadr pair))))
			  (and (no-duplicated-letter mletters1)
			       (no-duplicated-letter mletters2)
			       (gen-rules
				(make-sletter mletters1)
				(make-sletter mletters2)))))
		    pairs)))
     ps)))
    
;; pour le moment ne marche que pour args de lg 2
(defmethod rules-from-mix ((args list))
  (make-wrules
   (mappend #'gen-rules-tuple
	    (cartesian-product (mapcar (lambda (arg)
					   (letters (slalphabet-of arg)))
				       args)))))

;; ce serait bien de prouver que c'est correct
(defmethod cut-extreme-letter ((mix mix) (sletter sletter) &optional (last nil))
  (declare (ignore last))
;;  (format *error-output* "cut-extreme-letter mix ~%")
;; voir si on pourrait mettre directedp
  (if (elementaryp mix)
      (let ((res (projection mix (aset-difference (malphabet-of mix)
						  (malphabet-of sletter)))))
	(or
;;	 (assert (equivalent (compute-language res) (cut-extreme-letter (compute-language mix) sletter)))
;;	 (format *error-output* "cut-extreme-letter elementary mix ~A ~A   ~A~%" mix sletter res)
	 res))
      (or
;;       (format *error-output* "cut-extreme-letter mix not elementary ~%")
       (call-next-method))))

(defmethod compute-prefix-language ((mix mix) (n integer))
  (let ((args (mapcar (lambda (arg)
			(compute-prefix-language arg (1- n)))
		      (args mix))))
    (compute-prefix-language (reduce #'mix args) n)))

(defmethod clean-args :after ((lexpr mix))
  (when *trace-clean-args*
    (format *error-output* "clean-args :after mix ~A~%" lexpr))
  (let ((args (args lexpr)))
    (assert (disjoint-alphabets args))
    ;; remove multiple occ of empty sword
    (setf args (remove-duplicates args :test #'eq))
    (when (not (null (cdr args)))
      (setf args (remove-if #'empty-wordp args)))
    (setf (args lexpr) args))
  lexpr)
