(in-package :smots)

(define-condition all-errors (error) ())

(defvar *smots-readtable* (copy-readtable))

(defun absolute-filename (filename)
  (concatenate 'string *data-directory* filename))

(defun load-file-from-absolute-filename  (absolute-filename reader)
  (with-open-file (foo absolute-filename :direction :input :if-does-not-exist nil)
    (if foo
	(funcall reader foo)
	(prog1
	    nil
	  (format *error-output* "unable to open ~A~%" absolute-filename)))))

(defun input-sword-n (s &optional (number t))
  (with-input-from-string (stream s) (parse-sword-n stream number)))

(defun input-sword (s)
  (input-sword-n s nil))

(defun input-sexpr (s)
  (input-sexpr-n s nil))

(defun input-sexpr-n (s &optional (number t))
  (with-input-from-string (stream s) (parse-sexpr-n stream number)))

(defun input-sletter (s)
  (with-input-from-string (stream s) (parse-sletter stream)))

(defun input-mletter (s)
  (with-input-from-string (stream s) (parse-mletter stream)))

(defun read-constraints (stream)
  (handler-case
      (parse-sexprs stream)
    (smots-parse-error ()
      (prog1 nil (format *error-output* "parse error~%")))
    (all-errors ()
      (prog1 nil
        (format *error-output* "Unable to read constraints")))))

(defun read-swords (stream)
  (handler-case
      (parse-swords stream)
    (smots-parse-error ()
      (prog1 nil (format *error-output* "parse error~%")))
    (all-errors ()
      (prog1 nil
        (format *error-output* "Unable to read swords")))))

(defun read-sword (stream)
  (handler-case
      (parse-sword stream)
    (smots-parse-error ()
      (prog1 nil (format *error-output* "parse error~%")))
    (all-errors ()
      (prog1 nil
        (format *error-output* "Unable to read a sword")))))

(defun read-sexpr (stream)
  (handler-case
      (parse-sexpr stream)
    (smots-parse-error ()
      (prog1 nil (format *error-output* "parse error~%")))
    (all-errors ()
      (prog1 nil
        (format *error-output* "Unable to read an lexpr")))))

(defun read-slanguage (stream)
  (handler-case
      (parse-slanguage stream)
    (smots-parse-error ()
      (prog1 nil (format *error-output* "parse error~%")))
    (all-errors ()
      (prog1 nil
        (format *error-output* "Unable to read a slanguage")))))
