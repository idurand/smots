(in-package :smots)
;;; garder l'idée que pair = ponctuel ou début intervalle
;;; quand rien n'est précisé les lettres sont de granularité 2
;;; punctual indique granularité 1

(defgeneric parikh-malphabet (malphabet)
  (:documentation
   "list containing for each letter the mletter with the greatest mark"))

(defmethod parikh-malphabet ((malphabet alphabet))
  (make-malphabet
   (mapcar (lambda (letter)
	     (make-mletter letter (max-mark letter malphabet)))
	   (letters (alphabet-of malphabet)))))

(defmethod parikh-value ((letter letter) (parikh alphabet))
  (mark (find letter (letters parikh) :key #'letter)))

(defmethod parikh-malphabet ((sword sword))
  (parikh-malphabet (malphabet-of sword)))

(defgeneric parikh-intervalp (letter parik)
  (:documentation ""))

(defgeneric parikh-punctualp (letter parik)
  (:documentation ""))

(defmethod parikh-intervalp ((letter letter) (parikh alphabet))
  (oddp (parikh-value letter parikh)))

(defmethod parikh-punctualp ((letter letter) (parikh alphabet))
  (not (parikh-intervalp letter parikh)))

(defun interval-letters (sl)
  (remove-if #'punctualp
	     (letters (alphabet-of sl))))

(defun punctual-letters (sl)
  (remove-if-not #'punctualp
		 (letters (alphabet-of sl))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Convex components
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod closed-letter-sletters-p ((letter letter) (sletters list) (parikh alphabet))
  (assert (member letter (letters parikh) :key #'letter))
  (= (mod (parikh-value letter parikh) 2)
     (mod (max-mark letter (malphabet-of sletters)) 2)))

(defgeneric closed-component-p (sword parikh)
  (:documentation "plus petites composantes convexes")) 

(defmethod closed-component-p ((sletters list) (parikh alphabet))
  ;;   (unless (null (cdr sletters))
  ;;     (assert (not (closed-component-sletters-p (butlast sletters)))))
  (every 
   (lambda (letter)
     (closed-letter-sletters-p letter sletters parikh))
   (letters (alphabet-of sletters))))

(defmethod closed-component-p ((sword sword) (parikh alphabet))
  (closed-component-p (letters sword) parikh))

(defmethod convex-components ((sletters list))
  (let* ((parikh (parikh-malphabet (malphabet-of sletters)))
	 (closed t)
	 (newsletters
	  (loop
	     while sletters
	     collect
	     (loop
		for component = (list (pop sletters)) then (nconc component (list (pop sletters)))
		do (setf closed (closed-component-p component parikh))
		until closed
		while sletters
		finally (return component)))))
    (values newsletters closed)))

(defmethod convex-components ((sword sword))
  (multiple-value-bind (sletters complete) (convex-components (letters sword))
    (values
     (mapcar #'make-sword sletters)
     complete)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; EXPANSION OF PUNCTUAL LETTERS 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod intervalize-punctual-letter ((letter letter) (sletter sletter))
  (let ((mletters (letters sletter)))
    (unless (member letter mletters :key #'letter)
      (return-from intervalize-punctual-letter (make-sword (list sletter))))
    (let* ((mletter0 (find letter mletters :key #'letter))
	   (mletter1 (next-mletter mletter0))
	   (othermletters (remove mletter0 mletters))
	   (sl0 (make-sletter (list mletter0)))
	   (sl1 (make-sletter (list mletter1))))
      (if (endp othermletters)
	  (make-sword (list sl0 sl1))
	  (make-slanguage
	   (list
	    (make-sword
	     (list
	      sl0
	      (make-sletter (cons mletter1 othermletters))))
	    (make-sword
	     (list (make-sletter (cons mletter0 othermletters)) sl1))
	    (make-sword
	     (list sl0 (make-sletter othermletters) sl1))))))))
      
(defmethod intervalize-punctual-letter ((letter letter) (sletters list))
  (let ((current-sletters sletters))
    (assert (punctualp letter))
    (if (endp current-sletters)
	(empty-sword)
	(let ((prefix
	       (make-sword
		(loop
		   until (member letter (letters (car current-sletters)) :key #'letter)
		   while current-sletters
		   collect (pop current-sletters)))))
	  (unless current-sletters
	    (return-from intervalize-punctual-letter prefix))
	  (let* ((sletter (pop current-sletters))
		 (mletters (letters sletter))
		 (mletter (find letter mletters :key #'letter))
		 (othermletters (remove letter mletters :key #'letter))
		 (tojoin (remove-if-not (lambda (mletter)
					  (if (evenp (mark mletter))
					      (find (next-mletter mletter) (letters (malphabet-of current-sletters)))
					      (find (previous-mletter mletter) (letters (malphabet-of prefix)))))
					othermletters)))
	    (if tojoin
		(let ((join-letters (letters (alphabet-of tojoin))))
		  (intervalize-parallel-punctual-letters
		   (cons (letter mletter) join-letters)
		   (make-sword
		    (compress-punctual-letters join-letters sletters))))
		(make-concatenation
		 (list
		  prefix
		  (make-concatenation
		   (list
		    (intervalize-punctual-letter letter sletter)
		    (intervalize-punctual-letter letter current-sletters)))))))))))

(defmethod intervalize-punctual-letter ((letter letter) (sword sword))
  (assert (punctualp letter))
  (intervalize-punctual-letter letter (letters sword)))

(defmethod intervalize-punctual-letter ((letter letter) (slanguage language))
  (assert (punctualp letter))
  (make-lunion
   (mapcar
    (lambda (sword)
      (intervalize-punctual-letter letter sword))
    (words slanguage))))

(defmethod intervalize-punctual-letter ((letter letter) (sexpr op-lexpr))
;;; voir comment on peut faire sans calculer le language
  (assert (punctualp letter))
  (intervalize-punctual-letter letter (compute-language sexpr)))

(defmethod intervalize-punctual-letters ((letters list) sl)
;; sequential if independent
  (assert (every #'punctualp letters))
  (loop
     for slanguage = sl then (intervalize-punctual-letter letter slanguage)
     for letter in letters
     finally (return slanguage)))

(defmethod intervalize-all-punctual-letters (sl)
  (intervalize-punctual-letters (punctual-letters sl) sl))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PARALLEL
;;; et voir quand la transformation est applicable
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod mix-from-mletters ((mletters list))
  (make-mix
   (mapcar
    (lambda (mletter)
      (make-sword (list (make-sletter (list mletter)))))
    mletters)))
			      
(defmethod intervalize-parallel-punctual-letters ((letters list) (sletter sletter))
  (assert (every #'punctualp letters))
  (let ((mletters (letters sletter)))
    (if (asubsetp (alphabet-of letters) (alphabet-of mletters))
	(make-concatenation
	 (list
	  (mix-from-mletters mletters)
	  (mix-from-mletters (mapcar #'next-mletter mletters))))
	(intervalize-punctual-letters letters sletter))))

(defmethod intervalizedp ((letter letter) sl)
  (let* ((mletter0 (make-mletter letter))
	 (mletter1 (next-mletter mletter0))
	 (mletters (letters (malphabet-of sl))))
    (and (member mletter0 mletters) (member mletter1 mletters))))

(defmethod intervalize-parallel-punctual-letters ((letters list) (sletters list))
  (assert (every #'punctualp letters))
  (assert (notany (lambda (letter)
		  (intervalizedp letter sletters))
		letters))
  (let ((found
	 (find-if (lambda (sletter)
		    (eq (alphabet-of sletter) (make-alphabet letters)))
		  sletters)))
    (if found
	(make-concatenation
	 (mapcar (lambda (sletter) (intervalize-parallel-punctual-letters letters sletter))
		 sletters))
	(let ((found
	       	 (find-if (lambda (sletter)
			    (asubsetp (make-alphabet letters) (alphabet-of sletter)))
			  sletters)))
	  (if found
	      (intervalize-parallel-punctual-letters (letters (alphabet-of found)) sletters)
	      (progn
		(intervalize-punctual-letters letters sletters)))))))
  
(defmethod intervalize-parallel-punctual-letters ((letters list) (sword sword))
  (assert (every #'punctualp letters))
  (intervalize-parallel-punctual-letters letters (letters sword)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RECOMPRESSION OF EXPANDED PUNCTUAL LETTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SEQUENTIAL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod compress-punctual-letter ((letter letter) (sletters list))
  (assert (punctualp letter))
  (multiple-value-bind (ok new-sletters)
      (compress-letter letter sletters)
    (unless ok
      (format *error-output* "compress-letter not ok in compress-punctual-letter ~%"))
    new-sletters))

(defmethod compress-punctual-letter ((letter letter) (sword sword))
  (assert (punctualp letter))
  (compress-letter letter sword))

(defmethod compress-punctual-letter ((letter letter) (slanguage language))
  (assert (punctualp letter))
  (make-slanguage
   (mapcar (lambda (sword)
	     (compress-punctual-letter letter sword))
	   (words slanguage))))

(defmethod compress-punctual-letter ((letter letter) (sexpr op-lexpr))
  (assert (punctualp letter))
  (compress-punctual-letter letter (compute-language sexpr)))


(defmethod compress-punctual-letters ((letters list) slanguage)
  (assert (every #'punctualp letters))
  (loop
     for res = slanguage then (compress-punctual-letter letter res)
     for letter in letters
     finally (return res)))
  
(defmethod compress-all-punctual-letters (slanguage)
  (compress-punctual-letters (punctual-letters slanguage) slanguage))

(defmethod compress-all-punctual-letters ((sexpr op-lexpr))
  (compress-all-punctual-letters (compute-language sexpr)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PARALLEL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod compress-parallel-all-punctual-letters (sexpr)
  (declare (ignore sexpr))
  nil)

(defmethod compress-parallel-all-punctual-letters ((sexpr concatenation))
  (let ((malphabet (malphabet-of sexpr))
	(letters (letters (alphabet-of sexpr))))
      (assert (every #'punctualp letters))
      (let ((oddmletters (remove-if #'evenp (letters malphabet) :key #'mark))
	    (evenmletters (remove-if #'oddp (letters malphabet) :key #'mark)))
	(when (and (delanoyp  (first (args sexpr)) (malphabet-of evenmletters))
		 (delanoyp  (second (args sexpr)) (malphabet-of oddmletters)))
	    (make-sword
	     (list
	      (make-sletter evenmletters)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COMPRESSIONS INTERVALLES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod mletters-from-distinct-occurrences-p ((mletter1 mletter) (mletter2 mletter))
  (or (not (eq (name mletter1) (name mletter2)))
      (let ((m1 (mark mletter1))
	    (m2 (mark mletter2)))
	(and (/= m1 m2)
	     (or (> (abs (- m1 m2)) 1)
		 (evenp (min m1 m2)))))))

(defmethod squeeze ((letter letter) (sl0 sletter) (middle-sletters list) (sl1 sletter))
  (let ((mletters (letters sl0))
	(othermletters
	 (append (letters (malphabet-of middle-sletters))
		 (remove letter (letters sl1) :key #'letter)))
	(letters '()))
    (dolist (mletter othermletters (values t (make-sletter mletters) letters))
      (let ((found (car (member (name mletter) mletters :key #'name))))
	(if found
	    (if (mletters-from-distinct-occurrences-p found mletter)
		(push (letter mletter) letters)
		(return-from squeeze))
	    (push mletter mletters))))))

(defmethod punctualize-one-closed ((letter letter) (sletters list))
  (let ((newsletters '())
	(letters '())
	(evenmletters 
	 (remove-if-not
	  (lambda (mletter)
	    (and (eq (letter mletter) letter) (evenp (mark mletter))))
	  (letters (malphabet-of sletters)))))
    (unless evenmletters
      (return-from punctualize-one-closed (values t sletters nil)))
    (dolist (evenmletter evenmletters)
      (loop
	 while sletters
	 until (member evenmletter (letters (car sletters)))
	 do (push (pop sletters) newsletters))
      (when sletters
	(let ((sl0 (pop sletters))
	      (milieu (loop
			 while sletters
			 until (member (next-mletter evenmletter)
				       (letters (malphabet-of (car sletters))))
			 collect (pop sletters))))
	  (if sletters
	      (progn
		(multiple-value-bind (ok newsletter moreletters)
		    (squeeze letter sl0 milieu (pop sletters))
		  (unless ok
		    (return-from punctualize-one-closed))
		  (push newsletter newsletters)
		  (setf letters (union letters moreletters)))
		(loop
		   while sletters
		   do (push (pop sletters) newsletters)))
	      (setf newsletters (nconc newsletters (cons sl0 milieu)))))))
    (values t (nreverse newsletters) letters)))

;; SMOTS> *w*
;; [{e_0}{e_1,a_0,f_0}{e_2}{a_1,b_0}{f_1}{a_2,f_2,b_1}{a_3}{e_3}{f_3}]
;; SMOTS> (punctualize-one *la* (letters *w*))
;; ({e_0} {e_1,a_0,f_0,b_0} {f_1} {a_2,f_2,b_1} {e_3} {f_3})
;; (e)
;; Faux car il manque e_2
;; voir cet exemple

(defmethod punctualize-all-closed ((letters list) (sletters list))
  (loop
     while letters
     do (multiple-value-bind (ok newsletters moretodo)
	    (punctualize-one-closed (pop letters) sletters)
	  (unless ok
	    (return-from punctualize-all-closed (values nil nil)))
	  (setf letters (union letters moretodo))
	  (setf sletters newsletters)))
  (values t sletters))

(defmethod punctualize-all ((letters list) (sletters list))
  (values
   t
   (loop
      for component in (convex-components sletters)
      nconc (multiple-value-bind (ok sletters)
		(punctualize-all-closed letters component)
	      (unless ok
		(return (values nil sletters)))
	      sletters))))

(defmethod compress-all-letters ((sword sword))
  (multiple-value-bind (ok sletters)
      (punctualize-all (letters (alphabet-of sword)) (letters sword))
    (and ok (make-sword sletters))))

(defmethod compress-letter ((letter letter) (sletters list))
  (punctualize-all (list letter) sletters))

(defmethod compress-letter ((letter letter) (sword sword))
  (multiple-value-bind (ok sletters)
      (compress-letter letter (letters sword))
    (and ok (make-sword sletters))))

(defmethod compress-letter ((letter letter) (lexpr bool-mixin))
  (make-assoc-lexpr 
	   (mapcar (lambda (arg)
		     (compress-letter letter arg))
		  (args lexpr))
	   (class-of lexpr) (wordtype lexpr)))

(defmethod compress-letter-list ((letter letter) (lexprs list))
  (mapcar (lambda (lexpr)
	    (compress-letter letter lexpr))
	  lexprs))

(defmethod compress-letter ((letter letter) (lexpr mix))
  (make-mix
   (compress-letter-list (args lexpr))))

(defmethod compress-letter ((letter letter) (lexpr op-lexpr))
  (format *error-output* "compress-letter by default (compute-language) ~%")
  (compress-letter letter (compute-language lexpr)))
;; on doit pouvoir faire mieux pour la concatenation

(defmethod squeeze-to-sletter (&rest lexprs)
  (make-sletter
   (mapcar
    #'make-mletter
    (letters
     (reduce #'aunion (mapcar #'alphabet-of lexprs))))))

(defmethod compress-letter ((letter letter) (lexpr concatenation))
  (let*
      ((args (args lexpr))
       (m0 (make-mletter letter 0))
       (m1 (make-mletter letter 1))
       (pargs
	(mapcar
	 (lambda (arg)
	   (let ((mletters (letters (malphabet-of arg))))
	     (list arg
		   (member m0 mletters :test #'eq)
		   (member m1 mletters :test #'eq))))
	 args)))
    (if (find-if
	 (lambda (parg)
	   (and (second parg) (third parg)))
	 pargs)
	(make-concatenation
	 (compress-letter-list letter args))
	(let ((prefix
		(loop
		   until (second (car pargs))
		   while pargs
		   collect (first (pop pargs)))))
	  (assert pargs)
	  (let ((hasm0 (car (pop pargs)))
		(middle
		 (loop
		    until (third (car pargs))
		    while pargs
		    collect (first (pop pargs)))))
	    (assert pargs)
	    (let ((hasm1 (car (pop pargs)))
		  (suffix
		   (loop
		      while pargs
		      collect (first (pop pargs)))))
	  (format *trace-output* "prefix ~A hasm0 ~A middle ~A hasm1 ~A suffix~A ~%" prefix hasm0 middle hasm1 suffix)
	      (make-concatenation
	       (append
		prefix
		(list
		 (make-sword (list (squeeze-to-sletter hasm0 middle hasm1))))
		suffix))))))))

(defmethod compress-letter :around ((letter letter) lexpr)
  (if (not (member letter (letters (alphabet-of lexpr))))
      lexpr
      (call-next-method)))

(defmethod compress-letter ((letter letter) (slanguage slanguage))
  (make-slanguage
   (loop
      with csword
      for sword in (words slanguage)
      do (setf csword (compress-letter letter sword))
      when csword
      collect csword)))

(defmethod compress-interval-letter ((letter letter) sl)
  (let* ((ma (malphabet-of sl))
	 (a (alphabet-of sl)))
    (if (not (member letter (letters a) :test #'eq))
	sl
	(let ((m1 (make-mletter letter 1)))
	  (assert (member m1 (letters ma)))
	  (compress-letter letter sl)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; uncompression of compressed intervals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; retourne un langage
(defvar *lazy-compression* nil)

(defmethod compute-middle ((letter letter) (sletter sletter))
  (let* ((mletter0 (find letter (letters sletter) :key #'letter))
	 (mletter1 (make-mletter letter (1+ (mark mletter0))))
	 (othermletters (remove mletter0 (letters sletter)))
	 (sl0 (make-sletter (list mletter0)))
	 (sl1 (make-sletter (list mletter1))))
    ;;		(format t "m0 ~A m1 ~A sl0 ~A sl1 ~A othermletters ~A ~%" mletter0 mletter1 sl0 sl1 othermletters)
    (if (endp othermletters)
;;; on essayer de decompresser de maniere paresseuse: si la mletter est seule dans la sletter
;;; on la laisse telle quelle
	(if *lazy-compression* (make-sword (list sletter)) (make-sword (list sl0 sl1)))
	(make-slanguage
	 (list
	  (make-sword
	   (list
	    sl0
	    (make-sletter (cons mletter1 othermletters))))
	  (make-sword
	   (list (make-sletter (cons mletter0 othermletters)) sl1))
	  (make-sword
	   (list sl0 (make-sletter othermletters) sl1)))))))

(defmethod uncompress-interval-letter-closed ((letter letter) (sletters list))
  (assert (intervalp letter))
  (assert (every #'sletterp sletters))
  (if (endp sletters)
      (empty-sword)
      (let ((prefix
	     (make-sword
	      (loop
		 until (member letter (letters (car sletters)) :key #'letter)
		 while sletters
		 collect (pop sletters)))))
	(unless sletters
	  (return-from uncompress-interval-letter-closed prefix))
	(let* ((middle (compute-middle letter (pop sletters)))
	       (suffix (uncompress-interval-letter-closed letter sletters)))
	  (make-concatenation (list prefix middle suffix))))))

(defmethod uncompress-interval-letter ((letter letter) (sword sword))
  (uncompress-interval-letter letter (letters sword)))

(defmethod uncompress-interval-letter ((letter letter) (slanguage language))
  (reduce #'lunion
	  (mapcar
	   (lambda (sword)
	     (uncompress-interval-letter letter sword))
	   (words slanguage))))

(defmethod uncompress-interval-letter ((letter letter) (lexpr op-lexpr))
  (let ((args (args lexpr)))
    (make-assoc-lexpr
     (mapcar
      (lambda (arg)
	(uncompress-interval-letter letter arg))
      args)
     (type-of lexpr)
     (wordtype lexpr))))
   
(defmethod uncompress-interval-letters ((letters list) sl)
;; sequential if independent
  (loop
     for slanguage = sl then (uncompress-interval-letter letter slanguage)
     for letter in letters
     finally (return slanguage)))

(defmethod uncompress-all-interval-letters (sl)
  (uncompress-interval-letters (interval-letters sl) sl))

(defmethod uncompress-interval-letter ((letter letter) (sletters list))
  (assert (intervalp letter))
  (assert (every #'sletterp sletters))
  (make-concatenation
   (mapcar
    (lambda (component)
      (uncompress-interval-letter-closed letter component))
    (convex-components sletters))))
       
(defgeneric uncompress-interval-letter (letter slanguage)
  (:documentation "uncompress an interval letter previously compressed"))

(defmethod uncompress-interval-letter ((letter letter) (sword sword))
  (assert (intervalp letter))
  (uncompress-interval-letter letter (letters sword)))

(defmethod uncompress-interval-letter ((letter letter) (slanguage language))
  (reduce #'lunion
	  (mapcar
	   (lambda (sword)
	     (uncompress-interval-letter letter sword))
	   (words slanguage))))

(defmethod uncompress-interval-letter :around ((letter letter) (slanguage slanguage))
  (assert (intervalp letter))
  (if (amember letter (alphabet-of slanguage))
      (call-next-method)
      slanguage))
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric uncompress-interval-letters (letters l))

(defmethod uncompress-interval-letters ((letters list) l)
  (loop
     for slanguage = l then (uncompress-interval-letter letter slanguage)
     for letter in letters
     finally (return slanguage)))

(defmethod uncompress-all-interval-letters (l)
  (uncompress-interval-letters (interval-letters l) l))
       
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Passage de A ponctuel à A intervalle:
;; [{a,b}] -> [{a}{b}{a}] [{a,b}{a}] [{a}{a,b}]

;; Passage de A intervalle Ã  A ponctuel:
;; [{a_0}{b}{a_1}{b}] devient {a,b}

;; >
;; > non, car
;; > [{a}{b}{a}{b}] devient {a,b}
;; Si c'est juste A qui devient ponctuel Ã§a fait:
;; [{a}{b}{a}{b}] -> [{a,b}{b}]
;; Si maintenant B devient ponctuel
;; [{a,b}{b}] -> [{a,b}]

;; Dans l'autre sens maintenant:
;; si A devient intervalle
;; [{a,b}] -> [{a}{b}{a}], [{a,b}{a}], [{a}{a,b}]

;; si B devient un intervalle
;; [{a}{b}{a}] -> [{a}{b}{b}{a}]
;; [{a,b}{a}] -> [{b}{a,b}{a}], [{b}{a}{b}{a}], [{a,b}{b}{a}]
;; [{a}{a,b}] -> [{a}{b}{a}{b}], [{a}{b}{a,b}], [{a}{a,b}{b}]
