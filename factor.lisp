(in-package :smots)

(defgeneric factorize (lexpr)
  (:documentation "try to factorize LEXPR"))

(defmethod factorize ((lexpr assoc-lexpr))
  (reconstruct (factor (explode lexpr))))

(defgeneric apply-nodes-gen (lexpr fun))

(defmethod apply-nodes-gen ((assoc-lexpr assoc-lexpr) fun)
   (let ((args (mapcar fun (args assoc-lexpr))))
    (make-assoc-lexpr args (class-of assoc-lexpr) (wordtype assoc-lexpr))))

(defmethod apply-nodes-gen ((sword sword) fun)
  (funcall fun sword))

(defmethod factor ((sword sword))
  sword)

(defmethod factor ((assoc-lexpr assoc-lexpr))
  (let ((*simplify-concatenation* nil))
    (apply-nodes-gen assoc-lexpr #'factor)))

(defun extreme-factorizablep (args &optional (side #'car))
  (and (every #'concatenationp args)
       (= 1 (length
	     (remove-duplicates
	      (mapcar (lambda (arg)
			(funcall side (args arg)))
		      args)
	      :test #'equivalent)))))

(defun left-factorizablep (args)
  (extreme-factorizablep args))

(defun right-factorizablep (args)
  (extreme-factorizablep args (lambda (x) (car (last x)))))

(defmethod factor ((lunion lunion))
;; an exploded expression
;;  (format *error-output* "factor lunion ~A ~%" lunion)
;;  (format *error-output* "factor lunion ~A ~%" lunion)
  (let ((*simplify-concatenation* nil))
    (let ((args (mapcar #'factor (args lunion))))
      (format *error-output* "args ~A ~%" args)
      (cond
	((every #'ljoinp args)
	 (let* ((joinargs (mapcar #'args args))
		(inter (reduce
			(lambda (x y)
			  (intersection x y :test #'equivalent))
			joinargs)))
	   (if inter
	       (make-ljoin
		(cons
		 (factor (make-lunion
			  (mapcar
			   (lambda (args)
			     (make-ljoin
			      (set-difference
			       (args args)
			       inter
			       :test #'equivalent)))
			   args)))
		 inter))
	       (make-lunion args))))
	((left-factorizablep args)
;;	 (format *error-output* "factor first~%")
	 (make-concatenation
	  (list
	   (first (args (first args)))
	   (factor (make-lunion
		    (mapcar
		     (lambda (arg)
		       (make-concatenation (cdr (args arg))))
		     args))))))
      ((right-factorizablep args)
;;       (format *error-output* "factor last~%")
       (make-concatenation
	(list
	 (factor
	  (make-lunion
	   (mapcar (lambda (arg)
		     (make-concatenation (butlast (args arg))))
		   args)))
	 (car (last (args (car args)))))))
      (t (make-lunion args))))))

(defmethod factor-extreme-letter ((lexpr alanguage) &optional (last nil))
;;  (format *error-output* "factor-extreme-letter alanguage ~%")
  (let ((sls (extreme-letters lexpr last)))
    (make-lunion
     (mapcar
      (lambda (sl)
	(let ((sword (make-sword-n (list sl)))
	      (expr (cut-extreme-letter lexpr sl last)))
	(make-concatenation
	 (if last (list expr sword) (list sword expr)))))
     sls))))
				
(defmethod factor-first-letter ((lexpr alanguage))
  (factor-extreme-letter lexpr))

(defmethod factor-last-letter ((lexpr alanguage))
  (factor-extreme-letter lexpr t))
