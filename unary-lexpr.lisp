(in-package :smots)

(defclass unary-lexpr (op-lexpr memo-mixin)
  ((arg :initarg :arg :accessor arg))
  (:documentation "an unary expression"))

(defclass unary-sexpr (unary-lexpr s-mixin) ())

(defmethod args ((unary-lexpr unary-lexpr))
  (list (arg unary-lexpr)))

(defun unary-lexprp (o)
  (typep o 'unary-lexpr))

(defmethod print-object ((lexpr unary-lexpr) stream)
  (format stream "~A~A" (lop lexpr) (arg lexpr)))

(defmethod equivalent ((unary-lexpr1 unary-lexpr) (unary-lexpr2 unary-lexpr))
  (and (eq (lop unary-lexpr1) (lop unary-lexpr2))
       (equivalent (arg unary-lexpr1) (arg unary-lexpr2))))

(defgeneric simplify-unary-lexpr (unary-lexpr)
  (:documentation "simplifies UNARY-LEXPR when it has 0 or 1 argument"))

(defmethod simplify-unary-lexpr ((unary-lexpr unary-lexpr))
  (let ((arg (arg unary-lexpr)))
    (if (empty-wordp arg)
	(empty-sword)
	unary-lexpr)))

(defun make-unary-lexpr (arg classe)
  (simplify-unary-lexpr 
   (make-instance classe
		  :name nil
		  :arg arg
		  :wordtype (wordtype arg))))

;; on suppose pour l'instant toutes les operations unaires idempotentes 
;; il faudra faire une mixin si elles ne le sont plus toutes
(defmethod clean-args ((lexpr unary-lexpr))
  (when *trace-clean-args*
    (format *error-output* "clean-args unary-lexpr ~A~%" lexpr))
  (if (and (unary-lexprp lexpr) (unary-lexprp (arg lexpr))
	   (eq (lop lexpr) (lop (arg lexpr))))
      (arg  lexpr)
      lexpr))
    
(defmethod compute-language ((lexpr unary-lexpr))
;;  (format *error-output* "compute-language unary-lexpr~%")
  (funcall (lfun lexpr)
	   (compute-language (arg lexpr))))

(defgeneric simplify-unary-lexpr (unary-lexpr)
  (:documentation "simplifies UNARY-LEXPR when it has 0 or 1 argument"))

(defmethod weight ((lexpr unary-lexpr))
  (weight (arg lexpr)))

(defmethod complete-with-mletters ((unary-lexpr unary-lexpr) (malphabet alphabet))
  (let ((arg (arg unary-lexpr)))
    (make-unary-lexpr
     (complete-with-mletters arg malphabet)
     (class-of unary-lexpr))))

(defmethod projection ((unary-lexpr unary-lexpr) (alphabet alphabet))
  (let ((arg (projection (arg unary-lexpr) alphabet)))
    (make-unary-lexpr arg (class-of unary-lexpr))))

(defmethod gen-calphabet-of ((unary-lexpr unary-lexpr) lettertype)
;;  (format t "gen-calphabet-of unary-lexpr ~A  ~A~%" unary-lexpr lettertype)
  (gen-alphabet-of (arg unary-lexpr) lettertype))
