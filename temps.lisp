(in-package :smots)
;;; some sletters
(defparameter *r* (make-sletter (list (make-mletter "R"))))
(defparameter *s* (make-sletter (list (make-mletter "S"))))
(defparameter *e1* (make-sletter (list (make-mletter "E1"))))
(defparameter *e2* (make-sletter (list (make-mletter "E2"))))
(defparameter *e3* (make-sletter (list (make-mletter "E3"))))
(defparameter *re2* (make-sletter (list (make-mletter "R") (make-mletter "E2"))))
(defparameter *re3* (make-sletter (list (make-mletter "R") (make-mletter "E3"))))
;;; some swords
(defparameter *wr* (make-sword-n (list *r*)))
(defparameter *ws* (make-sword-n (list *s*)))
(defparameter *we1* (make-sword-n (list *e1*)))
(defparameter *we2* (make-sword-n (list *e2*)))
(defparameter *we3* (make-sword-n (list *e3*)))
(defparameter *wre2* (make-sword-n (list *re2*)))
(defparameter *wre3* (make-sword-n (list *re3*)))

(defparameter *lexpr*
  (make-ljoin
   (list
    (make-concatenation-n
     (list *we1* *wr* *ws*))
    (make-concatenation-n
     (list *wre2* *ws*))
    (make-concatenation-n
     (list *wre3* *ws*)))))
