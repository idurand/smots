(in-package :smots)

(defgeneric list-keys (hash-table &key fun)
  (:documentation "list the keys of a hashtable"))

(defgeneric list-values (hash-table)
  (:documentation "list the values of a hashtable"))

(defmethod list-keys ((ht hash-table) &key (fun #'identity))
  (let ((l nil))
    (maphash #'(lambda (key value) (declare (ignore value))
		       (push (funcall fun key) l))
	     ht)
    l))

(defmethod list-values ((ht hash-table))
    (let ((l nil))
      (maphash #'(lambda (key value) (declare (ignore key))
		       (push value l))
	       ht)
      l))
