(in-package :smots)

;; (defparameter *lc0* (input-sletter "{c_0}"))
;; (defparameter *lc1* (input-sletter "{c_1}"))
;; (defparameter *lp0* (input-sletter "{p_0}"))
;; (defparameter *lp1* (input-sletter "{p_1}"))
;; (defparameter *lb0* (input-sletter "{b_0}"))
;; (defparameter *la0b1* (input-sletter "{a_0,b_1}"))

;; (defparameter *g1*
;;   (make-verified-egraph
;;    (list (make-edge *lc0* *lp1*)
;; 	 (make-edge *lp0* *lp1*)
;; 	 (make-edge *lp1* *lc1*))))

;; (defparameter *g2*
;;   (make-verified-egraph
;;    (list (make-edge *lb0* *lc0*)
;; 	 (make-edge *lc0* *lc1*)
;; 	 (make-edge *lc1* *la0b1*))))

;; (defparameter *g3* ;; cycle
;;   (make-verified-egraph
;;    (list (make-edge *lb0* *lc0*)
;; 	 (make-edge *lc0* *lc1*)
;; 	 (make-edge *lc1* *lb0*))))

;; (defparameter *g4*
;;   (make-verified-egraph
;;    (list (make-edge *lp0* *lp1*))))

;(defparameter *g3* (list (make-edge *lc0* *lp1*) (make-edge *lp0* *lp1*) (make-edge *lp1* *lc1*) (make-edge *lc0* *lc1*)))
;;(defparameter *g4* (list (make-edge *lc0* *lp1*) (make-edge *lp0* *lp1*) (make-edge *lp1* *lp0*) (make-edge *lp1* *lc1*) (make-edge *lc0* *lc1*)))

(defparameter *g*
  (make-verified-egraph
   (list
   ;;  (make-edge
   ;;   (input-sletter "{a}")
   ;;   (input-sletter "{b}"))
   ;; (make-edge
   ;;  (input-sletter "{a}")
   ;;  (input-sletter "{d}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{c}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{e}"))
    (make-edge
     (input-sletter "{d}")
     (input-sletter "{c}"))
    (make-edge
     (input-sletter "{d}")
     (input-sletter "{e}"))
    ;; (make-edge
    ;;  (input-sletter "{c}")
    ;;  (input-sletter "{f}"))
;;     (make-edge
;;      (input-sletter "{e}")
;;      (input-sletter "{f}"))
)))

;; (defparameter *sp*
;;   (make-verified-egraph
;;    (list
;;     (make-edge
;;      (input-sletter "{a}")
;;      (input-sletter "{b}"))
;;     (make-edge
;;      (input-sletter "{a}")
;;      (input-sletter "{d}"))
;;     (make-edge
;;      (input-sletter "{b}")
;;      (input-sletter "{g}"))    
;;     (make-edge
;;      (input-sletter "{d}")
;;      (input-sletter "{g}"))
;;     (make-edge
;;      (input-sletter "{g}")
;;      (input-sletter "{c}"))    
;;     (make-edge
;;      (input-sletter "{g}")
;;      (Input-sletter "{e}"))
;;     (make-edge
;;      (input-sletter "{c}")
;;      (input-sletter "{f}"))
;;     (make-edge
;;      (input-sletter "{e}")
;;      (input-sletter "{f}"))
;;     )))

;; (defparameter *e*
;;   (input-sexpr
;;    "([{a}] . (([{b}] X [{d}]) . ([{c}] X [{e}])) . [{f}])"))
(defparameter *g*
  (make-verified-egraph
   (list
   ;;  (make-edge
   ;;   (input-sletter "{a}")
   ;;   (input-sletter "{b}"))
   ;; (make-edge
   ;;  (input-sletter "{a}")
   ;;  (input-sletter "{d}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{c}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{e}"))
    (make-edge
     (input-sletter "{d}")
     (input-sletter "{c}"))
    (make-edge
     (input-sletter "{d}")
     (input-sletter "{e}"))
    ;; (make-edge
    ;;  (input-sletter "{c}")
    ;;  (input-sletter "{f}"))
;;     (make-edge
;;      (input-sletter "{e}")
;;      (input-sletter "{f}"))
)))

(defparameter *e1*
  (input-sexpr
   "(([{a}{c}{g}] X [{b}{f}{h}]) J ([{b}{e}{g}] J [{a}{d}{h}]))"))

(defparameter *g1*
  (make-verified-egraph
   (list
    (make-edge
     (input-sletter "{a}")
     (input-sletter "{c}"))
    (make-edge
     (input-sletter "{a}")
     (input-sletter "{d}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{ee}"))    
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{f}"))
    (make-edge
     (input-sletter "{c}")
     (input-sletter "{g}"))    
    (make-edge
     (input-sletter "{ee}")
     (input-sletter "{g}"))
    (make-edge
     (input-sletter "{d}")
     (input-sletter "{h}"))
    (make-edge
     (input-sletter "{f}")
     (input-sletter "{h}"))
    )))

(defparameter *g2*
  (make-verified-egraph
   (list
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{f}"))    
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{g}"))
    (make-edge
     (input-sletter "{f}")
     (input-sletter "{j}"))
    (make-edge
     (input-sletter "{c}")
     (input-sletter "{h}"))    
    (make-edge
     (input-sletter "{c}")
     (input-sletter "{i}"))
    (make-edge
     (input-sletter "{h}")
     (input-sletter "{k}"))
    (make-edge
     (input-sletter "{g}")
     (input-sletter "{l}"))
    (make-edge
     (input-sletter "{i}")
     (input-sletter "{l}"))
    (make-edge
     (input-sletter "{a}")
     (input-sletter "{e}"))
    (make-edge
     (input-sletter "{a}")
     (input-sletter "{d}"))
    (make-edge
     (input-sletter "{e}")
     (input-sletter "{k}"))
    (make-edge
     (input-sletter "{d}")
     (input-sletter "{j}"))
    )))


(defparameter *g3*
  (make-verified-egraph
   (list
    (make-edge
     (input-sletter "{a}")
     (input-sletter "{c}"))    
    (make-edge
     (input-sletter "{a}")
     (input-sletter "{d}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{c}"))
    (make-edge
     (input-sletter "{b}")
     (input-sletter "{d}")))))

(defparameter *e3*
  (input-sexpr
   "(([{a}] . ([{c}] X [{d}]))  J ([{b}] . ([{c}] X [{d}])))"))
