(in-package :smots)

(defgeneric alphabets (spec)
  (:documentation "list of all the alphabets defined in SPECIFICATION"))

(defgeneric (setf alphabets) (val spec)
  (:documentation "writer on the list of alphabets defined in SPECIFICATION"))

(defclass alphabet ()
  ((lettertype :initarg :lettertype :initform 'letter :reader lettertype)
   (letters :initform nil :initarg :letters :reader letters))
  (:documentation "set of letters"))

(defvar *internal-alphabets* nil)

(defmethod alphabets ((spec (eql nil)))
  *internal-alphabets*)

(defmethod (setf alphabets) (val (spec (eql nil)))
  (setf *internal-alphabets* val))

(defmethod print-object ((alphabet alphabet) stream)
  (format stream "<")
  (display-sequence (letters alphabet) stream :sep ",")
  (format stream ">"))

(defun make-alphabet (letters &optional (lettertype 'letter))
  (assert (every (lambda (letter) (eq (type-of letter) lettertype))
		 letters))
  (let ((oletters
	 (sort-letters (remove-duplicates letters :from-end t
					  :test #'eq))))
    (or (find-if
	 (lambda (alphabet)
	   (and (eq (lettertype alphabet) lettertype)
		(equal oletters (letters alphabet))))
	 (alphabets *spec*))
	(let ((alphabet (make-instance 'alphabet :letters oletters
				       :lettertype lettertype)))
	  (push alphabet (alphabets *spec*))
	  alphabet))))

(defgeneric asubsetp (alpha1 alpha2)
  (:documentation
   "T if the letters of ALPHA1 are included in the letters of ALPHA2")
  (:method ((alpha1 alphabet) (alpha2 alphabet))
    (subsetp (letters alpha1) (letters alpha2))))

(defgeneric ageneric (alpha1 alpha2 lfun)
  (:documentation "alphabet containing the letters 
                   (LFUN (letters ALPHA1) (letters ALPHA2))")
  (:method ((alpha1 alphabet) (alpha2 alphabet) lfun)
    (let ((letters (funcall lfun (letters alpha1) (letters alpha2))))
      (if letters
	  (make-alphabet letters (type-of (first letters)))
	  (make-alphabet ())))))

(defgeneric aintersection (alpha1 alpha2)
  (:documentation "alphabet containing (intersection (letters ALPHA1)
                   (letters ALPHA2))")
  (:method ((alpha1 alphabet) (alpha2 alphabet))
    (ageneric alpha1 alpha2 #'intersection)))

(defgeneric aunion (alpha1 alpha2)
  (:documentation "alphabet containing (union (letters ALPHA1)
                   (letters ALPHA2))")
  (:method ((alpha1 alphabet) (alpha2 alphabet))
    (ageneric alpha1 alpha2 #'union)))

(defgeneric aset-difference (alpha1 alpha2)
  (:documentation "alphabet containing (set-difference (letters ALPHA1)
                   (letters ALPHA2))")
  (:method ((alpha1 alphabet) (alpha2 alphabet))
    (ageneric alpha1 alpha2 #'set-difference)))

(defgeneric aemptyp (alpha)
  (:documentation "T is ALPHA contains no letter")
  (:method ((alphabet alphabet)) (endp (letters alphabet))))

(defgeneric alength (alpha)
  (:documentation "number of letters in ALPHA")
  (:method ((alphabet alphabet))
    (length (letters alphabet))))

(defgeneric projection-letters (letters alphabet)
  (:method ((letters list) (alphabet alphabet))
    (intersection
     letters
     (letters alphabet)
     :test  (if (eq (type-of (car (letters alphabet))) 'letter)
		#'equiv-named-letters
		#'eq))))

(defgeneric projection (objects alphabet)
  (:documentation
   "remove in OBJECTS all the letters not in ALPHABET")
  (:method ((alpha alphabet) (alphabet alphabet))
    (let ((letters (letters alpha)))
      (if (endp letters)
	  alpha
	  (make-alphabet
	   (projection-letters letters alphabet)
	   (type-of (car letters))))))
  (:method ((lexprs list) (alphabet alphabet))
    (remove-duplicates
     (mapcar (lambda (lexpr) (projection lexpr alphabet))
	     lexprs))))

(defmethod gen-calphabet-of ((alphabet alphabet) lettertype)
  (gen-calphabet-of (letters alphabet) lettertype))

(defmethod gen-calphabet-of ((pair cons) lettertype)
;;  (format *error-output* "gen-calphabet-of pair ~A ~%" lettertype)
  (make-alphabet
   (append (letters (gen-alphabet-of (car pair) lettertype))
	   (letters (gen-alphabet-of (cdr pair) lettertype)))
   lettertype))

(defmethod gen-calphabet-of ((l list) lettertype)
;;  (format *error-output* "gen-calphabet-of list ~A ~%" lettertype)
  (make-alphabet
   (mappend (lambda (e) (letters (gen-alphabet-of e lettertype)))
	    l)
   lettertype))

(defgeneric amember (letter alphabet)
  (:documentation "T when LETTER belongs to ALPHABET"))

(defmethod amember ((letter abstract-letter) (alphabet alphabet))
  (member letter (letters alphabet) :test #'eq))
