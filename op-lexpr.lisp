(in-package :smots)
;;; expressions sur les langages
(defclass op-lexpr (alanguage memo-mixin) ())

;; every lexpr is elementary except unions
(defmethod elementaryp ((op-lexpr op-lexpr))
  (every #'elementaryp (args op-lexpr)))

(defmethod directedp ((op-lexpr op-lexpr))
  (every #'directedp (args op-lexpr)))

(defun op-lexprp (o)
  (typep o 'op-lexpr))

(defgeneric lop (op-lexpr)
  (:documentation "lop of op-lexpr"))

(defmethod lop (o)
  (declare (ignore o))
  nil)

(defgeneric lfun (op-lexpr)
  (:documentation "language function associated with the op of the op-lexpr"))

(defmethod lfun ((op-lexpr op-lexpr))
  (lfun-lop (lop op-lexpr)))

(defgeneric cfun (op-lexpr)
  (:documentation "counting function associated with the lop of the op-lexpr"))

(defmethod cfun ((op-lexpr op-lexpr))
  (cfun-lop (lop op-lexpr)))

(defgeneric fun (op-lexpr)
  (:documentation "function associated with the lop of the OP-LEXPR"))

(defmethod fun ((op-lexpr op-lexpr))
  (fun-lop (lop op-lexpr)))

(defmethod priority ((op-lexpr op-lexpr))
  (priority (lop op-lexpr)))

(defgeneric clean-args (op-lexpr)
  (:documentation "clean the args of OP-EXPR"))

(defmethod extreme-letters :around ((op-lexpr op-lexpr) &optional (last nil))
;;  (format *error-output* "extreme-letters :around op-lexpr ~A ~%" (type-of op-lexpr))
  (if (computed op-lexpr)
      (extreme-letters (memo op-lexpr) last)
      (call-next-method)))

(defmethod name ((op-lexpr op-lexpr))
  (let ((*show-mark* t))
    (format nil "~A" op-lexpr)))

;; default cut-extreme-letter should be provided for as much subclasses as possible
(defmethod cut-extreme-letter ((lexpr op-lexpr) (letter abstract-letter) &optional (last nil))
  (declare (ignore letter last))
  (format *error-output* "cut-extreme-letter op-lexpr DEFAULT ~A ~%" (class-of lexpr)))

(defmethod mcalphabet-of ((op-lexpr op-lexpr))
  (malphabet-of (args op-lexpr)))

(defmethod gen-calphabet-of ((op-lexpr op-lexpr) lettertype)
;;  (format *error-output* "gen-calphabet-of op-lexpr t~%")
  (gen-alphabet-of (args op-lexpr) lettertype))
