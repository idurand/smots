(in-package :smots)
(defparameter *lsa* (make-lstar (input-sword "[{a}]")))
(defparameter *lsab* (make-lstar (make-lunion (list (input-sword "[{a}]") (input-sword "[{b}]")))))
(defparameter *sla* (input-sletter "{a}"))
(defparameter *waba* (input-sword "[{a}{b}{a}]"))
(defparameter *wbab* (input-sword "[{b}{a}{b}]"))
(defparameter *l* (make-lstar (make-lunion (list *waba* *wbab*)))
(defparameter *lsa* (make-lstar (make-lunion (list (input-sword "[{a}]")))))
(defparameter *sla* (input-sletter "{a}"))
(defparameter *w1* (input-sword "[{r1}{g1}{r1,g1}]"))
(defparameter *w2* (input-sword "[{r2}{g2}{r2,g2}]"))
(defparameter *w3* (input-sword "[{r3}{g3}{r3,g3}]"))
(defparameter *c1* (make-mix (list (make-lstar *w1*) (make-lstar *w2*) (make-lstar *w3*))))
(([{r1_0}{g1_0}{r1_1,g1_1}])* X ([{r2_0}{g2_0}{r2_1,g2_1}])* X ([{r3_0}{g3_0}{r3_1,g3_1}])*)
(defparameter *wg1g1* (input-sword "[{g1}{g1}]"))
(defparameter *wg2g2* (input-sword "[{g2}{g2}]"))
(defparameter *wg3g3* (input-sword "[{g3}{g3}]"))

(defparameter *c2* (make-lstar (make-lunion (list *wg1g1* *wg2g2* *wg3g3*))))
([{g1_0}{g1_1}], [{g2_0}{g2_1}], [{g3_0}{g3_1}])*
(defparameter *wrg1* (input-sword "[{r1}{g1}{r1}{g1}]"))
(defparameter *wrg2* (input-sword "[{r2}{g2}{r2}{g2}]"))
(defparameter *wrg3* (input-sword "[{r3}{g3}{r3}{g3}]"))
(defparameter *c3* (make-mix (list (make-lstar *wrg1*)
				   (make-lstar *wrg2*)
				   (make-lstar *wrg3*))))
(([{r1_0}{g1_0}{r1_1}{g1_1}])* X ([{r2_0}{g2_0}{r2_1}{g2_1}])* X ([{r3_0}{g3_0}{r3_1}{g3_1}])*)

SMOTS> *e1*
(([{r1_0}{g1_0}{r1_1,g1_1}])* X ([{r2_0}{g2_0}{r2_1,g2_1}])* X ([{r3_0}{g3_0}{r3_1,g3_1}])*)
SMOTS> *e2*
(([{g1_0}{g1_1}], [{g2_0}{g2_1}], [{g3_0}{g3_1}])* X [{r1_0}{r1_1}] X [{r2_0}{r2_1}] X [{r3_0}{r3_1}])
SMOTS> *sletter*
{r2_0,r3_0}
SMOTS> (inter-cutting-first-letter *e1* *e2* *sletter*)


SMOTS> *e1*
(([{r1}{g1}{r1,g1}])* X ([{r2}{g2}{r2,g2}])* X ([{r3}{g3}{r3,g3}])*)
SMOTS> *sletter*
{r2_0,r3_0}
SMOTS> (cut-first-letter *e1* *sletter*)


(defparameter *e*
  (make-mix
   (list
    (make-lstar (input-sword "[{r1}]"))
    (make-lstar (input-sword "[{r2}]"))
    (make-lstar (input-sword "[{r3}]")))))
