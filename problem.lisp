(in-package :smots)

(defvar *limit-constraints* t)
(defvar *limit-constraints-value* 100)

(defgeneric constraints (problem)
  (:documentation "access to the constraints of a problem"))

(defgeneric (setf constraints) (problem newconstraints)
  (:documentation "modifies the constraints of the problem"))

(defun make-problems () (make-hash-table :test #'equal))

(defclass problem (named-object)
  ((constraints :initform nil
	  :initarg :constraints
	  :accessor constraints)
   (result :initform nil :accessor result)))

(defgeneric add-constraint (constraint problem))

(defmethod add-constraint ((constraint alanguage) (problem problem))
  (unless (member constraint (constraints problem))
    (setf (constraints problem) (append (constraints problem) (list constraint)))))

(defun make-problem (name constraints)
  (make-instance 'problem :name name :constraints constraints))

(defmethod (setf constraints) :after ((problem problem) (newconstraints list))
  (declare (ignore newconstraints))
  (setf (result problem) nil))

(defmethod display-object :after ((problem problem) &optional (stream t))
  (format stream "~%~A"  (constraints problem)))

(defmethod print-object :after ((problem problem) stream)
  (format stream "~%"))

(defmethod size ((problem problem))
  (size (constraints problem)))

(defmethod gen-calphabet-of ((problem problem) lettertype)
;;  (format t "gen-calphabet-of problem t")
  (gen-calphabet-of (constraints problem) lettertype))

(defmethod solve ((problem problem))
;;  (format *error-output* "solve primary~%")
  (let* ((constraints (constraints problem))
	 (lexpr
	  (cond
	    ((endp constraints) (empty-sword))
	    ((null (cdr constraints)) (car constraints))
	    (t
	     (reduce #'ljoin (constraints problem))))))
    (setf (result problem) lexpr)))

(defmethod solve :around ((problem problem))
;;  (format *error-output* "solve around~%")
  (or (result problem)
      (call-next-method)))

