(in-package :smots)
(defvar *smots-readtable* (copy-readtable))

(defun closed-acc-reader (stream char)
  (unread-char char stream))

(set-macro-character #\} #'closed-acc-reader *smots-readtable*)

(defclass kwd (named-object) ())
  
(defclass sym (named-object) ())

(defclass separator (named-object) ())

(defclass operator (named-object) ())

(defclass entier ()
  ((entier :initarg :entier :reader entier)))

(defmethod print-object ((object kwd) stream)
  (format stream "kwd: ~a" (name object)))

(defmethod print-object ((object sym) stream)
  (format stream "sym: ~a" (name object)))

(defmethod print-object ((object separator) stream)
  (format stream "separator: ~a" (name object)))

(defvar +sep-open-par+ (make-instance 'separator :name "("))
(defvar +sep-closed-par+ (make-instance 'separator :name ")"))
(defvar +sep-comma+ (make-instance 'separator :name ","))
(defvar +sep-open-bracket+ (make-instance 'separator :name "["))
(defvar +sep-closed-bracket+ (make-instance 'separator :name "]"))
(defvar +sep-open-acc+ (make-instance 'separator :name "{"))
(defvar +sep-closed-acc+ (make-instance 'separator :name "}"))
(defvar +sep-end+ (make-instance 'separator :name "end"))
(defvar +underscore+ (make-instance 'operator :name "_"))
(defvar +join+ (make-instance 'operator :name "J"))
(defvar +concatenation+ (make-instance 'operator :name "."))
(defvar +union+ (make-instance 'operator :name "U"))
(defvar +intersection+ (make-instance 'operator :name "I"))
(defvar +mix+ (make-instance 'operator :name "X"))
(defvar +star+ (make-instance 'operator :name "*"))
(defvar +mirror+ (make-instance 'operator :name "mirror"))

(defvar *operator-chars* '(#\J #\U #\. #\X #\I #\* #\~))

(defvar *separator-chars*
  '(nil #\Space #\Tab #\Newline #\_ #\( #\) #\: #\, #\; #\[ #\] #\{ #\}))

(defvar *keywords* '("Sword" "Problem" "Slanguage" "Sexpr" "Granularity"))

(defvar *smots-readtable* (copy-readtable))

(let ((*readtable* *smots-readtable*))
(defun lex (stream)
  (let ((buffer (make-array 1 :adjustable t :element-type 'character :fill-pointer 0)))
    ;; skip until next non blank or return nil if EOF
    (loop for ch = (read-char stream nil nil) then (read-char stream nil nil)
	  when (null ch) do (return-from lex +sep-end+)
	  until (not (member ch '(#\Space #\Tab #\Newline)))
	  finally (case ch
		    (#\_ (return-from lex +underscore+))
		    (#\J (return-from lex +join+))
		    (#\. (return-from lex +concatenation+))
		    (#\X (return-from lex +mix+))
		    (#\U (return-from lex +union+))
		    (#\I (return-from lex +intersection+))
		    (#\~ (return-from lex +mirror+))
		    (#\* (return-from lex +star+))
		    (#\( (return-from lex +sep-open-par+))
		    (#\) (return-from lex +sep-closed-par+))
		    (#\[ (return-from lex +sep-open-bracket+))
		    (#\] (return-from lex +sep-closed-bracket+))
		    (#\{ (return-from lex +sep-open-acc+))
		    (#\} (return-from lex +sep-closed-acc+))
		    (#\, (return-from lex +sep-comma+))
		    (#\; (loop for ch = (read-char stream nil nil)
			       then (read-char stream nil nil)
			       when (null ch) do (return-from lex +sep-end+)
			       until (eql ch #\Newline)
			       finally (return-from lex (lex stream))))
		    (#\" (loop for ch = (read-char stream nil nil)
			       then (read-char stream nil nil)
			       when (null ch) do (return-from lex +sep-end+)
			       until (eql ch #\")
			       do (vector-push-extend ch buffer)
			       finally (return-from lex buffer))
;;			       finally (return-from lex (make-instance 'sym :name buffer))
			       )
		    (otherwise (vector-push-extend ch buffer))))
    ;; read chars until a separator is reached
    (loop for ch = (read-char stream nil nil) then (read-char stream nil nil)
	  until (member ch *separator-chars*)
;	  when (null ch) do (return-from lex +sep-end+)
	  do (vector-push-extend ch buffer)
	  finally
	  (progn (unless (null ch) (unread-char ch stream))
		 (return-from lex
		   (cond ((member buffer *keywords*
				  :test #'string=)
			  (make-instance 'kwd :name buffer))
			 ((every #'digit-char-p buffer)
			  (make-instance 'entier :entier (parse-integer buffer)))
			 (t
			  (make-instance 'sym :name buffer))))))))
					)


(defun lex-file (filename)
  (with-open-file (stream filename :direction :input)
    (lex stream)))

(defun lex-stream (stream)
  (loop while (print (lex stream))))
