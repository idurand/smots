(in-package :smots)

(defgeneric words-generic (words1 words2 fun)
  (:documentation "fun of WORDS1 and WORDS2")
  (:method ((words1 list) (words2 list) fun)
    ;; retourne maintenant une seule expression (union)
    (make-lunion
     (loop
      for word1 in words1
      collect
	(make-lunion
	 (loop
	    for word2 in words2
	    collect (funcall fun word1 word2)))))))

(defgeneric words-generic-bool (words1 words2 boolfun)
  (:documentation "boolfun of WORDS1 and WORDS2")
  (:method ((words1 list) (words2 list) boolfun)
    (funcall boolfun words1 words2 :test #'eq)))
;;; peut-etre test equalp?
