(in-package :smots)

(defclass word (language)
  ((letters :initform nil :initarg :letters :reader letters)
   (lettertype :initform 'letter :initarg :lettertype :reader lettertype)))

(defun wordp (o)
  (typep o 'word))

(defmethod empty-wordp (o)
  (and (wordp o) (endp (letters o))))

(defmethod print-object ((word word) stream)
  (format stream "[")
  (display-sequence (letters word) stream :sep "")
  (format stream "]"))

(defgeneric concatenation-n (word1 word2)
  (:documentation "concatenation of WORD1 and WORD1"))

(defgeneric wlength (word)
  (:documentation "length of a word i.e the number of letters of WORD"))

(defmethod wlength (word)
  (length (letters word)))

(defmethod compute-cardinality ((word word))
  1)

(defgeneric words (language)
  (:documentation "the set of words of LANGUAGE")
  (:method ((word word)) (list word)))

(defmethod compute-language ((word word))
  word)

(defmethod extreme-letters ((word word) &optional (last nil))
  (format *error-output* "extreme-letters word~%")
  (let ((letters (letters word)))
    (if last
	(car (last letters))
	(car letters))))

(defmethod size ((word word))
  (length (letters word)))

(defmethod words ((word word))
  (list word))

(defmethod residual ((alanguage alanguage) (word word))
  (residual alanguage (letters word)))

(defmethod gen-calphabet-of ((word word) lettertype)
;;  (format t "gen-calphabet-of word ~A ~A ~%" word lettertype)
  (gen-calphabet-of (letters word) lettertype))
