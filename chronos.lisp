(in-package :smots)
(init)
(defparameter *wp1* (input-sword "[{p1}]"))
(defparameter *wp2* (input-sword "[{p2}]"))
(defparameter *wp3* (input-sword "[{p3}]"))
(defparameter *wp4* (input-sword "[{p4}]"))
(defparameter *wp5* (input-sword "[{p5}]"))
(defparameter *wp6* (input-sword "[{p6}]"))

(defparameter *wp1-p1* (input-sword "[{p1}{p1}]"))
(defparameter *wp2-p2* (input-sword "[{p2}{p2}]"))
(defparameter *wp3-p3* (input-sword "[{p3}{p3}]"))
(defparameter *wp4-p4* (input-sword "[{p4}{p4}]"))
(defparameter *wp5-p5* (input-sword "[{p5}{p5}]"))
(defparameter *wp6-p6* (input-sword "[{p6}{p6}]"))

(defparameter *univers* (delanoy-mix (make-malphabet (mletters *spec*))))

(defparameter *l1*
  (make-concatenation-n
   (list
    (make-mix (list *wp1* *wp2* *wp3*))
    (make-mix (list *wp4-p4* *wp5-p5* *wp6-p6*))
    (make-mix (list *wp1* *wp2* *wp3*)))))

(defparameter *l2*
  (make-concatenation-n
   (list *wp4*
	 (make-mix (list *wp4* *wp5-p5*)))))

(defparameter *l3*
  (make-lunion
   (list 
    (make-concatenation-n
     (list (input-sword "[{p5}{p6}]")
	   (make-mix (list *wp5* *wp6*))))
    (make-concatenation-n
     (list *wp6*
	   (make-mix (list *wp5-p5* *wp6*)))))))

(defparameter *pb* (make-ljoin (list *l1* *l2* *l3*)))
