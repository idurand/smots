(in-package :smots)

(defgeneric reconstruct-gen (lexpr fun)
  (:documentation "reconstructs recursively LEXPR applying fun"))

(defgeneric reconstruct (lexpr)
  (:documentation "reconstructs recursively LEXPR"))

(defgeneric explode (lexpr))

(defmethod explode ((sword sword))
  (make-instance 'concatenation
		 :args (mapcar
			(lambda (sletter)
			  (make-sword (list sletter)))
			(letters sword))
		 :wordtype 'sword))

(defmethod explode ((language language))
  (let ((*simplify-concatenation* nil))
    (make-lunion (mapcar #'explode (words language)))))


(defmethod explode ((assoc-lexpr assoc-lexpr))
  (format *error-output* "explode assoc-lexpr~%")
  (let ((*simplify-concatenation* nil))
    (reconstruct-gen assoc-lexpr #'explode)))

(defmethod reconstruct-gen ((sword sword) fun)
  (funcall fun sword))

(defmethod reconstruct-gen ((lexpr assoc-lexpr) fun)
  (make-assoc-lexpr
   (mapcar (lambda (arg)
	     (reconstruct-gen arg fun))
	   (args lexpr))
   (class-of lexpr)
   (wordtype lexpr)))

(defmethod reconstruct ((lexpr assoc-lexpr))
  (reconstruct-gen lexpr #'identity))
