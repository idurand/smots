(in-package :smots)
;; constants : +sep-open-par+ +sep-closed-par+ +sep-colon+ +sep-arrow+ +sep-comma+
;; +sep-open-bracket+ +sep-closed-bracket+ +sep-open-acc+ +sep-closed-acc+

(define-condition smots-parse-error (error) ())
(define-condition my-keyword-expected-error (smots-parse-error)
  ((my-keyword :reader my-keyword :initarg :my-keyword)))

(define-condition symbol-or-string-expected-error (smots-parse-error) ())
(define-condition integer-expected-error (smots-parse-error) ())
(define-condition colon-expected-error (smots-parse-error) ())
(define-condition comma-expected-error (smots-parse-error) ())
(define-condition arrow-expected-error (smots-parse-error) ())
(define-condition closed-par-expected-error (smots-parse-error) ())
(define-condition open-par-expected-error (smots-parse-error) ())
(define-condition closed-bracket-expected-error (smots-parse-error) ())
(define-condition closed-acc-expected-error (smots-parse-error) ())
(define-condition string-expected-error (smots-parse-error) ())
(define-condition name-expected-error (smots-parse-error) ())
(define-condition unexpected-eof (smots-parse-error) ())

(defvar *current-token* nil)
(defvar *previous-token* nil)

(defun init-parser ()
  (setf *previous-token* nil)
  (setf *current-token* nil))

(defmacro lexmem (stream)
  `(progn
    (setf *previous-token* *current-token*)
    (setf *current-token* (lex ,stream))
    *previous-token*))

(defun my-keyword-p (name)
  (and (eq (type-of *current-token*) 'kwd)
       (string-equal (name *current-token*) name)))

(defun end-of-file-p ()
  (or (null *current-token*)
      (and (eq (type-of *current-token*) 'separator)
	   (string-equal (name *current-token*) "end"))))

(defun symbol-or-string-p ()
  (or (eq (type-of *current-token*) 'sym)
      (stringp *current-token*)))

(defun scan-comma (stream)
  (or (eq +sep-comma+ (lexmem stream))
      (progn
	(format *error-output* "~A: comma expected~%" (name *previous-token*))
	(signal 'comma-expected-error))))

(defun parse-comma (stream)
  (lexmem stream)
  (scan-comma stream))

(defun scan-closed-acc (stream)
  (or (eq +sep-closed-acc+ (lexmem stream))
      (progn
	(format *error-output* "~A: closed-acc expected~%" (name *previous-token*))
	(signal 'closed-acc-expected-error))))

(defun parse-closed-acc (stream)
  (lexmem stream)
  (scan-closed-acc stream))

(defun scan-open-acc (stream)
  (or (eq +sep-open-acc+ (lexmem stream))
      (progn
	(format *error-output* "~A: open-acc expected~%" (name *previous-token*))
	(signal 'open-acc-expected-error))))

(defun parse-open-acc (stream)
  (lexmem stream)
  (scan-open-acc stream))

(defun scan-open-par (stream)
  (or (eq +sep-open-par+ (lexmem stream))
      (if (end-of-file-p)
	  (format *error-output* "Warning: missing closing parenthesis~%")
	  (progn
	    (format *error-output* "~A: open-par expected~%" (name *current-token*))
	    (signal 'open-par-expected-error)))))

(defun parse-open-par (stream)
  (lexmem stream)
  (scan-open-par stream))

(defun scan-closed-par (stream)
  (or (eq +sep-closed-par+ (lexmem stream))
      (if (end-of-file-p)
	  (format *error-output* "Warning: missing closing parenthesis~%")
	  (progn
	    (format *error-output* "~A: closed-par expected~%" (name *current-token*))
	    (signal 'closed-par-expected-error)))))

(defun parse-closed-par (stream)
  (lexmem stream)
  (scan-closed-par stream))

(defun scan-open-bracket (stream)
  (or (eq +sep-open-bracket+ (lexmem stream))
      (if (end-of-file-p)
	  (format *error-output* "Warning: missing closing bracket~%")
	  (progn
	    (format *error-output* "~A: open-bracket expected~%" (name *current-token*))
	    (signal 'open-bracket-expected-error)))))

(defun parse-open-bracket (stream)
  (lexmem stream)
  (scan-open-bracket stream))

(defun scan-closed-bracket (stream)
  (or (eq +sep-closed-bracket+ (lexmem stream))
      (if (end-of-file-p)
	  (format *error-output* "Warning: missing closing bracket~%")
	  (progn
	    (format *error-output* "~A: closed-bracket expected~%" (name *current-token*))
	    (signal 'closed-bracket-expected-error)))))

(defun parse-closed-bracket (stream)
  (lexmem stream)
  (scan-closed-bracket stream))

(defun scan-name (stream)
  (if (symbol-or-string-p)
      (if 
       (eq (type-of *current-token*) 'sym)
       (format nil "~A" (name (lexmem stream)))
       (lexmem stream))
      (progn
	(format *error-output* "~A: symbol or string expected~%" (if (eq (type-of *current-token*) 'sym)
							(name *current-token*)
							*current-token*))
		
	(signal 'symbol-or-string-expected-error))))

(defun parse-name (stream)
  (lexmem stream)
  (scan-name stream))

(defun parse-entier (stream)
  (lexmem stream)
  (if (eq (type-of *current-token*) 'entier)
      (entier *current-token*)
      (format *error-output* "~A: entier expected~%" *current-token*)))

(defun scan-mletter (stream)
  (let ((name (scan-name stream))
	(mark 0))
    (when (eq *current-token* +underscore+)
      (setf mark (parse-entier stream))
      (lexmem stream))
  (make-mletter name mark)))

(defun parse-mletter (stream)
  (lexmem stream)
  (scan-mletter stream))

(defun scan-mletters (stream)
  (let ((mletters nil))
    (do ()
	((not (symbol-or-string-p)))
      (push (scan-mletter stream) mletters)
      (when (eq *current-token* +sep-comma+)
	(scan-comma stream)))
    (nreverse mletters)))

(defun parse-mletters (stream)
  (lexmem stream)
  (scan-mletters stream))

(defun scan-sletter (stream)
  (scan-open-acc stream)
  (prog1
      (make-sletter (scan-mletters stream))
    (scan-closed-acc stream)))

(defun parse-sletter (stream)
  (parse-open-acc stream)
  (prog1
      (make-sletter (scan-mletters stream))
    (scan-closed-acc stream)))

(defun scan-sletters (stream)
  (let ((sletters nil))
    (do ()
	((not (eq *current-token* +sep-open-acc+)))
      (push (scan-sletter stream) sletters)
      (when (eq *current-token* +sep-comma+)
	(scan-comma stream)))
    (nreverse sletters)))

(defun parse-sletters (stream)
  (lexmem stream)
  (scan-sletters stream))

(defun scan-sword-n (stream &optional (number t))
  (scan-open-bracket stream)
  (prog1
      (let* ((sletters (scan-sletters stream))
	     (max (index-max sletters))
	     (number? (or number (zerop max))))
	(make-sword-n sletters :number number?))
    (scan-closed-bracket stream)))

(defun parse-sword-n (stream &optional (number t))
  (lexmem stream)
  (scan-sword-n stream number))

(defun parse-sword (stream)
  (parse-sword-n stream nil))

(defun scan-swords-n (stream &optional (number t))
  (let ((swords nil))
    (do ()
	((not (eq *current-token* +sep-open-bracket+)))
      (push (scan-sword-n stream number) swords)
      (when (eq *current-token* +sep-comma+)
	(scan-comma stream)))
    (nreverse swords)))

(defun scan-swords (stream)
  (scan-swords-n stream nil))
 
(defun parse-swords-n (stream &optional (number t))
  (lexmem stream)
  (scan-swords-n stream number))

(defun parse-swords (stream)
  (parse-swords-n stream nil))

(defun scan-slanguage (stream)
;;  (format *error-output* "~A~%" *current-token*)
  (scan-open-acc stream)
  (prog1
      (make-slanguage (scan-swords stream))
    (scan-closed-acc stream)))

(defun parse-slanguage (stream)
  (lexmem stream)
  (scan-slanguage stream))

(defun scan-operator (stream)
  (declare (ignore stream))
;;  (format *error-output* "scan-operator ~A ~A ~%" *current-token* (type-of *current-token*))
  (cond
    ((eq *current-token* +concatenation+) 'concatenation)
    ((eq *current-token* +mix+) 'mix)
    ((eq *current-token* +join+) 'ljoin)
    ((eq *current-token* +union+) 'lunion)
    ((eq *current-token* +intersection+) 'intersection)
    ))

(defun parse-operator (stream)
  (lexmem stream)
  (scan-operator stream))

(defun scan-this-operator (stream op)
  (assert (eq (scan-operator stream) op)))

(defun parse-this-operator (stream op)
  (lexmem stream)
  (scan-this-operator stream op))

(defun scan-infix-sexpr-n (stream &optional (number t))
  (if (eq *current-token* +sep-closed-par+)
      (empty-sword)
      (let ((lexpr (scan-sexpr-n stream number)))
	(unless (eq *current-token* +sep-closed-par+)
	  (let* ((op-class (scan-operator stream))
		 (arg2 (parse-sexpr-n stream number))
		 (moreargs (loop
			    until (or (end-of-file-p)
				      (eq *current-token* +sep-closed-par+))
			    do (scan-this-operator stream op-class)
			    collect (parse-sexpr-n stream number)))
		 (args (append (list lexpr arg2) moreargs)))
	    (when (eq op-class 'concatenation)
	      (setf args (set-sequential-marks args)))
	    (setf lexpr 
		  (make-assoc-lexpr
		   args
		   op-class
		   'sword))))
	(scan-closed-par stream)
	(when (or number (zerop (index-max lexpr)))
	  (setf lexpr (set-marks lexpr)))
	lexpr)))

(defun parse-infix-sexpr-n (stream &optional (number t))
  (lexmem stream)
  (scan-infix-sexpr-n stream number))

(defun scan-sexpr-n (stream &optional (number t))
;;    (format *error-output* "~A" *current-token*)
  (cond
    ((eq *current-token* +star+)
     (scan-operator stream)
     (let ((lexpr (parse-sexpr-n stream number)))
       (assert (lunionp lexpr))
       (make-lstar lexpr)))
    ((eq *current-token* +mirror+)
     (scan-operator stream)
     (let ((lexpr (parse-sexpr-n stream number)))
       (make-mirror lexpr)))
    ((eq *current-token* +sep-open-bracket+)
     (scan-sword-n stream number))
    (t
     (scan-open-par stream)
     (scan-infix-sexpr-n  stream number))))

(defun scan-sexpr (stream)
  (scan-sexpr-n stream nil))

(defun scan-sexprs (stream)
  (scan-sexprs-n stream nil))

(defun parse-sexpr-n (stream &optional (number t))
  (lexmem stream)
  (scan-sexpr-n stream number))

(defun parse-sexpr (stream)
  (parse-sexpr-n stream nil))

(defun scan-sexprs-n (stream &optional (number t))
  (do ((lexprs))
      ((or (end-of-file-p) (eq (type-of *current-token*) 'kwd))
       (nreverse lexprs))
;;    (format *error-output* "do scan-sexprs ~A ~%" *current-token*)
    (push (scan-sexpr-n stream number) lexprs)))

(defun parse-sexprs-n (stream &optional (number t))
  (lexmem stream)
  (scan-sexprs-n stream number))

(defun parse-sexprs (stream)
  (parse-sexprs-n stream nil))

(defun scan-named-slanguage (stream)
  (let ((name (scan-name stream))
	(language (scan-slanguage stream)))
    (rename-object language name)
    language))

(defun parse-named-slanguage (stream)
  (lexmem stream)
  (scan-named-slanguage stream))
  
(defun scan-named-problem (stream)
  (let ((name (scan-name stream)))
    (make-problem name (scan-sexprs-n stream))))
 
(defun parse-named-problem (stream)
  (lexmem stream)
  (scan-named-problem stream))
  
(defun parse-spec (stream)
    (init-parser)
    (lexmem stream)
    (do ()
	((or (end-of-file-p)
	     (not (eq (type-of *current-token*) 'kwd))))
;;      (format *error-output* "~A ~%" *current-token*)
      (cond
	((my-keyword-p "Problem") (set-current-problem (parse-named-problem stream)))
	((my-keyword-p "Slanguage") (set-current-slanguage (parse-named-slanguage stream)))
	((my-keyword-p "Sword") (set-current-sword (parse-sword stream)))
	((my-keyword-p "Sexpr") (set-current-sexpr (parse-sexpr stream)))
      ))
;;    (format *error-output* "~A" *current-token*)
    )
