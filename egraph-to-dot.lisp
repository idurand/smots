(in-package :smots)

(defmethod egraph-to-dot ((egraph egraph) &optional (stream t))
  (let ((freenodes (egraph-freenodes egraph))
	(edges (egraph-edges egraph)))
    (format stream "digraph ~S {~%" "egraph")
    (loop for edge in edges
	 do (let ((ori (edge-origin edge))
		  (ext (edge-extremity edge)))
	      (format stream "~S -> ~S;~%" (format nil "~A" ori) (format nil "~A" ext))))
    (loop for node in freenodes
	 do (format stream "~S;~%" node))
    (format stream "}~%")))

(defgeneric egraph-to-dotfile (egraph name))

(defmethod egraph-to-dotfile ((egraph egraph) (name string))
  (with-open-file (stream (format nil "/tmp/~A.dot" name)
    :direction :output :if-exists :overwrite :if-does-not-exist :create)
    (egraph-to-dot egraph stream)))

(defmethod dot-to-pdf ((name string))
  (let ((program-name
	 (concatenate
	  'string
	  (sb-ext::posix-getenv "HOME")
	  "/bin/my-dot")))
    (sb-ext:run-program program-name (list name))))

(defun display-egraph (name)
  (sb-ext:run-program
   "/usr/bin/xpdf"
   (list (format nil "/tmp/~A.pdf" name))))

(defun show-egraph (egraph &key (name "egraph"))
  (egraph-to-dotfile egraph name)
  (dot-to-pdf name)
  (display-egraph name))
