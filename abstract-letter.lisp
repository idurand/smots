(in-package :smots)

(defclass abstract-letter (alpha-object)
  ((number :initform (incf (letter-counter *spec*)) :reader letter-number))
  (:documentation "class for general letters"))

(defgeneric letter-number (letter)
  (:documentation "internal number of LETTER, useful for sorting letters"))

(defgeneric letter-counter (specification)
  (:documentation "value of internal counter of letters in specification"))

(defgeneric (setf letter-counter) (val specification)
  (:documentation "setf of internal counter of letters in specification"))

(defgeneric letter-value (letter)
  (:documentation "an artificial value for a letter for sorting letters")
  (:method ((letter abstract-letter)) (letter-number letter)))

(defun sort-letters (letters)
  (sort (copy-list letters) #'< :key #'letter-value))
