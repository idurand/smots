(in-package :smots)

(defgeneric delanoyp (alanguage malphabet)
  (:documentation "does ALANGUAGE represent a delanoys language"))

(defmethod delanoyp ((alanguage alanguage) (alpha alphabet))
  nil)

