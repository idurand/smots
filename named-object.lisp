(in-package :smots)

(defgeneric name (object)
  (:documentation "name of any object"))

(defclass named-object ()
  ((name :initarg :name
	 :initform "unamed"
	 :accessor name)))

(defmethod compare-object ((o1 named-object) (o2 named-object))
  (eq (name o1) (name o2)))

(defmethod print-object ((object named-object) stream)
  (format stream "~A" (name object))
  object)

(defmethod display-object ((object named-object) &optional (stream t))
  (format stream "~A" (name object)))

(defun rename-object (object name)
  (setf (name object) name) object)
