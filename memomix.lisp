 (in-package :smots)

(defvar *memoize-mix-swords* *memoize*)

;;; env est une liste de couple (mletter, mletters)

(defun gen-env (mletters1 mletters2 sword1 sword2)
  (append
   (mapcar #'cons mletters1
	   (letters sword1))
   (mapcar #'cons mletters2
	   (letters sword2))))

(defgeneric substitution-in-sletter (sletter env)
  (:documentation ""))

(defmethod substitution-in-sletter ((sletter sletter) (env list))
  (let ((mletters (letters sletter)))
    (make-sletter
      (mappend (lambda (mletter)
		 (letters (cdr (assoc mletter env :test #'eq))))
	       mletters))))

(defmethod substitution-in-sletter ((sword sword) (env list))
  (let ((sletters (letters sword)))
    (make-sword
     (mapcar (lambda (sletter)
	       (substitution-in-sletter sletter env))
	     sletters))))

(defmethod substitution-in-sletter ((language language) (env list))
  (make-language
   (mapcar (lambda (sword)
	     (substitution-in-sletter sword env))
	   (words language))
   :wordtype (wordtype language)
   :languagetype (languagetype language)))

(defmethod substitution-in-sletter ((assoc-lexpr assoc-lexpr) (env list))
  (make-assoc-lexpr
   (mapcar (lambda (e) (substitution-in-sletter e env)) (arg assoc-lexpr))
   (class-of assoc-lexpr)
   (wordtype assoc-lexpr)))

(defun value-of-pair (n1 n2)
  (+ (* 10 n1) n2))

(defmethod value-of-pair-of-swords ((sword1 sword) (sword2 sword))
  (value-of-pair (size sword1) (size sword2)))

(defgeneric make-n-mletters-with-letter (letter n &optional internal))

(defmethod make-n-mletters-with-letter ((letter letter) (mark integer) &optional (internal nil))
  (mapcar (lambda (i)
	    (if internal
		(make-instance 'mletter :letter letter :mark i)
		(make-mletter letter i)))
	  (iota mark)))

(defgeneric make-n-internal-mletter (n)
  (:documentation "creates a list of n mletters indexed 0,...,n-1 based on a new internal letter"))

(defmethod make-n-internal-mletters ((n integer))
  (make-n-mletters-with-letter (make-unique-internal-letter) n t))

(defun make-simple-sword-from-mletters (mletters)
  (make-sword (mapcar
	       (lambda (mletter) (make-sletter (list mletter)))
	       mletters)))

(defgeneric mix-with-equivalence (sword1 sword2))

(defmethod mix-with-equivalence ((sword1 sword) (sword2 sword))
;;  (format *error-output* "mix-with-equivalence ~A  ~A ~%"sword1 sword2)
  (assert (not (and (simple-swordp sword1) (simple-swordp sword2))))
  (let* ((l1 (size sword1))
	 (l2 (size sword2))
	 (n (value-of-pair l1 l2))
	 (found 
	  (gethash n (mix-basic-sword-table *spec*)))
	 (mletters1 (if found
			(first found)
			(make-n-internal-mletters l1)))
	 (mletters2 (if found
			(second found)
			(make-n-internal-mletters l2)))
	 (env (gen-env mletters1 mletters2 sword1 sword2))
	 (simple-mix (if found
			 (third found)
			 (mix
			  (make-simple-sword-from-mletters mletters1)
			  (make-simple-sword-from-mletters mletters2))))
	 (res-mix  (substitution-in-sletter simple-mix env)))
    (unless found 
      (setf (gethash n (mix-basic-sword-table *spec*))
	    (list mletters1 mletters2 simple-mix)))
;;    (assert (eq (alphabet-of (list sword1 sword2)) (alphabet-of res-mix)))
;;    (assert (equivalent res-mix (brute-mix sword1 sword2)))
    res-mix))
 
(defmethod mix :around ((sword1 sword) (sword2 sword))
  (if *memoize-mix-swords*
      (or (gethash (list sword1 sword2) (mix-sword-table *spec*))
	  (gethash (list sword2 sword1) (mix-sword-table *spec*))
	  (setf (gethash (list sword1 sword2) (mix-sword-table *spec*))
		(if (and (simple-swordp sword1) (simple-swordp sword2))
		    (call-next-method)
		    (mix-with-equivalence sword1 sword2))))
      (call-next-method)))
