(in-package :smots)
;;; operations specific to slanguages

(defgeneric size (o))

(defgeneric swordp (o)
  (:documentation "sword predicate"))

(defgeneric elementaryp (o)
  (:documentation "slanguage elementary?"))

(defgeneric directedp (o)
  (:documentation "slanguage directed?"))

(defun sexprp (o)
  (typep o 's-mixin))

(defun make-sexprs () (make-hash-table :test #'equal))

(defgeneric weight (lexpr)
  (:documentation "weight of LEXPR: nombre de ilettres"))

(defun add-sexpr (lexpr lexprs)
  (setf (gethash (name lexpr) lexprs) lexpr))

(defgeneric disjoint-alphabets (lexprs)
  (:documentation "true if all the LEXPRS have two by two disjoint alphabets"))

(defmethod disjoint-alphabets ((lexprs list))
  (= (alength (alphabet-of lexprs))
     (apply #'+
	    (mapcar
	     (lambda (lexpr) (alength (alphabet-of lexpr)))
	     lexprs))))

(defgeneric not-intersecting-alphabets (lexprs)
  (:documentation "true if the intersection of the alphabets of all the LEXPRS is empty"))

(defmethod not-intersecting-alphabets ((lexprs list))
  (aemptyp
   (reduce #'aintersection
	   (mapcar #'alphabet-of 
		   lexprs))))

(defun same-alphabets (alanguages)
  (let ((alpha (alphabet-of (car alanguages))))
    (every (lambda (alanguage) (eq alpha (alphabet-of alanguage)))
	   (cdr alanguages))))

(defmethod complete-with-mletters :before ((lexpr s-mixin) (malphabet alphabet))
  (let* ((malpha (malphabet-of lexpr))
	 (alpha (alphabet-from-malphabet malpha)))
    (assert (asubsetp (alphabet-from-malphabet malphabet)  alpha))
    (assert (aemptyp (aintersection malpha malphabet)))))

(defmethod complete-with-mletters :around ((lexpr s-mixin) (malphabet alphabet))
  (if (aemptyp malphabet)
      lexpr
      (call-next-method)))

(defgeneric index-max (alanguage)
  (:documentation "max index of an mletter in ALANGUAGE"))

(defun max-or-zero (l)
  (if (endp l)
      0
      (apply #'max l)))

(defmethod index-max ((alanguages list))
  (max-or-zero (mapcar #'index-max alanguages)))

(defmethod index-max ((alanguage alanguage))
  (max-or-zero (mapcar #'mark (letters (malphabet-of alanguage)))))

(defun write-sexpr (lexpr stream)
  (format stream " Sexpr ~A ~%" lexpr))

(defun write-current-sexpr (stream)
  (write-sexpr (lexpr *spec*) stream))

(defun save-sexpr (file)
  (back-up-file (absolute-filename file))
  (with-open-file (foo (absolute-filename file) :direction :output)
    (if foo
        (progn
          (write-current-sexpr foo)
          (format *standard-output* "Sexpr saved~%"))
        (prog1
            nil
          (format *error-output* "unable to open ~A~%" file)))))

(defmethod contains-empty-sword ((lexpr s-mixin))
  (let ((l (compute-language lexpr)))
    (if l
	(contains-empty-sword l)
	(call-next-method))))
 
(defgeneric parikh-vector (lexpr &optional alphabet)
  (:documentation "Parikh vector of LEXPR"))

(defmethod parikh-vector ((lexpr s-mixin) &optional alphabet)
  (let ((pairs 
	 (get-letter-mark-pairs (malphabet-of lexpr))))
    (when alphabet
	(let ((local-alphabet (alphabet-of lexpr)))
	  (assert (asubsetp local-alphabet alphabet))
	  (setf pairs
		(sort
		 (nconc pairs
			(mapcar (lambda (letter)
				  (cons letter 0))
				(letters
				 (aset-difference alphabet local-alphabet))))
		 #'<
		 :key (lambda (pair) (letter-value (car pair)))))))
	pairs))
