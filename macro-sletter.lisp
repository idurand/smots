(in-package :smots)

(defclass macro-sletter (abstract-sletter)
  ((lexpr :reader lexpr :initarg :lexpr)))

(defmethod print-object ((macro-sletter macro-sletter) stream)
  (format stream "~C~A" #\# (lexpr macro-sletter)))

(defmethod gen-calphabet-of ((macro-sletter macro-sletter) lettertype)
  (gen-alphabet-of (lexpr macro-sletter) lettertype))
  
(defun make-macro-sletter (lexpr)
  (assert (op-lexprp lexpr))
  (make-instance 'macro-sletter :lexpr lexpr))

(defmethod macro-expand ((macro-sletter macro-sletter))
  (lexpr macro-sletter))

(defmethod macro-expand ((sletter sletter))
  (make-sword (list sletter)))

(defmethod macro-expand ((sword sword))
  (make-concatenation (mapcar #'macro-expand (letters sword))))

(defmethod macro-expand ((lexpr op-lexpr))
  (make-assoc-lexpr
   (mapcar
    #'macro-expand (args lexpr))
   (type-of lexpr)
   (wordtype lexpr)))

(defmethod has-macrop (o) nil)

(defmethod has-macrop ((macro-sletter macro-sletter))
  (declare (ignore macro-sletter))
  t)

(defmethod has-macrop ((op-lexpr op-lexpr))
  (some #'has-macrop (args op-lexpr)))

	