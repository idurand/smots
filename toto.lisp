SMOTS> (defparameter *a* (make-letter "a" 1))
Warning letter a already defined with granularity 2 
Changing granularity to 1 
*A*
SMOTS> (defparameter *b* (make-letter "2" 2))
*B*
SMOTS> (defparameter *c* (make-letter "c" 1))
*C*
SMOTS> (defparameter *b* (make-letter "b" 2))
*B*
SMOTS> (defparameter *d* (make-letter "d" 1))
Warning letter d already defined with granularity 2 
Changing granularity to 1 
*D*
SMOTS> (defparameter *e* (make-letter "e" 1))
*E*
SMOTS> (defparameter *f* (make-letter "f" 1))
*F*
SMOTS> (defparameter *g* (make-letter "g" 1))
*G*
SMOTS> (defparameter *a-b* (input-sword "[{a}{b}]"))
*A-B*
SMOTS> (defparameter *b-c* (input-sword "[{b}{c}]"))
*B-C*
SMOTS> (defparameter *db* (input-sword "[{d,b}]"))
*DB*
SMOTS> (defparameter *be* (input-sword "[{b,e}]"))
*BE*
SMOTS> (defparameter *c-fg* (input-sword "[{c}{f,g}]"))
*C-FG*
SMOTS> (defparameter *pb* (list *a-b* *b-c* *db* *be* *c-fg*))
*PB*
SMOTS> *pb*
([{a}{b}] [{b}{c}] [{b,d}] [{b,e}] [{c}{f,g}])
SMOTS> (mapcar (lambda (e) (uncompress-interval-letter *b* e))
	       *pb*)
([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
 {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
SMOTS> (defvar *solution*)
*SOLUTION*
SMOTS> (defparameter *pbdecompresse* 
	 (mapcar (lambda (e) (uncompress-interval-letter *b* e))
	       *pb*))
*PBDECOMPRESSE*
SMOTS> (setf *solution* (reduce #'ljoin *pbdecompresse*))
mix [{b}] [{c}]; Evaluation aborted.
SMOTS> *pbdecompresse*

([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
 {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 1 *pbdecompresse*))
[{a}{b}{c}]
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 2 *pbdecompresse*))
{[{a}{b}{b,d}], [{a}{b,d}{b}], [{a}{b}{d}{b}]} 
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 3 *pbdecompresse*))
{[{a}{b}{b,e}], [{a}{b,e}{b}], [{a}{b}{e}{b}]} 
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 4 *pbdecompresse*))
([{a}{b}] X [{c}{f,g}])
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 5 *pbdecompresse*))
; Evaluation aborted.
SMOTS> (nth 5 *pbdecompresse*
	    )
NIL

(make-lunionSMOTS> (defparameter *pbdecompresse* 
		     (make-lunion
	 (mapcar (lambda (e) (uncompress-interval-letter *b* e))
	       *pb*)))
*PBDECOMPRESSE*
SMOTS> *pbdecompresse*
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
; compiling (DEFUN MAKE-LUNION ...)

; file: /tmp/fileb6PaII
; in: DEFUN MAKE-LUNION
;     (REDUCE #'SMOTS:LUNION SMOTS::LEXPR)
; 
; caught WARNING:
;   undefined variable: LEXPR
; 
; compilation unit finished
;   Undefined variable:
;     LEXPR
;   caught 1 WARNING condition
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
([{a}{b}] U [{b}{c}] U {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}  U {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  U [{c}{f,g}])
SMOTS> (defparameter *pbdecompresse* 
		     (make-lunion
	 (mapcar (lambda (e) (uncompress-interval-letter *b* e))
	       *pb*)))
*PBDECOMPRESSE*
SMOTS> *pbdecompresse*
{[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
SMOTS> (setf *solution* (reduce #'ljoin *pbdecompresse*))
; Evaluation aborted.
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
*PBDECOMPRESSE*
SMOTS> (trace lunion)
(LUNION)
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
*PBDECOMPRESSE*
SMOTS> (trace make-lunion)
(MAKE-LUNION)
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
  0: (MAKE-LUNION ([{b}{b,d}]))
  0: MAKE-LUNION returned [{b}{b,d}]
  0: (MAKE-LUNION ([{b,d}{b}]))
  0: MAKE-LUNION returned [{b,d}{b}]
  0: (MAKE-LUNION ([{b}{d}{b}]))
  0: MAKE-LUNION returned [{b}{d}{b}]
  0: (MAKE-LUNION ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}]))
  0: MAKE-LUNION returned {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} 
  0: (MAKE-LUNION ([{b}{b,e}]))
  0: MAKE-LUNION returned [{b}{b,e}]
  0: (MAKE-LUNION ([{b,e}{b}]))
  0: MAKE-LUNION returned [{b,e}{b}]
  0: (MAKE-LUNION ([{b}{e}{b}]))
  0: MAKE-LUNION returned [{b}{e}{b}]
  0: (MAKE-LUNION ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}]))
  0: MAKE-LUNION returned {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} 
  0: (MAKE-LUNION
      ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
       {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}]))
  0: MAKE-LUNION returned
       {[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
; compiling (DEFMETHOD LUNION ...)
STYLE-WARNING:
   redefining LUNION (#<STANDARD-CLASS SWORD>
                      #<STANDARD-CLASS SWORD>) in DEFMETHOD
*PBDECOMPRESSE*
SMOTS> (untrace)
T
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
*PBDECOMPRESSE*
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
*PBDECOMPRESSE*
SMOTS> (trace make-lunion)
(MAKE-LUNION)
SMOTS> (trace make-lunion)
WARNING: MAKE-LUNION is already TRACE'd, untracing it first.
(MAKE-LUNION)
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
  0: (MAKE-LUNION ([{b}{b,d}]))
  0: MAKE-LUNION returned [{b}{b,d}]
  0: (MAKE-LUNION ([{b,d}{b}]))
  0: MAKE-LUNION returned [{b,d}{b}]
  0: (MAKE-LUNION ([{b}{d}{b}]))
  0: MAKE-LUNION returned [{b}{d}{b}]
  0: (MAKE-LUNION ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}]))
  0: MAKE-LUNION returned {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} 
  0: (MAKE-LUNION ([{b}{b,e}]))
  0: MAKE-LUNION returned [{b}{b,e}]
  0: (MAKE-LUNION ([{b,e}{b}]))
  0: MAKE-LUNION returned [{b,e}{b}]
  0: (MAKE-LUNION ([{b}{e}{b}]))
  0: MAKE-LUNION returned [{b}{e}{b}]
  0: (MAKE-LUNION ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}]))
  0: MAKE-LUNION returned {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} 
  0: (MAKE-LUNION
      ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
       {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}]))
  0: MAKE-LUNION returned
       {[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
*PBDECOMPRESSE*
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
  0: (MAKE-LUNION ([{b}{b,d}]))
make-lunion ([{b}{b,d}])
  0: MAKE-LUNION returned [{b}{b,d}]
  0: (MAKE-LUNION ([{b,d}{b}]))
make-lunion ([{b,d}{b}])
  0: MAKE-LUNION returned [{b,d}{b}]
  0: (MAKE-LUNION ([{b}{d}{b}]))
make-lunion ([{b}{d}{b}])
  0: MAKE-LUNION returned [{b}{d}{b}]
  0: (MAKE-LUNION ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}]))
make-lunion ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}])
  0: MAKE-LUNION returned {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} 
  0: (MAKE-LUNION ([{b}{b,e}]))
make-lunion ([{b}{b,e}])
  0: MAKE-LUNION returned [{b}{b,e}]
  0: (MAKE-LUNION ([{b,e}{b}]))
make-lunion ([{b,e}{b}])
  0: MAKE-LUNION returned [{b,e}{b}]
  0: (MAKE-LUNION ([{b}{e}{b}]))
make-lunion ([{b}{e}{b}])
  0: MAKE-LUNION returned [{b}{e}{b}]
  0: (MAKE-LUNION ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}]))
make-lunion ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}])
  0: MAKE-LUNION returned {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} 
  0: (MAKE-LUNION
      ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
       {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}]))
make-lunion ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
             {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
  0: MAKE-LUNION returned
       {[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
*PBDECOMPRESSE*
SMOTS> (untrace)
T
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
make-lunion ([{b}{b,d}])
make-lunion ([{b,d}{b}])
make-lunion ([{b}{d}{b}])
make-lunion ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}])
make-lunion ([{b}{b,e}])
make-lunion ([{b,e}{b}])
make-lunion ([{b}{e}{b}])
make-lunion ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}])
make-lunion ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
             {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
*PBDECOMPRESSE*
SMOTS> (defparameter 
	   *ls*
	 (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
make-lunion ([{b}{b,d}])
make-lunion ([{b,d}{b}])
make-lunion ([{b}{d}{b}])
make-lunion ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}])
make-lunion ([{b}{b,e}])
make-lunion ([{b,e}{b}])
make-lunion ([{b}{e}{b}])
make-lunion ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}])
; Evaluation aborted.
SMOTS> (defparameter 
	   *ls*
	 (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*))
make-lunion ([{b}{b,d}])
make-lunion ([{b,d}{b}])
make-lunion ([{b}{d}{b}])
make-lunion ([{b}{b,d}] [{b,d}{b}] [{b}{d}{b}])
make-lunion ([{b}{b,e}])
make-lunion ([{b,e}{b}])
make-lunion ([{b}{e}{b}])
make-lunion ([{b}{b,e}] [{b,e}{b}] [{b}{e}{b}])
*LS*
SMOTS> *ls*
([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
 {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
SMOTS> (every #'swordp *ls*)
NIL
SMOTS> (every #'languagep *ls*)
T
SMOTS> (reduce #'lunion *ls*)
lunion swords
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
{[{b}{e}{b}], [{b,e}{b}], [{b}{b,e}], [{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}], [{c}{f,g}]} 
SMOTS> (make-lunion *ls*)
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
{[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
SMOTS> (make-lunion *ls*)
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
{[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
SMOTS> (make-lunion *ls*)
make-lunion  ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
              {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
{[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
SMOTS> (trace lunion)
(LUNION)
SMOTS> (make-lunion *ls*)
make-lunion  ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
              {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
{[{a}{b}], [{b}{c}], {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} , {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} , [{c}{f,g}]} 
SMOTS> (make-lunion *ls*)
make-lunion  ([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
              {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
make-lunion all languages
  0: (LUNION [{a}{b}] [{b}{c}])
lunion swords
  0: LUNION returned {[{a}{b}], [{b}{c}]} 
  0: (LUNION {[{a}{b}], [{b}{c}]}  {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]} )
  0: LUNION returned {[{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}]} 
  0: (LUNION {[{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}]}
             {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} )
  0: LUNION returned
       {[{b}{c}], [{a}{b}], [{b}{b,d}], [{b,d}{b}], [{b}{d}{b}], [{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]} 
  0: (LUNION
      {[{b}{c}], [{a}{b}], [{b}{b,d}], [{b,d}{b}], [{b}{d}{b}], [{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}
      [{c}{f,g}])
  0: LUNION returned
       {[{b}{e}{b}], [{b,e}{b}], [{b}{b,e}], [{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}], [{c}{f,g}]} 
{[{b}{e}{b}], [{b,e}{b}], [{b}{b,e}], [{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}], [{c}{f,g}]} 
SMOTS> (untrace)
; compiling (DEFUN MAKE-LUNION ...)
STYLE-WARNING: redefining MAKE-LUNION in DEFUN
T
SMOTS> (make-lunion *ls*)
lunion swords
{[{b}{e}{b}], [{b,e}{b}], [{b}{b,e}], [{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}], [{c}{f,g}]} 
SMOTS> (reduce #'ljoin (make-lunion *ls*))
lunion swords
; Evaluation aborted.
SMOTS> (reduce #'ljoin (words (make-lunion *ls*)))
lunion swords
{} 
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
lunion swords
*PBDECOMPRESSE*
SMOTS> *pbdecompresse*
{[{b}{e}{b}], [{b,e}{b}], [{b}{b,e}], [{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}], [{c}{f,g}]} 
SMOTS> (toggle-show-mark)
T
SMOTS> *pbdecompresse*
{[{b_0}{e_0}{b_1}], [{b_0,e_0}{b_1}], [{b_0}{b_1,e_0}], [{b_0}{d_0}{b_1}], [{b_0,d_0}{b_1}], [{b_0}{b_1,d_0}], [{a_0}{b_0}], [{b_0}{c_0}], [{c_0}{f_0,g_0}]} 
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 1 *pbdecompresse*))
; Evaluation aborted.
SMOTS> (defparameter *ws* (words *pbdecompresse*))
*WS*
SMOTS> (ljoin (nth 0 *ws*)
	      (nth 1 *ws*))
{} 
SMOTS> (nth 0 *ws*)
[{b_0}{e_0}{b_1}]
SMOTS> (nth 1 *ws*)
[{b_0,e_0}{b_1}]
SMOTS> *pb*
([{a_0}{b_0}] [{b_0}{c_0}] [{b_0,d_0}] [{b_0,e_0}] [{c_0}{f_0,g_0}])
SMOTS> (reduce #'ljoin *pb*)
[{a_0}{b_0,d_0,e_0}{c_0}{f_0,g_0}]
SMOTS> (uncompress-interval-letter *b* (reduce #'ljoin *pb*))
{[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
SMOTS> (setf *w* (reduce #'ljoin *pb*))
[{a_0}{b_0,d_0,e_0}{c_0}{f_0,g_0}]
SMOTS> (uncompress-interval-letter *b* *w*)
{[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
SMOTS> (uncompress-interval-letter *b* (letters *w*))
{[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
SMOTS> (defparameter *sletters* (letters *w*))
*SLETTERS*
SMOTS> (convex-components *sletters*)
(({a_0}) ({b_0,d_0,e_0}) ({c_0}) ({f_0,g_0}))
T
SMOTS> (nth 0 (convex-components *sletters*))
({a_0})
SMOTS> (uncompress-interval-letter-closed (nth 0 (convex-components *sletters*)))
; Evaluation aborted.
SMOTS> (uncompress-interval-letter-closed *b* (nth 0 (convex-components *sletters*)))
[{a_0}]
SMOTS> (uncompress-interval-letter-closed *b* (nth 1 (convex-components *sletters*)))
{[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
SMOTS> (nth 1 (convex-components *sletters*))
({b_0,d_0,e_0})
SMOTS> (setf *sletters* (nth 1 (convex-components *sletters*)))
({b_0,d_0,e_0})
SMOTS> (uncompress-interval-letter-closed *b* *sletters*)
{[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
SMOTS> (setf *prefix*  (make-sword
	      (loop
		 until (member *b* (letters (car *sletters*)) :key #'letter)
		 while *sletters*
		 collect (pop *sletters*))))

; in: LAMBDA NIL
;     (SETF SMOTS::*PREFIX*
;             (SMOTS::MAKE-SWORD
;              (LOOP SMOTS::UNTIL (MEMBER SMOTS::*B* (SMOTS:LETTERS #) :KEY
;                                         #'SMOTS:LETTER)
;                    SMOTS::WHILE SMOTS::*SLETTERS*
;                    SMOTS::COLLECT (POP SMOTS::*SLETTERS*))))
; ==>
;   (SETQ SMOTS::*PREFIX*
;           (SMOTS::MAKE-SWORD
;            (LOOP SMOTS::UNTIL (MEMBER SMOTS::*B* (SMOTS:LETTERS #) :KEY
;                                       #'SMOTS:LETTER)
;                  SMOTS::WHILE SMOTS::*SLETTERS*
;                  SMOTS::COLLECT (POP SMOTS::*SLETTERS*))))
; 
; caught WARNING:
;   undefined variable: *PREFIX*
; 
; compilation unit finished
;   Undefined variable:
;     *PREFIX*
;   caught 1 WARNING condition
[]
SMOTS> *prefix*
[]
SMOTS> *sletters*
({b_0,d_0,e_0})
SMOTS> (defparameter *middle* (compute-middle (car *sletters*)))
; Evaluation aborted.
SMOTS> (defparameter *middle* (compute-middle *e* (car *sletters*)))
*MIDDLE*
SMOTS> *middle*
{[{e_0}{b_0,d_0,e_1}], [{b_0,d_0,e_0}{e_1}], [{e_0}{b_0,d_0}{e_1}]} 
SMOTS> (trace make-concatenation)
(MAKE-CONCATENATION)
SMOTS> (uncompress-interval-letter-closed *b* *sletters*)
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
{[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
SMOTS> (defparameter *sletters* (letters *w*))
*SLETTERS*
SMOTS> (uncompress-interval-letter-closed *b* *sletters*)
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       {[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
{[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
SMOTS> (setf *sletters* (nth 1 (convex-components *sletters*)))
({b_0,d_0,e_0})
SMOTS> (uncompress-interval-letter-closed *b* *sletters*)
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
{[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
SMOTS> (uncompress-interval-letter-closed *b* *sletters*)
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
{[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
SMOTS> (defparameter *sletters* (letters *w*))
*SLETTERS*
SMOTS> (uncompress-interval-letter *b* (letters *w*))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       {[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
{[{a_0}{b_0}{b_1,d_0,e_0}{c_0}{f_0,g_0}], [{a_0}{b_0,d_0,e_0}{b_1}{c_0}{f_0,g_0}], [{a_0}{b_0}{d_0,e_0}{b_1}{c_0}{f_0,g_0}]} 
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       ([] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: (MAKE-CONCATENATION
      ([{a_0}]
       ([] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . [] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
; compiling (DEFMETHOD CLEAN-ARGS ...)

; file: /tmp/fileHIHQMW
; in: DEFMETHOD CLEAN-ARGS :AFTER (CONCATENATION)
;     #'SMOTS::EMPTY-SWORDP
; 
; caught STYLE-WARNING:
;   undefined function: EMPTY-SWORDP
; 
; compilation unit finished
;   Undefined function:
;     EMPTY-SWORDP
;   caught 1 STYLE-WARNING condition
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
; compiling (DEFMETHOD CLEAN-ARGS ...)
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
([{a_0}] . [] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}] . [{f_0,g_0}])
; compiling (DEFMETHOD CLEAN-ARGS ...)
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
; compiling (DEFMETHOD CLEAN-ARGS ...)
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
; compiling (DEFMETHOD CONCATENATION-SIMPLIFY-ARGS-REC ...)
STYLE-WARNING:
   redefining CONCATENATION-SIMPLIFY-ARGS-REC (#<BUILT-IN-CLASS LIST>) in DEFMETHOD
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}] . [{f_0,g_0}])
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: (MAKE-CONCATENATION
      ([{a_0}]
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
SMOTS> (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}{f_0,g_0}])
; Evaluation aborted.
SMOTS> (uncompress-interval-letter *b* (letters *w*))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}{f_0,g_0}])
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}{f_0,g_0}])
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: (MAKE-CONCATENATION
      ([{a_0}]
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
; compiling (DEFMETHOD CONCATENATION-SIMPLIFY-ARGS-REC ...)
STYLE-WARNING:
   redefining CONCATENATION-SIMPLIFY-ARGS-REC (#<BUILT-IN-CLASS LIST>) in DEFMETHOD
; compiling (DEFMETHOD CLEAN-ARGS ...)
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: (MAKE-CONCATENATION
      ([{a_0}]
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
SMOTS> (setf *trace-clean-args* t)
T
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
clean-args assoc-lexpr ([] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
clean args concatenation
  0: MAKE-CONCATENATION returned
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: (MAKE-CONCATENATION
      ([{a_0}]
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
       [{c_0}] [{f_0,g_0}]))
clean-args assoc-lexpr ([{a_0}] . ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . []) . [{c_0}] . [{f_0,g_0}])
clean args concatenation
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
; compiling (DEFMETHOD CLEAN-ARGS ...)
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
clean-args assoc-lexpr ([] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
clean args concatenation ([] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: MAKE-CONCATENATION returned
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
  0: (MAKE-CONCATENATION
      ([{a_0}]
       ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [])
       [{c_0}] [{f_0,g_0}]))
clean-args assoc-lexpr ([{a_0}] . ({[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . []) . [{c_0}] . [{f_0,g_0}])
clean args concatenation ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [] . [{c_0}] . [{f_0,g_0}])
SMOTS> (setf *trace-clean-args* nil)
; compiling (DEFMETHOD CLEAN-ARGS ...)
STYLE-WARNING:
   redefining CLEAN-ARGS :AFTER (#<STANDARD-CLASS CONCATENATION>) in DEFMETHOD
NIL
SMOTS> (let ((*simplify-concatenation* nil)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}] . [{f_0,g_0}])
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}] . [{f_0,g_0}])
SMOTS> (let ((*simplify-concatenation* t)) (uncompress-interval-letter *b* (letters *w*)))
  0: (MAKE-CONCATENATION
      ([] {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       []))
  0: MAKE-CONCATENATION returned
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]} 
  0: (MAKE-CONCATENATION
      ([{a_0}]
       {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}
       [{c_0}] [{f_0,g_0}]))
  0: MAKE-CONCATENATION returned
       ([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}{f_0,g_0}])
([{a_0}] . {[{b_0}{b_1,d_0,e_0}], [{b_0,d_0,e_0}{b_1}], [{b_0}{d_0,e_0}{b_1}]}  . [{c_0}{f_0,g_0}])
SMOTS> (untrace)
T
SMOTS> (toggle-show-mark)
NIL
SMOTS> (let ((*simplify-concatenation* t)) (uncompress-interval-letter *b* (letters *w*)))
([{a}] . {[{b}{b,d,e}], [{b,d,e}{b}], [{b}{d,e}{b}]}  . [{c}{f,g}])
SMOTS> *w*
[{a}{b,d,e}{c}{f,g}]
SMOTS> *pb*
([{a}{b}] [{b}{c}] [{b,d}] [{b,e}] [{c}{f,g}])
SMOTS> (reduce #'ljoin *pb*)
[{a}{b,d,e}{c}{f,g}]
SMOTS> (uncompress-interval-letter (reduce #'ljoin *pb*))
; Evaluation aborted.
SMOTS> (uncompress-interval-letter *b* (reduce #'ljoin *pb*))
([{a}] . {[{b}{b,d,e}], [{b,d,e}{b}], [{b}{d,e}{b}]}  . [{c}{f,g}])
SMOTS> (defparameter *ws* (words *pbdecompresse*))
*WS*
SMOTS> *ws*
([{b}{e}{b}] [{b,e}{b}] [{b}{b,e}] [{b}{d}{b}] [{b,d}{b}] [{b}{b,d}] [{a}{b}]
 [{b}{c}] [{c}{f,g}])
SMOTS> (nbutlast *ws* 4)
([{b}{e}{b}] [{b,e}{b}] [{b}{b,e}] [{b}{d}{b}] [{b,d}{b}])
SMOTS> (reduce #'ljoin (nbutlast *ws* 4))
[{b}{e}{b}]
SMOTS> (reduce #'ljoin (nbutlast *ws* 3))
; Evaluation aborted.
SMOTS> (nbutlast *ws* 3)
NIL
SMOTS> (nbutlast *ws* 5)
NIL
SMOTS> (nbutlast *ws* 1)
NIL
SMOTS> *ws*
([{b}{e}{b}])
SMOTS> (defparameter *ws* (words *pbdecompresse*))
*WS*
SMOTS> (butlast *ws* 4)
NIL
SMOTS> *ws*
([{b}{e}{b}])
SMOTS> (defparameter *ws* (words *pbdecompresse*))
*WS*
SMOTS> *ws*
([{b}{e}{b}])
SMOTS> (defparameter 
	   *pbdecompresse* 
	 (make-lunion
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*)))
lunion swords
*PBDECOMPRESSE*
SMOTS> *pbdecompresse*
{[{b}{e}{b}], [{b,e}{b}], [{b}{b,e}], [{b}{d}{b}], [{b,d}{b}], [{b}{b,d}], [{a}{b}], [{b}{c}], [{c}{f,g}]} 
SMOTS> (defparameter *ws* (words *pbdecompresse*))
*WS*
SMOTS> *ws*
([{b}{e}{b}] [{b,e}{b}] [{b}{b,e}] [{b}{d}{b}] [{b,d}{b}] [{b}{b,d}] [{a}{b}]
 [{b}{c}] [{c}{f,g}])
SMOTS> (butlast *ws* 2)
([{b}{e}{b}] [{b,e}{b}] [{b}{b,e}] [{b}{d}{b}] [{b,d}{b}] [{b}{b,d}] [{a}{b}])
SMOTS> (butlast *ws* 3)
([{b}{e}{b}] [{b,e}{b}] [{b}{b,e}] [{b}{d}{b}] [{b,d}{b}] [{b}{b,d}])
SMOTS> (reduce #'ljoin (butlast *ws* 3))
{} 
SMOTS> (defparameter 
	   *pbdecompresse* 
	  (mapcar (lambda (e) (uncompress-interval-letter *b* e))
		  *pb*))
*PBDECOMPRESSE*
SMOTS> *pbdecompresse*
([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
 {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
SMOTS> (reduce #'ljoin *pbdecompresse*)
; Evaluation aborted.
SMOTS> *pbdecompresse*
([{a}{b}] [{b}{c}] {[{b}{b,d}], [{b,d}{b}], [{b}{d}{b}]}
 {[{b}{b,e}], [{b,e}{b}], [{b}{e}{b}]}  [{c}{f,g}])
SMOTS> (ljoin (nth 0 *pbdecompresse*)
	      (nth 1 *pbdecompresse*))
[{a}{b}{c}]
SMOTS> (reduce #'ljoin (nth 0 *pbdecompresse*)
	      (nth 1 *pbdecompresse*)	      (nth 2 *pbdecompresse*))
; Evaluation aborted.
SMOTS> (reduce #'ljoin (list (nth 0 *pbdecompresse*)
	      (nth 1 *pbdecompresse*)	      (nth 2 *pbdecompresse*))) 
(([{a}{b}{d}{b}{c}] U [{a}{b}{d}{b,c}] U ([{a}{b}] . ([{c}] X [{d}]) . [{b}])) U {[{a}{b}{b,d}{c}], [{a}{b}{b,d,c}], [{a}{b}{c}{b,d}]}  U {[{a}{b,d}{b}{c}], [{a}{b,d}{b,c}], [{a}{b,d}{c}{b}]} )
SMOTS> 