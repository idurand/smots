(in-package :visu-smots)

(defvar *with-processes* nil)

(defgeneric display-processname (name)
  (:documentation "print process name in appropriate location"))

(defmethod display-processname ((name string))
  (format *error-output* "~A" name))

(defgeneric process-yield ())

(defmethod process-yield ()
  (clim-sys::process-yield))

(defmacro in-process (call name)
  `(if *with-processes*
    (progn
      (display-processname ,name)
      (setf *thread*
	    (clim-sys::make-process
	     (lambda ()
	       (setf *standard-output* *output-stream*)
	       ,call
	     :name ,name)))
    ,call))

(defun toggle-with-processes ()
  (setf *with-processes* (not *with-processes*)))

(defmacro launch-process (name call)
  `(if *with-processes*
    (in-process  (avec-temps ,call) ,name)
    (progn
      (avec-temps ,call)
      (display-current-spec))))


