(in-package :visu-smots)
(defvar *draw-smot-frame* nil)

(defun init-draw-smot ()
   (setf *draw-smot-frame* 
	 (make-application-frame 'draw-smot)))

(defun launch-draw-smot ()
  (run-frame-top-level *draw-smot-frame*))

(defun sword2list (sword)
  (mapcar (lambda (sletter) 
	    (mapcar 
	     (lambda (mletter) (letter mletter)) 
	     (letters sletter)))
	  (letters sword)))

(define-application-frame draw-smot () 
  ((margin-x :initform 20
	     :accessor margin-x)
   (margin-y :initform 20
            :accessor margin-y))
  (:panes
   (canvas :application
	   :display-function 'draw-sword
	   :width 800
	   :height 600
	   )
   (adjust :push-button
           :label "Adjust graph to window"
	   :width 100
	   :activate-callback 
	   (lambda (x) (declare (ignore x)) (com-adjust)))
   (close-display-window :push-button
			 :label "Close"
			 :width 100
			 :activate-callback 
			 (lambda (x) (declare 
				      (ignore x))
			   (com-close-display-window)))
   )
   
  (:layouts
   (default (vertically (:width 600) 
	      (horizontally () adjust close-display-window)
	      (horizontally (:height 400) canvas)
	      ))))

;;;----------------commands---------------

(define-draw-smot-command (com-adjust :name t) ()
"repaints everything"
  (window-clear *standard-output*)
  (draw-sword
   *application-frame* 
   (find-pane-named *application-frame* 'canvas)))

(define-draw-smot-command (com-close-display-window :name t) ()
"closes the window"
  (frame-exit *application-frame*))

;;;----------------functions--------------

(defgeneric coor-table (sword)
  (:documentation
   "returns a hash-table which contains
    each letter of the alphabet as key
    and its positions in the sletters as value
    (in the form of a list)")
  (:method ((sword sword))
    (loop
      with ht = (make-hash-table :test #'eq)
      for sletter in (sword2list sword)
      for i from 0
      finally (return ht)
      do (loop
	   for letter in sletter
	   do (setf (gethash letter ht) 
		    (cons i (values (gethash letter ht))))))))

(defun draw-sword (frame pane)
  "draws canvas's elements: axis, events..."
  (declare (ignore frame))
  (draw-axis pane)
  (draw-time-division pane)
  (draw-events pane (coor-table (sword *spec*))))

(defun draw-axis (pane)
  "draws the axis"
  (let* ((h (get-height pane))
	 (w (get-width pane))
	 (m-x (margin-x *draw-smot-frame*))
	 (m-y (margin-y *draw-smot-frame*))
	 (orig-y (- h m-y))
	 (lst (make-line-style :thickness 3)))
    ;;horizontal
    (draw-arrow* pane m-x orig-y (- w m-x) orig-y
		 :ink +black+
		 :line-style lst)
    ;;vertical
    (draw-arrow* pane m-x orig-y m-x m-y
		 :ink +black+
		 :line-style lst)))

(defun draw-time-division (pane)
  "draws time division numbers and dashed lines"
  (loop
   with margin = (margin-y *draw-smot-frame*)
   with nm = (size (sword *spec*))
   with w = (get-width pane)
   with h = (get-height pane)
   for i to (1- nm)
   do (draw-text* pane
		  (format nil "~D" i)
		  (calc-hor-pos i nm w)
		  (- h (/ margin 3)))
   do (draw-line* pane
		  (calc-hor-pos i nm w)
		  (- h margin)
		  (calc-hor-pos i nm w)
		  margin
		  :line-style (make-line-style 
			       :dashes t))))		  

(defun draw-events  (pane coor-table)
  "draws all the events"
  (with-hash-table-iterator (next coor-table)
    (loop
     with count = (hash-table-count coor-table)
     with h = (get-height pane)
     for i from 0
     do (multiple-value-bind (found? letter coors) (next)
	  (unless found? (return))
	  (draw-event pane letter coors 
		      (calc-vert-pos i count h))))))

(defun draw-event (pane letter coors y)
  "calls to draw event's letter and ocurrences"
  (draw-event-lettre pane letter y)
  (draw-event-ocurrences pane y coors))

(defun draw-event-lettre (pane lettre y)
  "draws the name of the event on the left of the y axis"
  (draw-text* pane 
	      (format nil "~a" lettre)
	      (/ (margin-x *draw-smot-frame*) 2)
	      y
	      :ink +blue+))

(defun draw-event-ocurrences (pane y x-list)
  "iterates on the calculated coordinates
  for the horizontal positions list
  and draws the lines corresponding to them"
  (loop
   with width = (get-width pane)
   with nm = (size (sword *spec*))
   with x-coors = (calc-coors-list x-list nm  width)
   with x = 0
   for i from 0
   for c in x-coors
   if (evenp i) do (setf x c)
   else 
   do (draw-event-line pane c x y)))

(defun calc-coors-list (x-positions n width)
  "converts the positions list into a coorinates list"
  (make-even-coor-list 
   (calc-hor-coors x-positions n width) width))

(defun make-even-coor-list (coors width)
  "returns a list with a pair number of coordinates
  to be represented"
  ;;Takes the list of coorinates and the panel width
  ;;if it has an odd length, then the event is not
  ;;finishing, so we add a finishing value which
  ;;is the right border of the pane and return it.
  (if (oddp (length coors))
      (cons width coors)
      coors))

(defun calc-hor-coors (x-positions n width)
  "replaces each position by its coordinate in the canvas"
  (mapcar 
   (lambda (x) (calc-hor-pos x n width)) 
   x-positions))

(defun draw-event-line (pane c x y)
  "if it is a non-ending event it draws an 
  arrow. Normally it draws a line."
  (let ((lst (make-line-style :thickness 2)))
    (if (= x (get-width pane))
	(draw-arrow* pane c y x y
		     :ink +red+
		     :line-style lst)
	(draw-line* pane c y x y
		    :ink +red+ 
		    :line-style lst))))

(defun get-width (pane)
  "returns the current width of a pane"
  (bounding-rectangle-width (sheet-region pane)))

(defun get-height (pane)
  "returns the current height of a pane"
  (bounding-rectangle-height (sheet-region pane)))
 
(defun calc-vert-pos (pos n height)
  "returns the vertical coordinate of the event"
  ;;height - (margin + (margin-y / 3))
  ;;- pos * ((height - (margin * 2)) / n)
  (let ((margin (margin-y *draw-smot-frame*)))
    (- height
       (+ margin (/ margin 3))
       (* pos (/ (- height margin) n)))))

(defun calc-hor-pos (pos n width)
  "returns the horizontal position of the start or end"
  ;;of an event
  ;;margin + (pos * (width - margin)) / n
  (let ((margin (margin-x *draw-smot-frame*)))
    (+ margin (/ (* pos (- width  margin 2)) n))))
