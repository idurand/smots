(in-package :visu-smots)

(defun display-slanguage (slanguage &key (prefix "Current "))
  (when slanguage
    (format *slanguage-pane* "~A" prefix)
    (write-slanguage slanguage *slanguage-pane*)))

(defun display-problem (problem &key (prefix "Current "))
  (when problem
    (format *problem-pane* "~A" prefix)
    (write-problem problem *problem-pane*)
    (unless *problem-pane*
      (scroll-extent *problem-pane* 0 0))))

(defun display-sexpr (sexpr &key (prefix "Current "))
  (when sexpr
    (format *sexpr-pane* "~A" prefix)
    (write-sexpr sexpr *sexpr-pane*))
    (unless *sexpr-pane*
      (scroll-extent *sexpr-pane* 0 0)))
  
(defun display-sword (sword &key (prefix "Current "))
  (when sword
    (format *sword-pane* "~A" prefix)
    (write-sword sword *sword-pane*)))

