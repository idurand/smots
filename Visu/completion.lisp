(in-package :visu-smots)

(defun suggest-arg (filename)
  (let ((str (file-namestring filename)))
    (if (member #\. (coerce str 'list))
	(concatenate 'string  (pathname-name filename) "." (pathname-type filename))
	(file-namestring filename))))

(define-presentation-method accept ((type data-file-name) stream
                    (view textual-view)
                    &key)
  (let* ((data-pattern (concatenate 'string *data-directory* (file-namestring pattern)))
	 (pathnames (directory data-pattern)))
    (handler-case
	(let ((filename 
	       (completing-from-suggestions
		(stream)
		(loop
		 for file in pathnames
		 do (suggest (suggest-arg file) file))
		)))
	  filename)
      (simple-parse-error ()
	  (prog1 nil
	    (format *output-stream* "no match~%")))
      (simple-completion-error ()
	  (prog1 nil
	    (format *output-stream* "no match~%")))
      (all-errors ()
	  (prog1 nil
	    (format *output-stream* "error"))))))

(define-presentation-type spec (&optional (pattern "*.*"))
)

(define-presentation-method accept ((type spec) stream
                                     (view textual-view)
                                     &key)
  (handler-case
     (let ((spec 
	    (completing-from-suggestions
	     (stream)
	     (loop
	      for spec in (list-values *specs*)
	      do
	      (suggest (name spec) spec)))))
       spec)
     (simple-completion-error ()
       (prog1 nil
	 (format *output-stream* "no match~%")))
     (all-errors ()
       (prog1 nil
	 (format *output-stream* "error")))))

(define-presentation-method accept ((type slanguage) stream
                                     (view textual-view)
                                     &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for slanguage in (list-values (slanguages *spec*))
           do (suggest (name slanguage) slanguage)))
     (simple-completion-error ()
       (prog1 nil
	 (format *output-stream* "no match~%")))
     (all-errors ()
       (prog1 nil
	 (format *output-stream* "error")))))

(define-presentation-method accept
    ((type sword) stream (view textual-view) &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for sword in (list-values (swords *spec*))
           do (suggest (name sword) sword)))
     (simple-completion-error ()
       (prog1 nil
	 (format *output-stream* "no match~%")))
     (all-errors ()
       (prog1 nil
	 (format *output-stream* "error")))))

(define-presentation-method accept
    ((type s-mixin) stream (view textual-view) &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for sexpr in (append
			 (list-values (swords *spec*))
			 (list-values (sexprs *spec*))
				)
           do (suggest (name sexpr) sexpr)))
     (simple-completion-error ()
       (prog1 nil
	 (format *output-stream* "no match~%")))
     (all-errors ()
       (prog1 nil
	 (format *output-stream* "error")))))

