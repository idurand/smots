;; -*- Mode: Lisp; Package: VISU-SMOTS -*-

;;;  (c) copyright 2001 by
;;;           Irène Durand (idurand@labri.fr)

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.

					;(setf interface:*interface-style* :tty)
#+cmu
(setf ext:*gc-verbose* nil)

(in-package :visu-smots)
(defconstant +largeur-texte+ (1+ (length "----------")))
;;(defvar *progress-pane* nil)
;;(defvar *processname-pane*)
(defvar *data-directory-field*)

(defun normalize-string (s n)
  (let ((l (length s)))
    (if (>= l n)
        s
        (concatenate 'string " " s (make-string (- n l) :initial-element #\space)))))

(let*
    ((counter 0)
     (chars (make-array 4 :initial-contents '("\/" "\-" "\\" "\|")))
     (tog 0)
     (len (length chars)))
  (defun next-char ()
    (setf counter (mod (1+ counter) 100000))
    (if (zerop (mod counter 1000))
        (progn
          (setf tog (mod (1+ tog) len))
          (values t (aref chars tog)))
	(values nil (aref chars tog)))))

(defun interface ()
  (run-test 'visu-smots)
  0)

(defun start ()
  (init)
  (init-draw-smot)
  (interface))

(defun start-darwin ()
  (init)
  (interface)
  (cl-user::quit))

(defun run-test (name)
  (loop for port in climi::*all-ports*
     do (destroy-port port))
  (setq climi::*all-ports* nil)
  (let ((frame (make-application-frame name)))
    (run-frame-top-level frame)))

(defvar *cpt* 0)

;; (defmethod display-step :around ()
;;   (when (and *progress-pane* (gadget-value *progress-pane*))
;;     (multiple-value-bind (res char)
;; 	(next-char)
;;       (when res
;; 	(setf (gadget-value *progress-pane*)
;; 	      (normalize-string char +largeur-texte+))))))

(defun pause ()
  (incf *cpt*)
  (dotimes (i 4000000000)
    (1+ 1))
  (format *output-stream* "f ~A done~%" *cpt*))

(make-command-table
 'file-command-table
 :errorp nil
 :menu '(
	 ("Change Data Directory" :command com-change-data-directory)
	 ("Default Data Directory" :command com-default-data-directory)
	 ("Show Data Directory" :command com-show-data-directory)
	 ("Clear" :command com-clear)
	 ("Quit" :command com-quit)))

(make-command-table
 'sword-command-table
 :errorp nil
 :menu
 '(
   ("Move sword to sexpr" :command com-move-sword-to-sexpr)
   ("Random sword" :command com-random-sword)
   ("Random inf sword" :command com-random-inf-swords)
   ("Mirror sword" :command com-mirror-sword)
   ("Is mirror sword" :command com-is-mirror-sword)
   ("Join sword sexpr" :command com-join-sword-sexpr)
   ("Alphabet sword" :command com-alphabet-sword)
   ("Malphabet sword" :command com-malphabet-sword)
   ("Size sword" :command com-size-sword)
   ("Parikh vector" :command com-parikh-vector)
   ("Membership to slanguage" :command com-membership-to-slanguage)
   ("Retrieve sword" :command com-retrieve-sword)
   ("Read sword" :command com-read-sword)
   ("Load sword" :command com-load-sword)
   ("Save sword" :command com-save-sword)
   ("Clear sword" :command com-clear-sword)
   ("Draw sword" :command com-draw-sword)
   ))

(make-command-table
 'spec-command-table
 :errorp nil
 :menu
 '(
   ("Alphabet spec" :command com-alphabet-spec)
   ("Malphabet spec" :command com-malphabet-spec)
   ("Retrieve spec" :command com-retrieve-spec)
   ("Load spec (.txt)" :command com-load-spec)
   ("Save spec" :command com-save-spec)
   ("Clear spec" :command com-clear-spec)
   ))

(make-command-table
 'sexpr-command-table
 :errorp nil
 :menu '(
	 ("Move sexpr to sword" :command com-move-sexpr-to-sword)
	 ("Elementary sexpr?" :command com-elementary-sexpr)
	 ("Directed sexpr?" :command com-directed-sexpr)
	 ("Slanguage sexpr" :command com-slanguage-sexpr)
	 ("Prefix slanguage sexpr" :command com-prefix-slanguage-sexpr)
	 ("Cardinality sexpr" :command com-cardinality-sexpr)
	 ("Factorize sexpr" :command com-factorize-sexpr)
	 ("Retrieve sexpr" :command com-retrieve-sexpr)
	 ("Read sexpr" :command com-read-sexpr)
	 ("Load sexpr" :command com-load-sexpr)
	 ("Save sexpr" :command com-save-sexpr)
         ("Clear sexpr" :command com-clear-sexpr)
	 ))

(make-command-table
 'slanguage-command-table
 :errorp nil
 :menu '(
	 ;;	 ("Size slanguage" :command com-size-slanguage)
	 ("Cardinality slanguage" :command com-cardinality-slanguage)
	 ("Alphabet slanguage" :command com-alphabet-slanguage)
	 ("Malphabet slanguage" :command com-malphabet-slanguage)
	 ("Move slanguage to sexpr" :command com-move-slanguage-to-sexpr)
	 ("Rename slanguage" :command com-rename-slanguage)
	 ("Retrieve slanguage" :command com-retrieve-slanguage)
	 ("Read slanguage" :command com-read-slanguage)
	 ("Is mirror slanguage" :command com-is-mirror-slanguage)
	 ("Mirror slanguage" :command com-mirror-slanguage)
	 ("Mirror closure" :command com-mirror-closure)
	 ("Load slanguage" :command com-load-slanguage)
	 ("Save slanguage" :command com-save-slanguage)
	 ("Clear slanguage" :command com-clear-slanguage)
	 ))

(make-command-table
 'slanguages-command-table
 :errorp nil
 :menu '(
	 ("Equality slanguage" :command com-equality-slanguage)
	 ("Union slanguage" :command com-union-slanguage)
	 ("Intersection slanguage" :command com-intersection-slanguage)
	 ("Join slanguage" :command com-join-slanguage)
	 ))

(make-command-table
 'problem-command-table
 :errorp nil
 :menu '(
	 ("Alphabet problem" :command com-alphabet-problem)
	 ("Malphabet problem" :command com-malphabet-problem)
	 ("Add sexpr" :command com-add-sexpr)
	 ("Solve" :command com-solve)
	 ("Resolve" :command com-resolve)
	 ("New problem" :command com-new-problem)
	 ("Clear constraints" :command com-clear-constraints)
	 ("Clear problem" :command com-clear-problem)
	 ))

(make-command-table
 'menubar-command-table
 :errorp nil
 :menu
 '(
   ("File " :menu file-command-table)
   ("Spec " :menu spec-command-table)
   ("Sword " :menu sword-command-table)
   ("Problem " :menu problem-command-table)
   ("Sexpr " :menu sexpr-command-table)
   ("Slanguage " :menu slanguage-command-table)
   ("Slanguages " :menu slanguages-command-table)
   ))

;; (defmethod display-processname :around ((name string))
;;   (if *processname-pane*
;;       (setf (gadget-value *processname-pane*)
;; 	    (normalize-string name +largeur-texte+))
;;       (format *error-output* "~A" name)))

(defun get-new-directory (stream previous)
  (let ((pathname (accept 'pathname :stream stream
				    :default previous
				    :default-type 'pathname
				    :insert-default t)))
    (if pathname
	(let ((dir (probe-file pathname)))
	  (if dir
	      (namestring dir)
	      (format *output-stream* "~A: no such directory~%" pathname)))
	(format *output-stream* "Data directory is ~A~%" *data-directory*))
    (format *output-stream* "~A incorrect pathname~%" pathname)))

(defclass data-directory-field (text-field) ())
   
(defmethod change-data-directory ((ddp data-directory-field))
  (let ((dirname (get-new-directory ddp *data-directory*)))
    (when dirname
      (setf *data-directory* dirname))))

;;(defclass data-directory-field (application-pane) ())

(defun display-ddp (frame ddp)
  (declare (ignore frame))
  (format ddp "~A" *data-directory*))

(define-application-frame visu-smots ()
  ()
  (:menu-bar menubar-command-table)
  (:panes
   (problem-pane
    (make-pane 'clim-stream-pane
	       :name 'problem-pane))
   (sexpr-pane
    (make-pane 'clim-stream-pane
	       :name 'sexpr-pane))
   (slanguage-pane
    (make-pane 'clim-stream-pane
	       :name 'slanguage-pane))
   (sword-pane
    (make-pane 'clim-stream-pane
	       :name 'sword-pane))
   (result-pane
    (make-pane 'clim-stream-pane :name 'result-pane))
   (interactor-pane :interactor)
   (data-directory-field
    :text-field
    :editable-p nil
    :value *data-directory* :height 4)
   (quit
    :push-button
    :label "Quit"
    :activate-callback (lambda (x)
			 (declare (ignore x))
			 (com-quit)))
   (clear
    :push-button
    :label "clear"
    :activate-callback (lambda (x)
			 (declare (ignore x))
			 (com-clear)))
   (mark-button
    :toggle-button
    :label "Marks"
    :indicator-type :one-of
    :value *show-mark*
    :value-changed-callback
    (lambda (gadget value)
      (declare (ignore gadget value))
      (com-toggle-mark)))
   (egraph-button
    :toggle-button
    :label "Egraphs"
    :indicator-type :one-of
    :value *egraph-mode*
    :value-changed-callback
    (lambda (gadget value)
      (declare (ignore gadget value))
      (com-toggle-egraph-mode))))
  (:layouts
   (default
       (vertically (:width 800)
;	 menu-bar
	 (vertically ()
	   (horizontally (:height 100) interactor-pane)
	   (scrolling (:height 150) problem-pane)
	   (scrolling (:height 150) sexpr-pane)
	   (horizontally (:height 100)
	     (scrolling () sword-pane)
	     (scrolling () slanguage-pane))
	   (scrolling (:height 100) result-pane)
	   )
	 (horizontally ()
	   quit clear data-directory-field egraph-button mark-button)))))

(define-condition all-errors (error) ())

(defmethod default-frame-top-level :before ((frame application-frame)
					    &key (command-parser 'command-line-command-parser)
					    (command-unparser 'command-line-command-unparser)
					    (partial-command-parser 'command-line-read-remaining-arguments-for-partial-command)
					    (prompt "command: "))
  (declare (ignore command-parser command-unparser partial-command-parser prompt))
  (unless (eq *application-frame* *draw-smot-frame*)
    (setf
     ;;   *progress-pane* (climi::find-pane-named *application-frame* 'progress-pane)
     *data-directory-field* (climi::find-pane-named *application-frame* 'data-directory-field)
     *sword-pane* (climi::find-pane-named *application-frame* 'sword-pane)
     *slanguage-pane* (climi::find-pane-named *application-frame* 'slanguage-pane)
     *problem-pane* (climi::find-pane-named *application-frame* 'problem-pane)
     *sexpr-pane* (climi::find-pane-named *application-frame* 'sexpr-pane)
     *output-stream* (climi::find-pane-named *application-frame* 'result-pane)
     )
    (setf *display-pane* (climi::find-pane-named *application-frame* 'display-pane))
    (setf *standard-output* *output-stream*)
    ))

(defmethod execute-frame-command :around ((frame application-frame) command)
  (call-next-method))

(defun check-problem ()
  (or (problem *spec*)
      (format *output-stream* "No current PROBLEM~%")))

(defun check-slanguage ()
  (or (slanguage *spec*)
      (format *output-stream* "No current Slanguage~%")))

(defun check-sword ()
  (or (sword *spec*)
      (format *output-stream* "No current Sword~%")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Commands
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-visu-smots-command (com-toggle-mark :name t) ()
  (toggle-show-mark)
  (display-current-spec))

(define-visu-smots-command (com-toggle-egraph-mode :name t) ()
  (toggle-egraph-mode)
  (display-current-spec))

;; (define-visu-smots-command (com-toggle-processes :name t) ()
;;   (toggle-with-processes))

(define-visu-smots-command (com-show-data-directory :name t) ()
  (format *output-stream* "Data directory is ~A~%" *data-directory*))

(define-visu-smots-command (com-change-data-directory :name t) ()
  (let ((pathname (accept 'pathname :prompt "data directory?"
				    :default *data-directory*
				    :default-type 'pathname
				    :insert-default t)))
    (if pathname
	(let ((dir (probe-file pathname)))
	  (if dir 
	      (setf *data-directory* (namestring (probe-file pathname)))
	      (format *output-stream* "no such directory~%")))
	(format *output-stream* "Data directory is ~A~%" *data-directory*))
    ))

(define-visu-smots-command (com-default-data-directory :name t) ()
  (setf *data-directory* (initial-data-directory))
  (format *output-stream* "Data directory is ~A~%" *data-directory*))

(define-visu-smots-command (com-alphabet-sword :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (format *output-stream* "Alphabet of current sword is ~A~%" (alphabet-of sword)))))

(define-visu-smots-command (com-malphabet-sword :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (format *output-stream* "Malphabet of current sword is ~A~%" (malphabet-of sword)))))

(define-visu-smots-command (com-malphabet-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (format *output-stream* "Malphabet of current sexpr is ~A~%" (malphabet-of sexpr)))))

(define-visu-smots-command (com-alphabet-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (format *output-stream* "Alphabet of current sexpr is ~A~%" (alphabet-of sexpr)))))

(define-visu-smots-command (com-elementary-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (format
       *output-stream*
       "~:[We cannot say yet whether the current sexpr is~;Current sexpr is~] elementary~%"
       (elementaryp sexpr)))))

(define-visu-smots-command (com-directed-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (multiple-value-bind (directed known) (directedp sexpr)
	(cond
	  (directed
	   (format *output-stream* "~A~%" (egraph-from-eexpr sexpr)))
	  (known 
	   (format *output-stream* "Current sexpr in not directed~%"))
	  (t
	   (format
	    *output-stream*
	    "We cannot say yet whether the current sexpr is directed~%")))))))

(define-visu-smots-command (com-malphabet-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (format *output-stream* "Malphabet of current slanguage is ~A~%" (malphabet-of slanguage)))))

(define-visu-smots-command (com-malphabet-problem :name t) ()
  (let ((problem (problem *spec*)))
    (when problem
      (format *output-stream*
	      "Malphabet of current problem is ~A~%" (malphabet-of problem)))))

(define-visu-smots-command (com-alphabet-problem :name t) ()
  (let ((problem (problem *spec*)))
    (when problem
      (format *output-stream*
	      "Alphabet of current problem is ~A~%" (alphabet-of problem)))))

(define-visu-smots-command (com-malphabet-spec :name t) ()
  (when *spec*
    (format *output-stream* "Malphabet of current spec is ~A~%" (malphabet-of *spec*))))

(define-visu-smots-command (com-alphabet-spec :name t) ()
  (when *spec*
    (format *output-stream* "Alphabet of current spec is ~A~%" (alphabet-of *spec*))))

(define-visu-smots-command (com-alphabet-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (format *output-stream* "Alphabet of current slanguage is ~A~%" (alphabet-of slanguage)))))

(define-visu-smots-command (com-size-sword :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (format *output-stream* "size of current sword is ~A~%" (size sword)))))

(define-visu-smots-command (com-parikh-vector :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (format *output-stream* "The Parikh vector of current sword is ~A~%" (parikh-vector sword)))))

(define-visu-smots-command (com-membership-to-slanguage :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (let ((slanguage (slanguage *spec*)))
	(when slanguage
	  (format
	   *output-stream*
	   "Current sword is~:[ not~;~] in current slanguage~%" (member sword (words slanguage))))))))
  
(define-visu-smots-command (com-load-spec :name t) ()
  (let ((name (accept '(data-file-name "*.txt")
		      :prompt "spec filename"
		      :default nil
		      :display-default nil
		      )))
    (when name
      (let ((spec (read-spec-from-path name)))
	(when spec
	  ;; 	  (com-clear-spec)
 	  (set-and-display-current-spec spec)
 	  )))))

(define-visu-smots-command (com-save-spec :name t) ()
  (let ((*limit-constraints* nil)
	(name (accept 'string :prompt "spec filename"
			      :default "save")))
    (save-spec (concatenate 'string name ".txt"))))

(define-visu-smots-command (com-save-sexpr :name t) ()
  (let ((name (accept 'string :prompt "sexpr filename"
			      :default "sexpr-save")))
    (save-sexpr (concatenate 'string name ".txt"))))

(define-visu-smots-command (com-save-slanguage :name t) ()
  (let ((name (accept 'string :prompt "slanguage filename"
			      :default "slanguage-save")))
    (save-slanguage (concatenate 'string name ".txt"))))

(define-visu-smots-command (com-save-sword :name t) ()
  (let ((name (accept 'string :prompt "sword filename"
			      :default "sword-save")))
    (save-sword (concatenate 'string name ".txt"))))

(define-visu-smots-command (com-save-spec-readable :name t) ()
  (let ((*limit-constraints* nil)
	(name (accept 'string :prompt "spec filename"
			      :default "save")))
    (save-spec (concatenate 'string name ".txt"))))

(define-visu-smots-command (com-retrieve-spec :name t) ()
  (let ((spec 
	 (accept 'spec
		 :prompt "spec name")))
    (when spec
      (set-and-display-current-spec spec))))

(define-visu-smots-command (com-load-problem :name t) ()
  (let ((name (accept '(data-file-name)
		      :prompt "problem filename"
		      :default nil
		      :display-default nil
		      )))
    (when name
      (let ((constraints (load-file-from-absolute-filename name #'read-constraints)))
	(if constraints
	    (set-and-display-current-problem (make-problem name constraints))
	    (format *output-stream* "no constraints~%"))))))

(define-visu-smots-command (com-read-problem :name t) ()
  (let ((name (accept 'string :prompt "problem name")))
    (when name
      (let* ((str (concatenate 'string (accept 'string :prompt "constraints") " "))
	     (constraints (with-input-from-string (foo str)
			    (read-constraints foo))))
    	(if constraints
	    (set-and-display-current-problem (make-problem name constraints))
	    (format *output-stream* "no constraints~%"))))))

(define-visu-smots-command (com-retrieve-problem :name t) ()
  (let ((problem 
	 (accept 'problem
		 :prompt "problem name"
		 )))
    (when problem
      (set-and-display-current-problem problem))))

(define-visu-smots-command (com-solve :name t) ()
  (let ((problem (problem *spec*)))
    (when problem
      (solve problem)
      (rename-object (result problem) (name problem))
      (clear-slanguage-pane)
      (set-and-display-current-sexpr (result (problem *spec*))))))

(define-visu-smots-command (com-clear-constraints :name t) ()
  (let ((problem (problem *spec*)))
    (when problem
      (setf (constraints problem) nil)
      (set-and-display-current-problem problem))))


(define-visu-smots-command (com-resolve :name t) ()
  (let ((problem (problem *spec*)))
    (when problem
      (setf (result problem) nil)
      (com-solve))))

;;      (launch-process "Solve" (process-solve problem)))))

(define-visu-smots-command (com-quit :name t) ()
  (frame-exit *application-frame*))

(define-visu-smots-command (com-load-slanguage :name t) ()
  (let ((name (accept 'string
		      :prompt "slanguage filename"
		      :default nil
		      :display-default nil
		      )))
    (when name
      (let ((swords (load-file-from-absolute-filename (absolute-filename name) #'read-swords)))
	(when swords
	  (load-slanguage name (make-slanguage swords)))))))

(define-visu-smots-command (com-read-slanguage :name t) ()
  (let ((name (accept 'string :prompt "name")))
    (when name
      (let ((str (accept 'string :prompt "slanguage")))
	(when str
	  (let ((slanguage (with-input-from-string (foo str)
			     (read-slanguage foo))))
	    (when slanguage
	      (load-slanguage name slanguage))))))))

(define-visu-smots-command (com-retrieve-slanguage :name t) ()
  (let ((slanguage 
	 (accept 'slanguage
		 :prompt "slanguage name"
		 )))
    (when slanguage
      (set-and-display-current-slanguage slanguage))))

(define-visu-smots-command (com-clear-slanguage :name t) ()
  (clear-current-slanguage))

(define-visu-smots-command (com-clear-sexpr :name t) ()
  (clear-current-sexpr))

(define-visu-smots-command (com-clear-sword :name t) ()
  (clear-current-sword)
  (clear-display))

(define-visu-smots-command (com-clear-problem :name t) ()
  (clear-current-problem))

(define-visu-smots-command (com-new-problem :name t) ()
  (set-and-display-current-problem (make-problem "New" '())))

(define-visu-smots-command (com-retrieve-sword :name t) ()
  (let ((sword (accept 'sword :prompt "sword")))
    (when sword
      (set-and-display-current-sword sword))))

(define-visu-smots-command (com-move-sword-to-sexpr :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (clear-sword-pane)
      (set-and-display-current-sexpr sword))))

(define-visu-smots-command (com-move-sexpr-to-sword :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when (and sexpr (swordp sexpr))
      (clear-sexpr-pane)
      (set-and-display-current-sword sexpr))))

(define-visu-smots-command (com-retrieve-sexpr :name t) ()
  (let ((sexpr (accept 's-mixin :prompt "sexpr")))
    (when sexpr
      (set-and-display-current-sexpr sexpr))))

(define-visu-smots-command (com-random-sword :name t) ()
  (let ((sword (random-sword 10 (alphabet-of *spec*))))
    (when sword
      (set-and-display-current-sword sword))))

(define-visu-smots-command (com-random-inf-swords :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (let ((newsword (random-inf-sword sword))
	    (sexpr (random-inf-sword sword)))
	(loop
	   do (setf sexpr (random-inf-sword sword))
	   until (joinablep newsword sexpr))
	(when newsword
	  (set-and-display-current-sword newsword))
	(when sexpr
	  (set-and-display-current-sexpr sexpr))))))

(define-visu-smots-command (com-cardinality-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (format *output-stream* "the cardinality of the slanguage associated with current sexpr is ~A~%" (ccard (sexpr *spec*))))))

(define-visu-smots-command (com-slanguage-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (let ((slanguage (compute-language sexpr)))
	(set-and-display-current-slanguage slanguage)))))
;;      (launch-process "Slanguage-sexpr" (process-slanguage-sexpr sexpr)))))

(define-visu-smots-command (com-prefix-slanguage-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (let ((n (accept 'integer :prompt "swords length")))
	(when n
	  (let ((slanguage (compute-prefix-language sexpr n)))
	    (set-and-display-current-slanguage slanguage)))))))

(define-visu-smots-command (com-factorize-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*)))
    (when sexpr
      (let ((newsexpr (factorize sexpr)))
	(set-and-display-current-sexpr newsexpr)))))

(define-visu-smots-command (com-load-sword :name t) ()
  (let ((name (accept '(data-file-name "*.txt")
		      :prompt "sword filename"
		      :default nil
		      :display-default nil)))
    (when name
      (let ((sword (load-file-from-absolute-filename name #'read-sword)))
	(when sword
	  (load-sword sword))))))

(define-visu-smots-command (com-load-sexpr :name t) ()
  (let ((name (accept '(data-file-name "*.txt")
		      :prompt "sexpr filename"
		      :default nil
		      :display-default nil)))
    (when name
      (let ((sexpr (load-file-from-absolute-filename name #'read-sexpr)))
	(when sexpr
	  (load-sexpr sexpr))))))

(define-visu-smots-command (com-join-sword-sexpr :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (let ((sexpr (sexpr *spec*)))
	(when sexpr
	  (let ((join (ljoin sword sexpr)))
	    (when join
	      (format *output-stream* "~A~%" join))))))))

(define-visu-smots-command (com-mirror-sword :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (load-sword (mirror sword)))))

(define-visu-smots-command (com-is-mirror-sword :name t) ()
  (let ((sword (sword *spec*)))
    (when sword
      (format *output-stream* "~A is ~:[not ~;~]a mirror~%"
	      sword
	      (mirror-p sword)))))

(define-visu-smots-command (com-read-sword :name t) ()
  (let ((sword-string (accept 'string
			      :prompt "sword")))
    (when sword-string
      (load-sword
       (with-input-from-string (foo sword-string)
	 (read-sword foo))))))

(define-visu-smots-command (com-read-sexpr :name t) ()
  (let ((sexpr-string (accept 'string
			      :prompt "sexpr")))
    (when sexpr-string
      (load-sexpr
       (with-input-from-string (foo sexpr-string)
	 (read-sexpr foo))))))

(define-visu-smots-command (com-add-sexpr :name t) ()
  (let ((sexpr (sexpr *spec*))
	(problem (problem *spec*)))
    (when (and sexpr problem)
      (add-constraint sexpr (problem *spec*))
      (display-current-problem))))

(define-visu-smots-command (com-rename-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (let ((name (accept 'string
			  :prompt "name"
			  )))
	(setf (name slanguage) name)
	(set-and-display-current-slanguage slanguage)))))
     
(define-visu-smots-command (com-size-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (format *output-stream*
	      "The size of Slanguage ~A is ~A~%" (name slanguage) (size slanguage)))))

(define-visu-smots-command (com-cardinality-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (format *output-stream*
	      "The cardinality of Slanguage ~A is ~A~%" (name slanguage) (cardinality slanguage)))))

(define-visu-smots-command (com-equality-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (let ((slanguage2 (accept 'slanguage
				:prompt "equality with slanguage")))
	(when slanguage2
	  (let ((res (equivalent slanguage slanguage2)))
	    (format *output-stream* "~A is ~:[not~;~] equal to ~A ~%"
		    (name slanguage)
                    res
		    (name slanguage2))))))))

(define-visu-smots-command (com-union-slanguage :name t) ()
  (let ((slanguage1 (slanguage *spec*)))
    (when slanguage1
      (let ((slanguage2 (accept 'slanguage
				:prompt "union with slanguage")))
	(when slanguage2
	  (let ((slanguage (lunion slanguage1 slanguage2)))
	    (set-and-display-current-slanguage slanguage)))))))

(define-visu-smots-command (com-intersection-slanguage :name t) ()
  (let ((slanguage1 (slanguage *spec*)))
    (when slanguage1
      (let ((slanguage2 (accept 'slanguage
				:prompt "intersection with slanguage")))
	(when slanguage2
	  (let ((slanguage (lintersection slanguage1 slanguage2)))
	    (set-and-display-current-slanguage slanguage)))))))

(define-visu-smots-command (com-join-slanguage :name t) ()
  (let ((slanguage1 (slanguage *spec*)))
    (when slanguage1
      (let ((slanguage2 (accept 'slanguage
				:prompt "join with slanguage")))
	(when slanguage2
	  (let ((slanguage (ljoin slanguage1 slanguage2)))
	    (set-and-display-current-slanguage slanguage)))))))

(define-visu-smots-command (com-is-mirror-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (format *output-stream* "~A is ~:[not ~;~]a mirror~%"
	      (name slanguage)
	      (mirror-p slanguage)))))

(define-visu-smots-command (com-mirror-slanguage :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (set-and-display-current-slanguage (mirror slanguage)))))

(define-visu-smots-command (com-mirror-closure :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (set-and-display-current-slanguage (mirror-closure slanguage)))))

(define-visu-smots-command (com-move-slanguage-to-sexpr :name t) ()
  (let ((slanguage (slanguage *spec*)))
    (when slanguage
      (setf (sexpr *spec*) slanguage)
      (display-current-sexpr))))

(define-visu-smots-command (com-clear :name t) ()
  (window-clear *output-stream*))

(define-visu-smots-command (com-clear-spec :name t) ()
  (set-and-display-current-spec (get-spec "empty")))

(define-visu-smots-command (com-draw-sword :name t) ()
  (launch-draw-smot))

;;;;; End of Commands
