;;; -*- Mode: Lisp -*-
;;;  (c) copyright 2006 by
;;;           Ir�ne Durand (idurandlabri.fr)

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.

;;; ASDF system definition for VISU-SMOTS.

(defpackage :visu-smots-system
  (:use :cl :asdf))

(in-package :visu-smots-system)

(defsystem :visu-smots
  :description "Graphical Inferface for Smots"
  :version "1.0"
  :author "Irene Durand <idurand@labri.fr>"
  :licence "Public Domain"
  :depends-on (:mcclim :smots)
  :serial t
  :components ((:file "package")
	       (:file "time")
;;	       (:file "process")
	       (:file "interface-variables")
	       (:file "smots-display")
	       (:file "presentations")
	       (:file "completion")
	       (:file "utils-interface")
	       (:file "toy")
;;	       (:file "processes")
	       (:file "interface")
))

