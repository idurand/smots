(in-package :visu-smots)

(define-presentation-type data-file-name (&optional (pattern "*")))

(define-presentation-method present (object (type language) stream view &key)
  (declare (ignore view))
  (write-string (name object) stream))

