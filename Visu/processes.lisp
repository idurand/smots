(in-package :visu-smots)

(defun process-slanguage-sexpr (sexpr)
  (setf (language *spec*) (compute-language sexpr)))

(defun process-solve (problem)
  (solve problem)
  (rename-object (result problem) (name problem))
  (setf (sexpr *spec*) (result (problem *spec*))))

;; (defun process-slanguage-sexpr (sexpr)
;;   (let ((language (compute-language sexpr)))
;;     (set-and-display-current-slanguage slanguage)))

;; (defun process-solve (problem)
;;   (clear-slanguage-pane)
;;   (solve problem)
;;   (rename-object (result problem) (name problem))
;;   (set-and-display-current-sexpr (result problem)))

;; (define-condition end-of-process-slanguage-sexpr ()
;;   ((%language :initarg :language :reader slanguage)))

;; (define-condition end-of-process-solve () ())

;; (defun process-slanguage-sexpr (sexpr)
;;   (let ((language (compute-language sexpr)))
;;     (signal 'end-of-process-language-sexpr :language slanguage)))

;; (defun process-solve (problem)
;;   (clear-slanguage-pane)
;;   (solve problem)
;;   (signal 'end-of-process-solve))
