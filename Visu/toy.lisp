;; -*- Mode: Lisp; Package: VISU-SMOTS -*-

;;;  (c) copyright 2007 by
;;;           Irène Durand (idurand@labri.fr)

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.


;; faire que les ) trailing (ou manquantes) ne bloquent pas l'analyse
;; tuer les processus avant de tout quitter

(in-package :visu-smots)

					;(defun get-absolute-filename-from-browser ()
					;  (car (clim-demo::file-selector-test)))

(defmethod process-yield ())

(defun clear-slanguage-pane ()
  (window-clear *slanguage-pane*))

(defun clear-sword-pane ()
  (window-clear *sword-pane*))

(defun clear-sexpr-pane ()
  (window-clear *sexpr-pane*))

(defun clear-problem-pane ()
  (window-clear *problem-pane*))

(defun scroll-slanguage-pane ()
  (scroll-extent *slanguage-pane* 0 0))

(defun scroll-sword-pane ()
  (scroll-extent *sword-pane* 0 0))

(defun scroll-sexpr-pane ()
  (scroll-extent *sexpr-pane* 0 0))

(defun scroll-problem-pane ()
  (scroll-extent *problem-pane* 0 0))

(defun clear-display ()
  (window-clear *display-pane*))

(defun display-current-problem ()
  (clear-problem-pane)
  (when (problem *spec*)
    (display-problem (problem *spec*))
    (scroll-problem-pane)))

(defun display-current-sword ()
  (clear-sword-pane)
  (when (sword *spec*)
    (display-sword (sword *spec*))
    (scroll-sword-pane)))

(defun display-current-slanguage ()
  (clear-slanguage-pane)
  (when (slanguage *spec*)
    (display-slanguage (slanguage *spec*))
    (scroll-slanguage-pane)))

(defun display-current-sexpr ()
  (clear-sexpr-pane)
  (when (sexpr *spec*)
    (display-sexpr (sexpr *spec*))
    (scroll-sexpr-pane)))

(defun display-current-spec ()
  (when *spec*
    (display-current-problem)
    (display-current-slanguage)
    (display-current-sword)
    (display-current-sexpr)
    (window-clear *output-stream*)))

(defun set-and-display-current-sexpr (sexpr)
  (set-current-sexpr sexpr)
  (display-current-sexpr))

(defun set-and-display-current-sword (sword)
  (set-current-sword sword)
  (display-current-sword))

(defun set-and-display-current-slanguage (slanguage)
  (set-current-slanguage slanguage)
  (display-current-slanguage))

(defun load-sword (new-sword)
  (when new-sword
    (set-and-display-current-sword new-sword)))

(defun load-sexpr (new-sexpr)
  (when new-sexpr
    (set-and-display-current-sexpr new-sexpr)))

(defun clear-current-sword ()
  (setf (sword *spec*) nil)
  (clear-sword-pane))

(defun clear-current-sexpr ()
  (setf (sexpr *spec*) nil)
  (clear-sexpr-pane))

(defun clear-current-slanguage ()
  (setf (slanguage *spec*) nil)
  (clear-slanguage-pane))

(defun load-slanguage (name new-slanguage)
  (when new-slanguage
    (setf (name new-slanguage) name)
    (set-and-display-current-slanguage new-slanguage)))

(defun clear-current-problem ()
  (setf (problem *spec*) nil)
  (clear-problem-pane))

(defun set-and-display-current-problem (problem)
  (set-current-problem problem)
  (display-current-problem))

(defun set-and-display-current-spec (spec)
  (set-current-spec spec)
  (display-current-spec))
