(in-package :common-lisp-user)

(defpackage :smots-display
  (:use :smots :clim :clim-extensions :clim-lisp))

(in-package :smots-display)

(defvar *sword-ex* '())
(setf *sword-ex* '( (a) (b c) (a) (e) (a) (a) (a) (a) (b)))

(defun main ()
  (init)
  (run-frame-top-level 
   (make-application-frame 'smots-display)))

(define-application-frame smots-display () 
  ((coord-table :initform (create-coord-table *sword-ex*)
		:accessor coord-table)
   (nmoments :initform (length *sword-ex*)
	     :accessor nmoments)
   (margin-x :initform 30
	     :accessor margin-x)
   (margin-y :initform 30
	     :accessor margin-y))
  (:panes
   (canvas :application
	   :display-function 'draw-canvas
	   :height 300
	   :width 800
	   )
   ;;(com-window :interactor
   ;;:height 100
   ;;:width *panelwidth*)
   (adjust :push-button
           :label "Adjust graph to window"
	   :width 100
	   :activate-callback 
	   (lambda (x) (declare (ignore x)) (com-adjust)))
   )
  (:layouts
   (default (vertically () 
	      (horizontally () adjust)
	      (horizontally () canvas)))))


;;;----------------commands---------------

(define-smots-display-command (com-adjust :name t) ()
  "repaints everything"
  (window-clear *standard-output*)
  (draw-canvas 
   *application-frame* 
   (find-pane-named *application-frame* 'canvas)))

;;;----------------functions--------------

(defun create-coord-table (sword)
  "returns a hash-table which contains
  each letter of the alphabet as key
  and its positions in the sword as value
  (in the form of a list)"
  (loop
   with ht = (make-hash-table :test #'eq)
   for sletter in sword
   for i from 0
   finally (return ht)
   do (loop
       for letter in sletter
       do (setf (gethash letter ht) 
		(cons i (values (gethash letter ht)))))))

(defun draw-canvas (frame pane)
  "draws canvas's elements: axis, events..."
  (let ((ct (coord-table frame)))
    (draw-axis pane)
    (draw-time-division pane)
    (draw-events pane ct)))

(defun draw-axis (pane)
  "draws the axis"
  (let* ((h (get-height pane))
	 (w (get-width pane))
	 (m-x (margin-x *application-frame*))
	 (m-y (margin-y *application-frame*))
	 (orig-y (- h m-y))
	 (lst (make-line-style :thickness 3)))
    ;;horizontal
    (draw-arrow* pane m-x orig-y (- w m-x) orig-y
		 :ink +black+
		 :line-style lst)
    ;;vertical
    (draw-arrow* pane m-x orig-y m-x m-y
		 :ink +black+
		 :line-style lst)))

(defun draw-time-division (pane)
  "draws time division numbers and dashed lines"
  (loop
   with margin = (margin-y *application-frame*)
   with nm = (nmoments *application-frame*)
   with w = (get-width pane)
   with h = (get-height pane)
   for i to (1- nm)
   do (draw-text* pane
		  (format nil "~D" i)
		  (calc-hor-pos i nm w)
		  (- h (/ margin 3)))
   do (draw-line* pane
		  (calc-hor-pos i nm w)
		  (- h margin)
		  (calc-hor-pos i nm w)
		  margin
		  :line-style (make-line-style 
			       :dashes t))))		  

(defun draw-events  (pane coord-table)
  "draws all the events"
  (with-hash-table-iterator (next coord-table)
    (loop
     with count = (hash-table-count coord-table)
     with h = (get-height pane)
     for i from 0
     do (multiple-value-bind (found? letter coords) (next)
	  (unless found? (return))
	  (draw-event pane letter coords 
		      (calc-vert-pos i count h))))))

(defun draw-event (pane letter coords y)
  "calls to draw event's letter and ocurrences"
  (draw-event-lettre pane letter y)
  (draw-event-ocurrences pane y coords))

(defun draw-event-lettre (pane lettre y)
  "draws the name of the event on the left of the y axis"
  (draw-text* pane 
	      (format nil "~a" lettre)
	      (/ (margin-x *application-frame*) 2)
	      y
	      :ink +blue+))

(defun draw-event-ocurrences (pane y x-list)
  "iterates on the calculated coordinates
  for the horizontal positions list
  and draws the lines corresponding to them"
  (loop
   with width = (get-width pane)
   with nm = (nmoments *application-frame*)
   with x-coords = (calc-coords-list x-list nm  width)
   with x = 0
   for i from 0
   for c in x-coords
   if (evenp i) do (setf x c)
   else 
   do (draw-event-line pane c x y)))

(defun calc-coords-list (x-positions n width)
  "converts the positions list into a coordinates list"
  (make-pair-coord-list 
   (calc-hor-coords x-positions n width) width))

(defun make-pair-coord-list (coords width)
  "returns a list with a pair number of coordinates
  to be represented"
  ;;Takes the list of coordinates and the panel width
  ;;if it has an odd length, then the event is not
  ;;finishing, so we add a finishing value which
  ;;is the right border of the pane and return it.
  (if (oddp (length coords))
      (cons width coords)
      coords))

(defun calc-hor-coords (x-positions n width)
  "replaces each position by its coordinate in the canvas"
  (mapcar 
   (lambda (x) (calc-hor-pos x n width)) 
   x-positions))

(defun draw-event-line (pane c x y)
  "if it is a non-ending event it draws an 
  arrow. Normally it draws a line."
  (let ((lst (make-line-style :thickness 2)))
    (if (= x (get-width pane))
	(draw-arrow* pane c y x y
		     :ink +red+
		     :line-style lst)
	(draw-line* pane c y x y
		    :ink +red+ 
		    :line-style lst))))

(defun get-width (pane)
  "returns the current width of a pane"
  (bounding-rectangle-width (sheet-region pane)))

(defun get-height (pane)
  "returns the current height of a pane"
  (bounding-rectangle-height (sheet-region pane)))
 
(defun calc-vert-pos (pos n height)
  "returns the vertical coordinate of the event"
  ;;height - (margin + (margin-y / 3))
  ;;- pos * ((height - (margin * 2)) / n)
  (let ((margin (margin-y *application-frame*)))
    (- height
       (+ margin (/ margin 3))
       (* pos (/ (- height margin) n)))))

(defun calc-hor-pos (pos n width)
  "returns the horizontal position of the start or end"
  ;;of an event
  ;;margin + (pos * (width - margin)) / n
  (let ((margin (margin-x *application-frame*)))
    (+ margin (/ (* pos (- width  margin 2)) n))))