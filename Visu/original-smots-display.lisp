(in-package :common-lisp-user)

(defpackage :smots-display
(:use :smots :clim :clim-extensions :clim-lisp))

(in-package :smots-display)


(defvar *panelheight* 400)
(defvar *panelwidth* 600)



(defvar *word-ex* '())
(setf *word-ex* '(e d c b a))
(defvar *sword-ex* '())
(setf *sword-ex* '( (a) (b c) (a) (b d) (e) (c d) (e)))


(define-application-frame smots-display
() ()
(:panes
 (canvas :application
	 :display-time :command-loop
	 :display-function 'draw-objects
	 :height *panelheight*
	 :width *panelwidth*
	 )
; (com-window :interactor
;	     :height 100
;	     :width 600)
; (show :push-button
;       :label "Show"
;       :activate-callback 
;       (lambda (x) (declare (ignore x)) (com-show)))
)
(:layouts
 (default (vertically () canvas))))

(defun draw-objects (frame pane)
  (declare (ignore frame))
  (let ((coordlist 
	 (create-coord-list *sword-ex* *word-ex*))
	(coord '()))
;draws the axis
  (draw-arrow* pane 30 (- *panelheight* 30) (- *panelwidth* 30) (- *panelheight* 30)
	       :ink +black+
	       :line-style (make-line-style :thickness 3)) 
  (draw-arrow* pane 30 (- *panelheight* 30) 30 30
	       :ink +black+
	       :line-style (make-line-style :thickness 3)) 
;  (draw-rectangle* pane 
;		   50 50 100 100)
;draws the events from their coordinates in the coordlist;
;draws as well the texts indicating events names
  (dotimes (i (length coordlist)) 
    
    (setf coord (nth i coordlist))
    (draw-text* pane 
		(format nil "~a" (nth i *word-ex*))
		15 
		(first coord)
		:ink +blue+)
    
    (draw-line* pane 
		(second coord) ;x
		(first coord) ;y
		(third coord) ;x1 
		(first coord) ;y1
		:ink +red+ 
		:line-style (make-line-style 
			     :thickness 2)))))


(defun create-coord-list (sword word)
;sword is a list of lists (sletters)
;word is a list of letters present in the sword
;returns a list which 
;contains pairs of coordinates in the form
;(y x1 x2)
(let ((coordlist '())
      (letter nil)
      (lengthwrd (length word)))
  (dotimes (i (length word))
    (setf letter (nth i word))
    (setf coordlist (cons 
		     (list 
		      (calc-vert-pos i lengthwrd)
		      (calc-start letter sword)
		      (calc-end letter sword))
		     coordlist)))
  (reverse coordlist)))

(defun calc-vert-pos (pos n)
;height- 40 - pos*(height-60)/n
  (- *panelheight* 40 (* pos (/ (- *panelheight* 60) n))))

(defun calc-hor-pos (pos n)
;30 + pos * (width-100)/n
  (+ 30 (* pos (/ (- *panelwidth* 100) n))))

(defun calc-start (letter sword)
;returns the relative position in the window for the given
;letter according to its position in the sword
  (do ((i 0 (1+ i)))
      ((find letter (nth i sword) :test #'equal) 
       (calc-hor-pos i (length sword))) ()))

(defun calc-end (letter sword)
(let ((count 0))
  (do ((i 0 (1+ i)))
      ((= count 2)
       (calc-hor-pos i (length sword)))
    (if (find letter (nth i sword) :test #'equal)
	(incf count)))))


     

(defun main ()
  (run-frame-top-level (make-application-frame 'smots-display)))

;(define-smots-display-command (com-show :name t) ()
;  (draw-rectangle* (find-pane-named *application-frame* 'canvas) 
;		   10 20 90 100))

;(define-smots-display-command (com-draw :name t)
;()
;(let ((sword-string (accept 'string
;		      :prompt "Sword?"
;		      :default nil
;		      :display-default nil
;		      )))
;     (when sword-string
;	  (let ((sword (smots::parse-sword sword-string)))
;	    (format t "length: ~a~%" (smots::wlength sword))))))