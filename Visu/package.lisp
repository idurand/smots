(in-package :common-lisp-user)

(defpackage :visu-smots
  (:use :smots :clim :clim-extensions :clim-lisp))
