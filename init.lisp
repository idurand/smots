(in-package :smots)

(defun init ()
  (setf *data-directory* (initial-data-directory))
  (setf *specs*  (make-specs))
  (setf *spec*
        (set-current-spec (make-spec "empty")))
  *spec*)

(defun memoize ()
    (setf *memoize* t
     *memoize-mix-swords* t
     *memoize-ljoin-swords* t)
    (setf 
     (slot-value *spec* 'mix-sword-table) (make-hash-table :test #'equal)
     (slot-value *spec* 'mix-basic-sword-table) (make-hash-table :test #'eql)
     (slot-value *spec* 'ljoin-sword-table) (make-hash-table :test #'equal)))

(defun dememoize ()
  (setf *memoize* nil
	*memoize-mix-swords* nil
	*memoize-ljoin-swords* nil))

