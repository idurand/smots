(in-package :smots)

(defvar *union-lop* (make-instance
		     'lop
		     :symbol-lop "U"
		     :lfun-lop #'lunion
		     :priority 2
		     :cfun-lop #'+
		     :fun-lop #'union))
				   
(defclass lunion (s-mixin bool-mixin assoc-lexpr)
  ((lop :allocation :class :initform *union-lop* :reader lop)))

(defun lunionp (o)
  (or (typep o 'lunion) (swordp o)))

(defun make-lunion (lexprs)
;;  (format *trace-output* "make-lunion  ~A~%" lexprs)
  (cond
    ((every #'swordp lexprs)
     (make-slanguage lexprs))
    ((every #'languagep lexprs)
     (reduce #'lunion lexprs))
    (t
     (make-assoc-lexpr lexprs 'lunion 'sword))))

(defmethod lunion ((lexpr1 alanguage) (lexpr2 alanguage))
  (make-lunion (list lexpr1 lexpr2)))

(defmethod lunion ((lexpr1 alanguage) (lexpr2 mix))
  (if (delanoyp lexpr2 (malphabet-of lexpr1))
      lexpr2
      (call-next-method)))

(defmethod lunion ((lexpr1 mix) (lexpr2 language))
  (if (delanoyp lexpr1 (malphabet-of lexpr2))
      lexpr1
      (call-next-method)))

(defmethod extreme-letters ((lexpr lunion) &optional (last nil))
;;  (format *error-output* "extreme-letters lunion~%")
  (remove-duplicates
   (mappend (lambda (args) (extreme-letters args last))
	    (args lexpr))))

(defmethod clean-args :after ((lexpr lunion))
  (when *trace-clean-args*
    (format *error-output* "clean-args  lunion ~A~%" lexpr))
  (let ((args (args lexpr)))
    (setf (args lexpr) (remove-if #'empty-languagep args)))
  lexpr)

(defmethod contains-empty-sword ((lunion lunion))
  (some #'contains-empty-sword  (args lunion)))

(defmethod elementaryp ((lunion lunion))
  (values nil nil))

(defmethod directedp ((lunion lunion))
  (let ((args (args lunion)))
    (and (every #'directedp args)
	 (let ((egraphs (mapcar #'egraph-from-eexpr args)))
	   (or
	    (reduce #'egraph-union egraphs)
	    (values nil t))))))

(defmethod egraph-from-eexpr ((lunion lunion))
  (assert (directedp lunion))
  (reduce #'egraph-union (mapcar #'egraph-from-eexpr (args lunion))))

(defmethod compute-cardinality ((lunion lunion))
  (loop
    for arg in (args lunion)
    for card = (cardinality arg) then (cardinality arg)
    for total = 0 then (+ total card)
    unless card
      do (return)
    finally (return (+ total card))))



