(in-package :smots)
;; a node is a sletter
;; an edge is a two letter sword
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Edge: an edge is a sword of length 2 (2 sletters)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun possible-edgep (origin extremity)
  (correct-sletters (list origin extremity)))

(defun possible-trivial-implicit-edgep (origin extremity)
  (let ((morigin (letters origin))
	(mextremity (letters extremity)))
    (and (null (cdr morigin))
	 (null (cdr mextremity))
	 (let ((mo (car morigin))
	       (me (car mextremity)))
	   (and
	    (eq (letter mo) (letter me))
	    (< (mark mo) (mark me)))))))

(defun possible-implicit-edgep (origin extremity)
  (and (possible-edgep origin extremity)
       (let ((ai (aintersection (alphabet-of origin) (alphabet-of extremity))))
	 (some (lambda (letter)
		 (possible-trivial-implicit-edgep
		  (projection origin (make-alphabet (list letter)))
		  (projection extremity (make-alphabet (list letter)))))
	       (letters ai)))))
  
(defun make-edge (origin extremity)
  (assert (possible-edgep origin extremity))
  (make-sword (list origin extremity)))

(defun edge-origin (edge)
  (car (letters edge)))

(defun edge-extremity (edge)
  (cadr (letters edge)))

(defun implicit-edgep (edge)
;; assuming the edge is correct
  (not (aemptyp
	(aintersection
	 (alphabet-of (edge-origin edge))
	 (alphabet-of (edge-extremity edge))))))

(defun inverse-edge (edge)
  (make-edge (edge-extremity edge) (edge-origin edge)))

(defun edge-nodes (edge)
  (letters edge))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Node
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun node-outgoing-edges (node edges)
  (remove-if-not
   (lambda (edge)
     (eq (edge-origin edge) node))
   edges))

(defun node-ingoing-edges (node edges)
  (remove-if-not
   (lambda (edge)
     (eq (edge-extremity edge) node))
   edges))

(defun node-in-degree (node edges)
  (length (node-ingoing-edges node edges)))

(defun node-out-degree (node edges)
  (length (node-outgoing-edges node edges)))

(defun node-outgoing-nodes (node edges)
  (mapcar #'edge-extremity (node-outgoing-edges node edges)))

(defun node-ingoing-nodes (node edges)
  (mapcar #'edge-origin (node-ingoing-edges node edges)))

(defun node-strict-descendant-nodes (node edges)
  (nodes-strict-descendant-nodes (list node) edges))

(defun node-strict-antecedant-nodes (node edges)
  (nodes-strict-antecedant-nodes (list node) edges))

(defun node-originp (node edges)
  (not (node-ingoing-edges node edges)))

(defun node-extremityp (node edges)
  (not (node-outgoing-edges node edges)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Nodes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun nodes-outgoing-edges (nodes edges)
  (reduce #'append
	  (mapcar (lambda (node)
		    (node-outgoing-edges node edges))
		  nodes)
	  :from-end t))

(defun nodes-ingoing-edges (nodes edges)
  (reduce #'append
	  (mapcar (lambda (node)
		    (node-ingoing-edges node edges))
		  nodes)
	  :from-end t))

(defun nodes-outgoing-nodes (nodes edges)
  (mapcar #'edge-extremity (nodes-outgoing-edges nodes edges)))

(defun nodes-ingoing-nodes (nodes edges)
  (mapcar #'edge-origin (nodes-ingoing-edges nodes edges)))

(defun nodes-strict-descendant-nodes (nodes edges)
  (set-difference (nodes-descendant-nodes nodes edges) nodes))

(defun nodes-strict-antecedant-nodes (nodes edges)
  (set-difference (nodes-antecedant-nodes nodes edges) nodes))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Edges
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun edges-nodes (edges)
  (letters (slalphabet-of edges)))

(defun node-cycle-p (node edges)
  (loop
     with todo = (list node)
     while todo
     do (let* ((n (pop todo))
	       (next (node-outgoing-nodes n edges)))
	  (if (member node next)
	      (return-from node-cycle-p t)
	      (setf todo (nconc todo next))))))

(defun edge-cycle-p (edge edges)
  (node-cycle-p (edge-origin edge) (cons edge edges)))

(defun edge-antisymmetricp (edge edges)
  (or
   (implicit-edgep edge)
   (not (member (inverse-edge edge) edges :test #'eq))))

(defun edges-antisymmetricp (edges)
  (every 
   (lambda (edge) (edge-antisymmetricp edge edges))
   edges))

(defun node-antitransitivep (node edges)
  (let ((cn (node-outgoing-nodes node edges)))
    (not
     (intersection
      cn
      (nodes-strict-descendant-nodes cn edges)
      :test #'eq))))

(defun edges-antitransitivep (edges)
  (assert (edges-antisymmetricp edges))
  (every 
   (lambda (node) (node-antitransitivep node edges))
   (edges-nodes edges)))

(defun edges-hassep (edges)
  (and
   (edges-antisymmetricp edges)
   (notany (lambda (edge)
	     (edge-cycle-p edge edges))
	   edges)))

(defun edges-origin-nodes (edges)
  (remove-if-not
   (lambda (node) (node-originp node edges))
   (edges-nodes edges)))

(defun edges-extremity-nodes (edges)
  (remove-if-not (lambda (node) (node-extremityp node edges))
		 (edges-nodes edges)))

;; valables quand on a deja un dag
(defun nodes-strict-descendant-edges (nodes edges)
  (if (endp nodes)
      '()
      (let ((new (nodes-outgoing-edges nodes edges)))
	(union
	 new
	 (nodes-strict-descendant-edges
	  (set-difference new nodes :test #'eq)
	  edges)))))

(defun nodes-strict-antecedant-edges (nodes edges)
  (if (endp nodes)
      '()
      (let ((new (nodes-ingoing-edges nodes edges)))
	(union
	 new
	 (nodes-strict-antecedant-edges
	  (set-difference new nodes :test #'eq)
	  edges)))))

(defun nodes-descendant-edges-sf (nodes edges stop-fun)
  (reduce
   (lambda (x y) (union x y :test #'eq))
   (mapcar (lambda (node)
	     (node-descendant-edges-sf node edges stop-fun))
	   nodes)
   :initial-value '()
   :from-end t))

(defun node-descendant-edges-sf (node edges stop-fun)
  (let ((oes (node-outgoing-edges node edges)))
    (if (endp oes)
	'()
	(append oes
		(nodes-descendant-edges-sf
		 (mapcar #'edge-extremity (remove-if stop-fun oes))
		 edges
		 stop-fun)))))

(defun node-descendant-edges-z (node edges)
  (node-descendant-edges-sf
   node edges
   (lambda (edge)
     (> (length (node-ingoing-edges (edge-extremity edge) edges)) 1))))

(defun node-antecedant-edges-z (node edges)
  (node-antecedant-edges-sf
   node edges
   (lambda (edge)
     (> (length (node-outgoing-edges (edge-origin edge) edges)) 1))))

(defun nodes-descendant-edges (nodes edges)
  (nodes-descendant-edges-sf
   nodes
   edges
   (lambda (edge)
     (node-extremityp (edge-extremity edge) edges))))

(defun node-descendant-edges (node edges)
  (nodes-descendant-edges (list node) edges))

(defun nodes-antecedant-edges-sf (nodes edges stop-fun)
  (reduce
   (lambda (x y) (union x y :test #'eq))
   (mapcar (lambda (node)
	     (node-antecedant-edges-sf node edges stop-fun))
	   nodes)
   :initial-value '()
   :from-end t))

(defun nodes-antecedant-edges (nodes edges)
  (nodes-antecedant-edges-sf
   nodes
   edges
   (lambda (edge) (declare (ignore edge)) t)))

(defun node-antecedant-edges-sf (node edges stop-fun)
  (let ((oes (node-ingoing-edges node edges)))
    (if (endp oes)
	'()
	(append oes
		(nodes-antecedant-edges-sf
		 (mapcar
		  #'edge-origin
		  (remove-if stop-fun oes))
		  edges stop-fun)))))

(defun node-antecedant-edges (node edges)
  (nodes-antecedant-edges (list node) edges))

(defun nodes-descendant-nodes-sf (nodes edges stop-fun)
  (edges-nodes
   (nodes-descendant-edges-sf
    nodes edges
    (lambda (edge)
      (funcall stop-fun (edge-extremity edge))))))

(defun nodes-descendant-nodes (nodes edges)
  (nodes-descendant-nodes-sf
   nodes
   edges
   (lambda (node) (node-extremityp node edges))))

(defun nodes-antecedant-nodes-sf (nodes edges stop-fun)
  (edges-nodes
   (nodes-antecedant-edges-sf
    nodes
    edges
    (lambda (edge)
      (funcall stop-fun (edge-origin edge))))))

(defun nodes-antecedant-nodes (nodes edges)
  (nodes-antecedant-nodes-sf
   nodes edges
   (lambda (node)
     (node-originp node edges))))

(defun node-ad-edges (node edges)
  (union (node-antecedant-edges node edges)
	 (node-descendant-edges node edges)
	 :test #'eq))

(defun nodes-ad-edges (nodes edges)
  (reduce (lambda (x y) (union x y :test #'eq))
	  (mapcar (lambda (node)
		    (node-ad-edges node edges))
		  nodes)))

(defun node-ad-nodes (node edges)
  (edges-nodes (node-ad-edges node edges)))

(defun nodes-ad-nodes (nodes edges)
  (edges-nodes (nodes-ad-edges nodes edges)))

(defun nodes-connected-to-node (node edges)
  (do* ((todo (list node) new)
	(done todo (append done todo))
	(new (set-difference (nodes-ad-nodes todo edges) done :test #'eq)
	     (set-difference (nodes-ad-nodes todo edges) done :test #'eq)))
       ((endp new) done)))

(defun nodes-connected-to-nodes (nodes edges)
  (reduce (lambda (x y) (union x y :test #'eq))
	  (mapcar (lambda (node)
		    (nodes-connected-to-node node edges))
		  nodes)))

(defun edge-directly-ingoing-at-nodep (edge node)
 (eq (edge-extremity edge) node))

(defun edge-directly-outgoing-at-nodep (edge node)
 (eq (edge-origin edge) node))

(defun edge-directly-connected-to-nodep (edge node)
  (or (edge-directly-ingoing-at-nodep edge node)
      (edge-directly-outgoing-at-nodep edge node)))

(defun edge-directly-outgoing-at-some-nodep (edge nodes)
  (some (lambda (node) (edge-directly-outgoing-at-nodep edge node))
	nodes))

(defun edge-directly-ingoing-at-some-nodep (edge nodes)
  (some (lambda (node) (edge-directly-ingoing-at-nodep edge node))
	nodes))

(defun edge-directly-connected-to-some-nodep (edge nodes)
  (some (lambda (node) (edge-directly-connected-to-nodep edge node))
	nodes))

;; returns list of lists of nodes
(defun edges-connected-components (edges)
  (if (endp edges)
      '()
      (let* ((origin (car (edges-origin-nodes edges)))
	     (cc (nodes-connected-to-node origin edges)))
	(cons cc
	      (edges-connected-components
	       (remove-if (lambda (edge)
			    (edge-directly-connected-to-some-nodep edge cc))
			  edges))))))
	
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; egraph
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass egraph (alanguage s-mixin)
  ((freenodes :initform nil :reader egraph-freenodes :initarg :freenodes)
   (edges :initform nil :reader egraph-edges :initarg :edges)))

(defmethod compute-language ((egraph egraph))
  (compute-language (egraph-expression egraph)))

(defmethod directedp ((egraph egraph))
  t)

;; a specialiser
(defmethod weight ((egraph egraph))
  (weight (egraph-expression egraph)))

;; a specialiser
(defmethod extreme-letters ((egraph egraph) &optional last)
  (extreme-letters (egraph-expression egraph) last))

(defun make-egraph (edges &optional (freenodes nil))
  (make-instance 'egraph :edges edges :freenodes freenodes))

(defun make-verified-egraph (edges &optional (freenodes nil))
  (assert (edges-hassep edges))
  (assert (every #'sletterp freenodes))
  (make-egraph edges freenodes))

(defmethod compute-language ((egraph egraph))
  (compute-language (egraph-expression egraph)))

(defun egraph-extremity-nodes (g)
  (append (egraph-freenodes g) (edges-extremity-nodes (egraph-edges g))))

(defun egraph-origin-nodes (g)
  (append (egraph-freenodes g) (edges-origin-nodes (egraph-edges g))))

(defun egraph-copy (g)
  (with-slots (edges freenodes) g
  (make-egraph (copy-list edges)
	       (copy-list freenodes))))

(defmethod gen-calphabet-of ((g egraph) lettertype)
  (with-slots (freenodes edges) g
    (make-alphabet
     (append
      (letters (gen-calphabet-of freenodes lettertype))
      (letters (gen-calphabet-of edges lettertype)))
     lettertype)))

(defun egraph-nodes (g)
  (letters (slalphabet-of g)))

(defun egraph-empty (g)
  (with-slots (freenodes edges) g
    (and (endp freenodes) (endp edges))))

(defmethod print-object ((g egraph) stream)
  (let ((e (egraph-expression g)))
    (unless (swordp e)
      (format stream "#"))
    (print-object e stream)))

;; (defmethod print-object :after ((g egraph) stream)
;;   (format stream "Edges: ~A " (egraph-edges g))
;;   (when (egraph-freenodes g)
;;     (format stream "Freenodes: ~A" (egraph-freenodes g))))

(defun connect-edges (edges1 edges2)
  (let ((edges ()))
    (dolist (e1 edges1 edges)
      (dolist (e2 edges2)
	(push (make-edge (edge-origin e1) (edge-extremity e2))
	      edges)))))

(defun split-edges-at-node (node edges)
  (let ((with-origin ())
	(with-extremity ())
	(rest ()))
    (dolist (edge edges)
      (cond
	((eq (edge-origin edge) node)
	 (push edge with-origin))
	((eq (edge-extremity edge) node)
	 (push edge with-extremity))
	(t
	 (push edge rest))))
    (append rest (connect-edges with-extremity with-origin))))
       
(defun egraph-delete-node (node g)
  (with-slots (freenodes edges) g
    (let ((old-nodes (remove node (edges-nodes edges)))
	  (new-edges (split-edges-at-node node edges)))
      (if (member node freenodes)
	  (setf freenodes (delete node freenodes))
	  (setf edges new-edges
		freenodes
		(set-difference old-nodes (edges-nodes new-edges)
				:test #'eq)))))
  g)

(defun egraph-remove-node (node g)
  (egraph-delete-node node (egraph-copy g)))

(defun egraph-delete-nodes (nodes g)
  (loop for node in nodes
       do (egraph-delete-node node g))
  g)

(defun egraph-remove-nodes (nodes g)
  (egraph-delete-nodes nodes (egraph-copy g)))

(defun egraph-ldisjointp (g1 g2)
;; no common letters
  (aemptyp (aintersection (alphabet-of g1) (alphabet-of g2))))

(defmethod egraph-ldisjoint-union ((g1 egraph) (g2 egraph))
  (assert (egraph-ldisjointp g1 g2))
  (make-egraph
   (append (egraph-edges g1) (egraph-edges g2))
   (append (egraph-freenodes g1) (egraph-freenodes g2))))

(defmethod projection ((g egraph) (slalphabet alphabet))
  (assert (not (aemptyp slalphabet)))
  (let ((sletters (letters slalphabet)))
    (assert sletters)
    (assert (eq 'sletter (type-of (car sletters))))
    (let* ((edges (egraph-edges g))
	   (nodes-to-remove (set-difference (edges-nodes edges)
					    sletters
					    :test #'eq)))
      (egraph-remove-nodes nodes-to-remove g))))

(defun edges-useful-edge (edge edges)
  (let ((d (node-strict-descendant-nodes (edge-origin edge) edges)))
    (not (member (edge-extremity edge) d :test #'eq))))
   
(defun edges-compatible-edge (edge edges)
  (and (not (edge-cycle-p edge edges))))

(defun get-rid-of-redondant-edges (new edges)
;; get rid of edges which become redondant when adding new
  (let ((descendants (node-strict-descendant-nodes (edge-extremity new) edges))
	(antecedants (node-strict-antecedant-nodes (edge-origin new) edges)))
    (remove-if (lambda (edge)
		 (or
		  (and (member (edge-origin edge) antecedants)
		       (eq (edge-extremity edge) (edge-extremity new)))
		  (and (member (edge-extremity edge) descendants)
		       (eq (edge-origin edge) (edge-origin new)))))
	       edges)))

(defun edges-add-edge (edge edges)
  (assert (edges-compatible-edge edge edges))
  (if (edges-useful-edge edge edges)
      (cons edge (get-rid-of-redondant-edges edge edges))
    edges))

(defun edges-try-add-edge (edge edges)
  (if (edges-compatible-edge edge edges)
      (edges-add-edge edge edges)
    (return-from edges-try-add-edge nil)))

(defun edges-try-add-edges (newedges edges)
;;  (assert edges)
;; what was this assert for?
  (dolist (newedge newedges edges)
    (setf edges (edges-try-add-edge newedge edges))
    (unless edges
	(return-from edges-try-add-edges nil))))

(defun possibly-complete-edge (origin extremity nodes)
  (let ((norig (find-if (lambda (node)
			  (intersection (letters origin) (letters node)))
			nodes))
	(nextr (find-if (lambda (node)
			  (intersection (letters extremity) (letters node)))
			nodes)))
    (and (possible-edgep norig nextr)
	 (make-edge norig nextr))))
	
(defun possibly-complete-edges (edges nodes)
  (let ((new-edges '()))
    (dolist (edge edges new-edges)
      (let ((new-edge (possibly-complete-edge
		       (edge-origin edge)
		       (edge-extremity edge)
		       nodes)))
	(if new-edge
	    (pushnew new-edge new-edges)
	    (return-from possibly-complete-edges nil))))))

(defmethod egraph-connected-components ((g egraph))
  (let ((cc (edges-connected-components (egraph-edges g)))
	(freenodes (egraph-freenodes g)))
    (if freenodes
	(append (mapcar #'list freenodes) cc)
	cc)))

(defmethod bipartite-edges (nodes1 nodes2)
  (assert (not (intersection nodes1 nodes2 :test #'eq)))
  (mapcar (lambda (pair)
	    (make-edge (car pair) (cadr pair)))
	  (cartesian-product (list nodes1 nodes2))))

;; a tester
(defmethod egraph-not-disjoint-union ((g1 egraph) (g2 egraph))
  (let ((nodes (try-to-join-sletters (letters (slalphabet-of g1))
				     (letters (slalphabet-of g2)))))
    (unless nodes
      (return-from egraph-not-disjoint-union nil))
    (let ((edges
	   (mapcar (lambda (pair)
			  (make-edge (car pair) (cadr pair)))
		   (remove-if-not
		    (lambda (pair)
		      (possible-implicit-edgep (car pair) (cadr pair)))
		    (cartesian-product (list nodes nodes))))))
      (when (egraph-edges g1)
	(let ((edges1 (possibly-complete-edges (egraph-edges g1) nodes)))
	  (unless edges1
	    (return-from egraph-not-disjoint-union nil))
	  (setf edges (edges-try-add-edges edges1 edges))
	  (unless edges
	    (return-from egraph-not-disjoint-union nil))))
      (when (egraph-edges g2)
	(let ((edges2 (possibly-complete-edges (egraph-edges g2) nodes)))
	  (unless edges2
	    (return-from egraph-not-disjoint-union nil))
	  (setf edges (edges-try-add-edges edges2 edges))
	  (unless edges
	    (return-from egraph-not-disjoint-union nil))))
      (make-egraph
       edges
       (set-difference nodes (edges-nodes edges))))))

;; en fait la jointure
(defmethod egraph-union (g1 (g2 (eql nil)))
  nil)

(defmethod egraph-union ((g1 (eql nil)) (g2 egraph))
  nil)

(defmethod egraph-union ((g1 egraph) (g2 egraph))
  (cond
    ((or (null g1) (null g2)) nil)
    ((egraph-ldisjointp g1 g2)
     (egraph-ldisjoint-union g1 g2))
    (t
     (egraph-not-disjoint-union g1 g2))))
