(in-package :smots)

(defun hat (char)
  (assert (alpha-char-p char))
  (code-char
   (case (char-code char)
     (101 234)
     (otherwise 63))))


(defun trema (char)
  (assert (alpha-char-p char))
  (code-char
   (case (char-code char)
     (97 228)
     (101 235)
     (otherwise 63))))
