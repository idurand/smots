(in-package :smots)

(defvar *simplify-sletter-output* nil)

(defgeneric sletters (specification)
  (:documentation "current sletters of SPECIFICATION"))

(defgeneric (setf sletters) (val specification)
 (:documentation "writer on current sletters of SPECIFICATION"))

(defvar *internal-sletters* nil)

(defmethod sletters ((spec (eql nil)))
  *internal-sletters*)

(defmethod (setf sletters) (val (spec (eql nil)))
  (setf *internal-sletters* val))

(defclass s-mixin (m-mixin)
  ((slalphabet-of :initform nil))
  (:documentation "for languages whose words are Swords"))

(defclass abstract-sletter ()())

(defclass sletter (word abstract-letter s-mixin abstract-sletter) ())

;; pour les s-mixin objects memoisation
(defmethod slalphabet-of :around ((s-mixin s-mixin))
  (with-slots (slalphabet-of) s-mixin
    (or slalphabet-of
	(setf slalphabet-of (call-next-method)))))

;; pour les autres on doit construire
(defmethod slalphabet-of (o)
  (gen-calphabet-of o 'sletter))

(defun sletterp (o)
  (typep o 'sletter))

(defun slettersp (l)
  (every #'sletterp l))

(defmethod print-object ((sletter sletter) stream)
  (let ((mletters (letters sletter)))
    (if (and *simplify-sletter-output* (null (cdr mletters)))
	(prin1 (car mletters) stream)
	(progn 
	  (write-char #\{ stream)
	  (display-sequence mletters stream :sep ",")
	  (write-char #\} stream)))))

(defgeneric no-duplicated-letter (mletters)
  (:documentation
   "true if no two mletters in MLETTERS have the same corresponding
    letter"))

(defmethod no-duplicated-letter ((mletters list))
  (let ((letters (letters-from-mletters mletters)))
    (= (length mletters)
       (length letters))))

(defgeneric make-sletter (mletters)
  (:documentation "return the sletter whose mletters are MLETTERS"))

(defun valid-mletters (mletters)
  (= (length mletters)
     (length (remove-duplicates mletters
				:test #'=
				:key #'letter-number))))

(defmethod make-sletter ((mletters list))
  (assert (every (lambda (letter) (typep letter 'mletter)) mletters))
  (assert (no-duplicated-letter mletters))
  (assert (valid-mletters mletters))
  (setf mletters (sort-letters mletters))
  (or (find-if
       (lambda (sletter)
	 (equal mletters (letters sletter)))
       (sletters *spec*))
      (let ((sletter (make-instance 'sletter :letters mletters
					     :lettertype 'mletter)))
	(push sletter (sletters *spec*))
	sletter)))

(defgeneric empty-sletterp (sletter)
  (:documentation "true if SLETTER is the empty one"))

(defmethod empty-sletterp ((sletter sletter))
  (endp (letters sletter)))

(defun make-slalphabet (sletters)
  (make-alphabet sletters 'sletter))

(defgeneric arity (sletter)
  (:documentation "number of mletters in SLETTER"))

(defmethod arity ((sletter sletter))
  (length (letters sletter)))

(defgeneric projection-mletters (mletters alphabet))

(defmethod projection-mletters ((mletters list) (alphabet alphabet))
  (intersection
   mletters
   (letters alphabet)
   :test  (if (eq (type-of (car (letters alphabet))) 'letter)
	      #'equiv-named-letters
	      #'eq)))

(defmethod projection ((sletter sletter) (alphabet alphabet))
  (make-sletter
   (projection-mletters
    (letters sletter)
    alphabet)))

(defun malphabet-from-slalphabet (slalphabet)
  (make-alphabet
   (mappend #'letters
	    (letters slalphabet))
   'mletter))

(defmethod slalphabet-of (o)
  (gen-calphabet-of o 'sletter))

(defmethod gen-calphabet-of ((sletter sletter) lettertype)
;;  (format *error-output* "gen-calphabet-of mletter t ~A~%" mletter)
  (gen-calphabet-of (letters sletter) lettertype))

(defmethod gen-alphabet-of (o (lettertype (eql 'sletter)))
  (declare (ignore lettertype))
  (slalphabet-of o))

(defun find-sletter-with-mletter (mletter sletters)
  (find-if (lambda (sletter)
	     (member mletter (letters sletter) :test #'eq))
	   sletters))

(defun merge-sletter-with-sletters (sletter sletters)
  (multiple-value-bind (intersecting not-intersecting)
      (split sletters 
	     (lambda (sl)
	       (intersection (letters sl) (letters sletter))))
    (if intersecting
	(let ((mletters 
	       (reduce (lambda (x y) (union x y :test #'eq))
		       (mapcar #'letters (cons sletter intersecting)))))
	  (if (valid-mletters mletters)
	      (cons (make-sletter mletters) not-intersecting)
	      nil))
	(cons sletter not-intersecting))))

;; a tester
(defun try-to-join-sletters (sletters1 sletters2)
  (assert (or sletters1 sletters2))
  (unless sletters1
    (return-from try-to-join-sletters sletters2))
  (unless sletters2
    (return-from try-to-join-sletters sletters1))
  (let ((sletters sletters2))
    (dolist (sletter sletters1 sletters)
      (setf sletters (merge-sletter-with-sletters sletter sletters))
      (unless sletters
	(return-from try-to-join-sletters nil)))))

(defgeneric simple-sletterp (sletter)
  (:documentation "SLETTER contains only one mletter"))

(defmethod simple-sletterp ((sletter sletter))
  (null (cdr (letters sletter))))
