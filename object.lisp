(in-package :smots)

(defgeneric display-object (o &optional stream)
  (:documentation "print an Autowrite object O defined in Autowrite to the stream STREAM when defined to *standard-output* otherwise"))

(defgeneric compare-object (o1 o2)
  (:documentation "compare the content of two Autowrite objects"))

(defgeneric display-sequence (l stream &key sep)
  (:documentation "print the objects of the sequence L separated by the separator SEP on the STREAM"))

(defmethod display-object ((o t) &optional stream)
  (print-object o stream))

(defmethod compare-object ((l1 sequence) (l2 sequence))
  (or (eq l1 l2)
      (and (compare-object (car l1) (car l2))
	   (compare-object (cdr l1) (cdr l2)))))

(defmethod display-object ((l sequence) &optional (stream t))
  (mapc (lambda (x)
	  (display-object x stream)
	  (format stream " ")) l))

(defmethod display-sequence ((l sequence) stream &key (sep " "))
  (when l
    (mapc (lambda (e)
	    (format stream "~A~A" e sep))
	  (butlast l))
    (format stream "~A" (car (last l)))))

(defmethod display-sequence ((l vector) stream &key (sep " "))
  (display-sequence (coerce l 'list) stream :sep sep))
