
;; ne pas lancer slime mais juste sbcl
;;(sb-thread:list-all-threads)

(asdf:oos 'asdf:compile-op :smots)
(asdf:oos 'asdf:compile-op :visu-smots)
(asdf:oos 'asdf:load-op :smots)
(asdf:oos 'asdf:load-op :visu-smots)
(in-package :visu-smots)
(sb-ext:save-lisp-and-die "sls.core" 
				 :toplevel #'start :purify t)

