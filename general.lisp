(in-package :smots)

(defun mappend (fn &rest lsts)
  (apply #'append (apply #'mapcar fn lsts)))

(defun cartesian-product (args)
  (if (null args)
      '(())
      (distribute-set (car args) (cartesian-product (cdr args)))))

(defun distribute (e setofsets)
  (mapcar (lambda (x) (cons e x)) setofsets))

(defun distribute-set (set setofsets)
  (mapcan (lambda (x) (distribute x setofsets)) set))

(defun powerset (s)
  (if (null s)
      '(())
      (let ((p (powerset (cdr s))))
	(append p (distribute (car s) p)))))

(defun disjointp (l &key (key #'identity) (test #'eql))
  (do ((l1 l (cdr l1)))
      ((endp (cdr l1)) t)
    (dolist (l2 (cdr l1))
      (when (intersection (funcall key (car l1))
			  (funcall key l2)
			  :test test)
	(return-from disjointp nil)))))

(defun split (l pred)
  (let ((lt '())
	(lf '()))
    (dolist (e l (values (nreverse lt) (nreverse lf)))
      (if (funcall pred e)
	  (push e lt)
	  (push e lf)))))

(defun print-spaces (n stream)
  (dotimes (i n)
    (princ #\Space stream)))

(defun make-numbered-pairs (l)
  (let ((i -1))
    (mapcar #'(lambda (e)
		(cons e (incf i)))
	    l)))

(defun rank (e lp &key (test #'eql))
  (cdr (assoc e lp :test test)))


;; il faut respecter l'ordre des elements
(defun partitions (l)
  (if (endp l)
      '()
      (let ((partitions (list (mapcar #'list l)))
	    (sp (partitions (cdr l))))
	(mapc #'(lambda (partition)
		  (do ((ss partition (cdr ss))
		       (done '() (append (list (car ss)) done)))
		      ((endp ss))
		    (push (append
			   (cons
			    (cons (car l)
				  (car ss)) (cdr ss))
			   done)
			  partitions)))
	      sp)
	partitions)))

(defun iota (n &optional (start 0) (step 1))
  (loop repeat n
        for i from start by step
        collect i))
