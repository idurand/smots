(in-package :smots)

(defvar *mirror-lop* (make-lop "~" #'mirror 3))

(defclass mirror (unary-lexpr s-mixin)
  ((lop :allocation :class
	:initform *mirror-lop*
	:reader lop)))

(defun mirrorp (lexpr)
  (typep lexpr 'mirror))

(defun make-mirror (lexpr)
  (make-unary-lexpr lexpr 'mirror))

(defmethod cut-extreme-letter ((mirror mirror) (letter abstract-letter) &optional (last nil))
;;  (format *error-output* "cut-extreme-letter mirror ~%")
  (let ((res (cut-extreme-letter (arg mirror) letter (not last))))
    (when res
      (make-mirror res))))

(defmethod compute-language ((mirror mirror))
;;  (format *error-output* "compute-language mirror ~%")
  (let ((language (compute-language (arg mirror))))
    (make-language
     (mapcar
      #'mirror
      (words language))
     :wordtype (wordtype mirror)
     :languagetype (languagetype mirror))))
