(in-package :smots)

(defvar *lintersection-lop* (make-instance
		     'lop
		     :symbol-lop "I"
		     :lfun-lop #'lintersection
		     :priority 3
		     :fun-lop #'intersection))

(defclass lintersection (bool-mixin ljoin)
  ((lop :allocation :class :initform *lintersection-lop* :reader lop)))

(defun lintersectionp (o)
  (typep o 'lintersection))

(defun make-lintersection (lexprs)
  (make-assoc-lexpr lexprs 'lintersection 'sword))

(defmethod clean-args :after ((lexpr lintersection))
  (when *trace-clean-args*
    (format *error-output* "clean-args :after lintersection ~A~%" lexpr))
  (let ((args (args lexpr)))
    (when (and (not (null (cdr args))) (not-intersecting-alphabets args))
      (setf (args lexpr) '())))
  lexpr)

(defmethod lintersection ((lexpr1 alanguage) (lexpr2 mix))
  (if (delanoyp lexpr2 (malphabet-of lexpr1))
      lexpr1
      (call-next-method)))

(defgeneric inter-cutting-first-letter (alanguage1 alanguage2 sletter))

(defmethod inter-cutting-first-letter
    ((lexpr1 alanguage) (lexpr2 alanguage) (sl sletter))
  (let ((e1 (cut-first-letter lexpr1 sl))
	(e2 (cut-first-letter lexpr2 sl)))
    (let ((lintersection
	   (if (or (empty-languagep e1) (empty-languagep e2))
	       (empty-language lexpr1)
	       (when (and e1 e2)
		 ;; si on a ete autorise a faire les resuduels
		 ;; faut-il un join?
		 (concatenation
		  (make-sword (list sl))
		  (if (or (lstarp e1) (lstarp e2))
		      (make-ljoin (list e1 e2))
		      (ljoin e1 e2)))))))
      lintersection)))

(defgeneric linter-with-first-letters (alanguage1  alanguage2))

(defmethod linter-with-first-letters ((lexpr1 alanguage) (lexpr2 alanguage))
  ;;(assert (eq (malphabet-of lexpr1) (malphabet-of lexpr2)))
;;  (assert (and (not (empty-wordp lexpr1)) (not (empty-wordp lexpr2))))
  (let* ((fsls1 (first-letters lexpr1))
	 (fsls2 (first-letters lexpr2))
	 (ifsls (intersection fsls1 fsls2)))
    (let ((lintersection
	   (if ifsls
	       (if (and (> *level-first* 0) (null (cdr ifsls)))
		   (inter-cutting-first-letter lexpr1 lexpr2 (car ifsls))
		   (if (> *level-first* 1)
		       (let ((args (mapcar
				    (lambda (sl)
				      (inter-cutting-first-letter lexpr1 lexpr2 sl))
				    ifsls)))
			 (unless (some #'null args)
			   (make-lunion args)))
		       nil))
	       (empty-language lexpr1))))
      lintersection)))

(defmethod lintersection ((lexpr1 alanguage) (lexpr2 alanguage))
  (let ((malpha1 (malphabet-of lexpr1))
	(malpha2 (malphabet-of lexpr2)))
;;    (assert (eq malpha1 malpha2))
    (when (or (empty-languagep lexpr1) (empty-languagep lexpr2))
      (return-from lintersection (empty-language lexpr1)))
    (let ((lintersection
	   (cond
	     ((eq lexpr1 lexpr2)
	      lexpr1)
	     ((delanoyp lexpr1 malpha1)
	      lexpr2)
	     ((delanoyp lexpr2 malpha2)
	      lexpr1)
	     ((and (swordp lexpr1)
		   (swordp lexpr2))
	      (empty-sword))
	     (t (or
		 (and (> *level-first* 0)
		      (linter-with-first-letters lexpr1 lexpr2))
		 (make-lintersection (list lexpr1 lexpr2)))))))
      lintersection)))

;;       (assert (equivalent (compute-language lintersection)
;; 			  (lintersection
;; 			   (compute-language lexpr1)
;; 			   (compute-language lexpr2))))

;; faut-il vraiment que lintersection herite de ljoin?
;; oui pour l'élément absorbant {}

