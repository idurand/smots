(in-package :smots)

(defclass idem-mixin () ())

(defmethod clean-args :before ((lexpr idem-mixin))
  (when *trace-clean-args*
    (format *error-output* "clean-args :before idem-mixin ~A~%" lexpr))
  (setf (args lexpr)
	(remove-duplicates (args lexpr) :test #'equivalent))
  lexpr)

(defclass ic-mixin (idem-mixin commutative-mixin) ()
  (:documentation "idempotent and commutative"))